#include "part.h"
#include "position.h"
#include "Service/innercsswitcher.h"
#include "Service/logicchecker.h"

Part::Part(const AbstractDetail *detail, const AbstractPosition *position)
{
    _detail = detail;
    _pos = new Position(*position);
}

Part::~Part()
{
    delete _pos;
}

const AbstractDetail* Part::getDetail() const
{
    return _detail;
}

const AbstractPosition* Part::getPosition() const
{
    return _pos;
}

void Part::setPosition(const AbstractPosition &position)
{
    *_pos = position;
}

AbstractDesignCore* Part::cloneCore() const
{
    return _detail->getDesign()->cloneCore();
}

bool Part::isConnectedWith(const AbstractPart* part) const
{
    return LogicChecker::isPartsConnected(this,part);
}

bool Part::isSupportedBy(const AbstractPart* part) const
{
    return LogicChecker::isPartASupportingB(part,this);
}

bool Part::isSupportFor(const AbstractPart* part) const
{
    return LogicChecker::isPartASupportingB(this,part);
}

bool Part::isConflictingWith(const AbstractPart* part) const
{
    return LogicChecker::isPartsConflicting(this,part);
}

int Part::getLength() const
{
    return InnerCSSwitcher::getLength(_pos->getRotation(), _detail->getDesign());
}

int Part::getWidth() const
{
    return InnerCSSwitcher::getWidth(_pos->getRotation(), _detail->getDesign());
}

int Part::getHeight() const
{
    return _detail->getDesign()->getHeight();
}

bool Part::isUpperSpaceFree(int x, int y, int z) const
{
    InnerCSSwitcher(_detail->getDesign(),_pos).switchCS(x,y,z);
    return _detail->getDesign()->isUpperSpaceFree(x,y,z);
}

bool Part::isLowerSpaceFree(int x, int y, int z) const
{
    InnerCSSwitcher(_detail->getDesign(),_pos).switchCS(x,y,z);
    return _detail->getDesign()->isLowerSpaceFree(x,y,z);
}

bool Part::isMidSpaceFree(int x, int y, int z) const
{
    InnerCSSwitcher(_detail->getDesign(),_pos).switchCS(x,y,z);
    return _detail->getDesign()->isMidSpaceFree(x,y,z);
}

AbstractPart* Part::clonePart() const
{
    return new Part(_detail,_pos);
}

bool Part::isSimple() const
{
    return _detail->getDesign()->isSimple();
}

bool Part::isCuttedUpper() const
{
    return _detail->isCuttedUpper();
}

bool Part::isCuttedLower() const
{
    return _detail->isCuttedLower();
}

const AbstractDesignCore* Part::getDesignCore() const
{
    return _detail->getDesign();
}

bool Part::isComposite() const
{
    return (_detail->getDesign()->getSubdesignsNum() != 0);
}
