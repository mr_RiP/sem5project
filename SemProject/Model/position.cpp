#include "position.h"
#include <cmath>
#include <stdexcept>

Position::Position()
{
    _x = 0;
    _y = 0;
    _z = 0;
    _rotation = 0;
}

int Position::GetRotationNormalView(int rotation)
{
    while (rotation >= 360)
        rotation -= 360;
    while (rotation < 0)
        rotation += 360;
    return rotation;
}

void Position::RotationCheck(int rotation)
{
    if ((rotation % 90) != 0)
        throw std::invalid_argument("Trying to set invalid rotation parameter. "
                                    "Rotation parameter must be multiple of 90.");
}

Position::Position(int x, int y, int z, int rotation)
{
    RotationCheck(rotation);
    _x = x;
    _y = y;
    _z = z;
    _rotation = GetRotationNormalView(rotation);
}

Position::Position(const AbstractPosition &parent)
{
    RotationCheck(parent.getRotation());

    _x = parent.getX();
    _y = parent.getY();
    _z = parent.getZ();
    _rotation = GetRotationNormalView(parent.getRotation());
}

Position::Position(const AbstractPosition *parent)
{
    if (parent == NULL)
        throw std::invalid_argument("Trying to create a Position object from NULL parent.");

    RotationCheck(parent->getRotation());

    _x = parent->getX();
    _y = parent->getY();
    _z = parent->getZ();
    _rotation = GetRotationNormalView(parent->getRotation());
}

Position::Position(const QVector3D &pos, int rotation, bool round)
{
    RotationCheck(rotation);

    _x = round? roundf(pos.x()): pos.x();
    _y = round? roundf(pos.y()): pos.y();
    _z = round? roundf(pos.z()): pos.z();
    _rotation = GetRotationNormalView(rotation);
}

int Position::getRotation() const
{
    return _rotation;
}

int Position::getX() const
{
    return _x;
}

int Position::getY() const
{
    return _y;
}

int Position::getZ() const
{
    return _z;
}

void Position::setRotation(int value)
{
    RotationCheck(value);
    _rotation = GetRotationNormalView(value);
}

AbstractPosition *Position::clonePosition() const
{
    return new Position(this);
}

void Position::setX(int value)
{
    _x = value;
}

void Position::setY(int value)
{
    _y = value;
}

void Position::setZ(int value)
{
    _z = value;
}

const AbstractPosition& Position::operator =(const AbstractPosition& new_pos)
{
    if (&new_pos == this)
        return *this;

    RotationCheck(new_pos.getRotation());
    _x = new_pos.getX();
    _y = new_pos.getY();
    _z = new_pos.getZ();
    _rotation = GetRotationNormalView(new_pos.getRotation());

    return *this;
}

Position::~Position()
{

}

const AbstractPosition& Position::operator +=(const AbstractPosition& pos)
{
    RotationCheck(pos.getRotation());

    _x += pos.getX();
    _y += pos.getY();
    _z += pos.getZ();
    _rotation = GetRotationNormalView(_rotation + pos.getRotation());

    return *this;
}
