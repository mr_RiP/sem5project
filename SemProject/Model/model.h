#ifndef MODEL_H
#define MODEL_H

#include "Abstract/abstractmodel.h"
#include <QList>

class Model : public AbstractModel
{
    Q_OBJECT
public:
    Model();
    ~Model();

    int getDesignsNum() const;
    int getDetailsNum() const;
    int getPartsNum() const;

    const AbstractDesign* getDesign(int index) const;
    const AbstractDetail* getDetail(int index) const;
    const AbstractPart* getPart(int index) const;

    void setDesignName(int index, const QString& name);
    void setDetailName(int index, const QString& name);

    bool isDesignNameExists(const QString& name) const;
    bool isDetailNameExists(const QString& name) const;

    int getDesignIndex(const AbstractDesign* design) const;
    int getDetailIndex(const AbstractDetail* detail) const;
    int getPartIndex(const AbstractPart* part) const;

    void setDetailColor(int detail_index, const QColor& color);
    const QColor& getDetailColor(int detail_index) const;

    void addDesignSimple(int length, int width, int height, const QString& name, bool check=false);
    void addDesignCuttedUpper(int length, int width, int height, const QString& name, bool check=false);
    void addDesignCuttedLower(int length, int width, int height, const QString& name, bool check=false);
    //void addDesignComposite(const QList<int>& parts_indexes, const QString& name, bool check=false);

    void addDetail(int design_index, const QColor& color, const QString& name, bool check=false);
    void addPart(int detail_index, const AbstractPosition& pos, bool check=false);
    void addPart(int detail_index, int x, int y, int z, int rotation, bool check=false);

    bool isPartsConnected(const QList<int>& parts_indexes) const;

    bool isDesignCanBeDeleted(int design_index) const;
    bool isDetailCanBeDeleted(int detail_index) const;
    bool isPartCanBeDeleted(int part_index) const;

    bool isPartsCanBeDeleted(const QList<int>& parts_indexes) const;
    bool isDetailCanBePlaced(int detail_index, const AbstractPosition& position) const;
    bool isDetailCanBePlaced(int detail_index, int x, int y, int z, int rotation) const;

    void clearParts();
    void clearAll();

    int loadDesign(AbstractDesign* design);

    void deleteDesign(int index, bool check=false);
    void deleteDetail(int index, bool check=false);
    void deletePart(int index, bool check=false);
    void deleteParts(const QList<int>& index_list, bool check=false);

protected:
    QList<AbstractDesign*> _designs;
    QList<AbstractDetail*> _details;
    QList<AbstractPart*> _parts;

    void FreeLists();
    void SetPosedDesignsList(QList<const AbstractPosedDesign *> &parts, const QList<int>& parts_indexes) const;
    void SetPartsList(QList<const AbstractPart*> &parts, const QList<int>& parts_indexes) const;
    template <typename T>
    static void FreeConcreteList(QList<T*>& list);
    static void SortIndexList(QList<int>& list);
};

#endif // MODEL_H
