#ifndef CUTTEDDESIGN_H
#define CUTTEDDESIGN_H

#include "simpledesign.h"

class CuttedDesign : public SimpleDesign
{
public:
    CuttedDesign(int length, int width, int height, bool upper_cut);
    CuttedDesign(int length, int width, int height, bool upper_cut, const QString& name);

    bool isUpperSpaceFree(int x, int y, int z) const;
    bool isLowerSpaceFree(int x, int y, int z) const;
    bool isMidSpaceFree(int x, int y, int z) const;
    bool isSimple() const;
    bool isCuttedUpper() const;
    bool isCuttedLower() const;

    AbstractDesign* cloneDesign() const;
    AbstractDesignCore* cloneCore() const;

protected:
    bool _upper_cut;
    float _a;
    float _b;

    CuttedDesign();

    void SetEqualiationParameters();
    float LineEqualiation(int x, int z, float shift) const;
};

#endif // CUTTEDDESIGN_H
