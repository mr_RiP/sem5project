#include "abstractposition.h"

bool operator ==(const AbstractPosition &a, const AbstractPosition &b)
{
    return (a.getX() == b.getX()) && (a.getY() == b.getY()) &&
            (a.getZ() == b.getZ()) && (a.getRotation() == b.getRotation());
}


bool operator !=(const AbstractPosition &a, const AbstractPosition &b)
{
    return (a.getX() != b.getX()) || (a.getY() != b.getY()) ||
            (a.getZ() != b.getZ()) || (a.getRotation() != b.getRotation());
}
