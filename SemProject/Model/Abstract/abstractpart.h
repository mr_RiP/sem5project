#ifndef ABSTRACTPART_H
#define ABSTRACTPART_H

#include "abstractdetail.h"
#include "abstractposeddesign.h"

// Абстрактный класс части
// Под частью я понимаю экземпляр детали, помещенный в модель

class AbstractPart : public AbstractPosedDesign
{
public:
    virtual ~AbstractPart() {}

    virtual const AbstractDetail* getDetail() const = 0;

    virtual bool isConnectedWith(const AbstractPart* part) const = 0;
    virtual bool isSupportedBy(const AbstractPart* part) const = 0;
    virtual bool isSupportFor(const AbstractPart* part) const = 0;
    virtual bool isConflictingWith(const AbstractPart* part) const = 0;

    virtual bool isComposite() const = 0;

    virtual AbstractPart* clonePart() const = 0;
};

#endif // ABSTRACTPART_H

