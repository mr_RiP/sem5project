#ifndef ABSTRACTDESIGNCORE_H
#define ABSTRACTDESIGNCORE_H

// Абстрактный класс - основа для классов дизайнов и поддизайнов
// Под дизайном я понимаю данные о форме детали, без данных о цвете и её размещении в модели

class AbstractDesignCore
{
public:
    virtual ~AbstractDesignCore() {}

    virtual int getLength() const = 0;
    virtual int getWidth() const = 0;
    virtual int getHeight() const = 0;

    virtual bool isUpperSpaceFree(int x, int y, int z) const = 0;
    virtual bool isLowerSpaceFree(int x, int y, int z) const = 0;
    virtual bool isMidSpaceFree(int x, int y, int z) const = 0;
    virtual bool isSimple() const = 0;
    virtual bool isCuttedUpper() const = 0;
    virtual bool isCuttedLower() const = 0;

    virtual AbstractDesignCore* cloneCore() const = 0;
};

#endif // ABSTRACTDESIGNCORE_H

