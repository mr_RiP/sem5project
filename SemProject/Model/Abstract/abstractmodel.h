#ifndef ABSTRACTMODEL_H
#define ABSTRACTMODEL_H

#include "abstractdesign.h"
#include "abstractdetail.h"
#include "abstractpart.h"
#include <QList>
#include <QObject>

// Абстрактный класс модели
// Предоставляет интерфейс для работы с дизайнами, деталями и частями

class AbstractModel : public QObject
{
    Q_OBJECT
public:
    virtual ~AbstractModel() {}

    virtual int getDesignsNum() const = 0;
    virtual int getDetailsNum() const = 0;
    virtual int getPartsNum() const = 0;

    virtual const AbstractDesign* getDesign(int index) const = 0;
    virtual const AbstractDetail* getDetail(int index) const = 0;
    virtual const AbstractPart* getPart(int index) const = 0;

    virtual void setDetailColor(int detail_index, const QColor& color) = 0;
    virtual const QColor& getDetailColor(int detail_index) const = 0;

    virtual void setDesignName(int index, const QString& name) = 0;
    virtual void setDetailName(int index, const QString& name) = 0;

    virtual bool isDesignNameExists(const QString& name) const = 0;
    virtual bool isDetailNameExists(const QString& name) const = 0;

    virtual int getDesignIndex(const AbstractDesign* design) const = 0;
    virtual int getDetailIndex(const AbstractDetail* detail) const = 0;
    virtual int getPartIndex(const AbstractPart* part) const = 0;

    virtual void addDesignSimple(int length, int width, int height, const QString& name, bool check=false) = 0;
    virtual void addDesignCuttedUpper(int length, int width, int height, const QString& name, bool check=false) = 0;
    virtual void addDesignCuttedLower(int length, int width, int height, const QString& name, bool check=false) = 0;
    //virtual void addDesignComposite(const QList<int>& parts_indexes, const QString& name, bool check=false) = 0;

    virtual void addDetail(int design_index, const QColor& color, const QString& name, bool check=false) = 0;
    virtual void addPart(int detail_index, const AbstractPosition& pos, bool check=false) = 0;
    virtual void addPart(int detail_index, int x, int y, int z, int rotation, bool check=false) = 0;

    virtual bool isPartsConnected(const QList<int>& parts_indexes) const = 0;

    virtual bool isDesignCanBeDeleted(int design_index) const = 0;
    virtual bool isDetailCanBeDeleted(int detail_index) const = 0;
    virtual bool isPartCanBeDeleted(int part_index) const = 0;

    virtual bool isPartsCanBeDeleted(const QList<int>& parts_indexes) const = 0;
    virtual bool isDetailCanBePlaced(int detail_index, const AbstractPosition& position) const = 0;
    virtual bool isDetailCanBePlaced(int detail_index, int x, int y, int z, int rotation) const = 0;

    virtual void clearParts() = 0;
    virtual void clearAll() = 0;

    virtual int loadDesign(AbstractDesign* design) = 0;

    virtual void deleteDesign(int index, bool check=false) = 0;
    virtual void deleteDetail(int index, bool check=false) = 0;
    virtual void deletePart(int index, bool check=false) = 0;
    virtual void deleteParts(const QList<int>& index_list, bool check=false) = 0;

signals:
    void designsChanged();
    void detailsChanged();
    void partsChanged();
    void designDeleted(int index);
    void detailDeleted(int index);
    void partDeleted(int index);
};

#endif // ABSTRACTMODEL_H

