#ifndef ABSTRACTDETAIL_H
#define ABSTRACTDETAIL_H

#include "abstractdesign.h"
#include <QColor>

// Абстрактный класс детали
// Под деталью я понимаю дизайн, обладающий параметром цвета

class AbstractDetail : public AbstractDesign
{
public:
    virtual ~AbstractDetail() {}

    virtual const AbstractDesign* getDesign() const = 0;
    virtual void setColor(const QColor& color) = 0;
    virtual const QColor& getColor() const = 0;

    virtual AbstractDetail* cloneDetail() const = 0;
};

#endif // ABSTRACTDETAIL_H

