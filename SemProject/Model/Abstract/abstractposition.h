#ifndef ABSTRACTPOSITION_H
#define ABSTRACTPOSITION_H

// Абстрактный класс для определения позиции части (поддизайна) в модели

class AbstractPosition
{
public:
    virtual ~AbstractPosition() {}

    virtual int getX() const = 0;
    virtual int getY() const = 0;
    virtual int getZ() const = 0;
    virtual int getRotation() const = 0;

    virtual void setX(int value) = 0;
    virtual void setY(int value) = 0;
    virtual void setZ(int value) = 0;
    virtual void setRotation(int value) = 0;

    virtual AbstractPosition* clonePosition() const = 0;

    virtual const AbstractPosition& operator =(const AbstractPosition& new_pos) = 0;
    virtual const AbstractPosition& operator +=(const AbstractPosition& pos) = 0;


    friend bool operator ==(const AbstractPosition& a, const AbstractPosition& b);
    friend bool operator !=(const AbstractPosition& a, const AbstractPosition& b);
};

/* Интерфейс для наследников
    int getX() const;
    int getY() const;
    int getZ() const;
    int getRotation() const;

    void setX(int value);
    void setY(int value);
    void setZ(int value);
    void setRotation(int value);

    const AbstractPosition& operator =(const AbstractPosition& new_pos);
*/

#endif // ABSTRACTPOSITION_H

