#ifndef ABSTRACTSUBDESIGN_H
#define ABSTRACTSUBDESIGN_H

#include "abstractposeddesign.h"
#include "abstractposition.h"

// Абстрактный класс поддизайна - части составного дизайна
// Допущение - составной дизайн может состоять только из несоставных частей

class AbstractSubdesign : public AbstractPosedDesign
{
public:
    virtual ~AbstractSubdesign() {}

    virtual AbstractSubdesign* cloneSubdesign() const = 0;
};

#endif // ABSTRACTSUBDESIGN_H

