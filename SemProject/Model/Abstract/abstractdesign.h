#ifndef ABSTRACTDESIGN_H
#define ABSTRACTDESIGN_H

#include "abstractdesigncore.h"
#include "abstractsubdesign.h"
#include <QString>

// Абстрактный класс дизайна
// Используется для полиморфных операций с любым дизайном

class AbstractDesign : public AbstractDesignCore
{
public:
    virtual ~AbstractDesign() {}

    virtual void setName(const QString& name) = 0;
    virtual const QString& getName() const = 0;

    virtual const AbstractSubdesign* getSubdesign(int index) const = 0;
    virtual int getSubdesignsNum() const = 0;    
    virtual AbstractDesign* cloneDesign() const = 0;
};

#endif // ABSTRACTDESIGN_H
