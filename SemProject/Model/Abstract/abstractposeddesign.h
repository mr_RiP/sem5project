#ifndef ABSTRACTPOSEDDESIGN_H
#define ABSTRACTPOSEDDESIGN_H

#include "abstractdesigncore.h"
#include "abstractposition.h"

// Абстрактный класс - основа дизайна, расположенного в модели или составном дизайне

class AbstractPosedDesign : public AbstractDesignCore
{
public:
    virtual ~AbstractPosedDesign() {}

    virtual const AbstractPosition* getPosition() const = 0;
    virtual void setPosition(const AbstractPosition& position) = 0;

    virtual const AbstractDesignCore* getDesignCore() const = 0;
};

#endif // ABSTRACTPOSEDDESIGN_H

