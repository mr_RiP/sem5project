#include "cutteddesign.h"
#include <stdexcept>
#include <float.h>

CuttedDesign::CuttedDesign()
{

}

void CuttedDesign::SetEqualiationParameters()
{
    _a = _length-1;
    _b = _height;

    if (_upper_cut)
    {
        _a = -_b / _a;
    }
    else
    {
        _a = _b / _a;
        _b = 0.f;
    }
}

CuttedDesign::CuttedDesign(int length, int width, int height, bool upper_cut)
{
    if ((length < 2) || (width < 1) || (height < 1))
        throw std::invalid_argument("Trying to create irreal cutted design.");
    _length = length;
    _width = width;
    _height = height;
    _upper_cut = upper_cut;

    SetEqualiationParameters();
}

CuttedDesign::CuttedDesign(int length, int width, int height, bool upper_cut, const QString &name)
{
    if ((length < 2) || (width < 1) || (height < 1))
        throw std::invalid_argument("Trying to create irreal cutted design.");
    _length = length;
    _width = width;
    _height = height;
    _upper_cut = upper_cut;
    _name = name;

    SetEqualiationParameters();
}

#define CUTTEDDESIGN_UPPER_SPACE_SHIFT 1.f
#define CUTTEDDESIGN_LOWER_SPACE_SHIFT 0.f
#define CUTTEDDESIGN_MID_SPACE_SHIFT 0.5f

float CuttedDesign::LineEqualiation(int x, int z, float shift) const
{
    float xf = (float)x - 1;
    if (x == 1)
        xf += 0.00001f;
    float zf = (float)z + shift;
    xf = _a * xf + _b;
    return (zf - xf);
}

bool CuttedDesign::isLowerSpaceFree(int x, int y, int z) const
{
    if (IsSpaceFree(x,y,z))
        return true;

    if (x == 0)
        return false;

    float eq = LineEqualiation(x,z,CUTTEDDESIGN_LOWER_SPACE_SHIFT);
    if (((_upper_cut) && (eq >= 0.f)) || ((!_upper_cut) && (eq <= 0.f)))
        return true;
    else
        return false;

}

bool CuttedDesign::isUpperSpaceFree(int x, int y, int z) const
{
    if (IsSpaceFree(x,y,z))
        return true;

    if (x == 0)
        return false;

    float eq = LineEqualiation(x,z,CUTTEDDESIGN_UPPER_SPACE_SHIFT);
    if (((_upper_cut) && (eq >= 0.f)) || ((!_upper_cut) && (eq <= 0.f)))
        return true;
    else
        return false;
}

bool CuttedDesign::isMidSpaceFree(int x, int y, int z) const
{
    if (IsSpaceFree(x,y,z))
        return true;

    if (x == 0)
        return false;

    float eq = LineEqualiation(x,z,CUTTEDDESIGN_MID_SPACE_SHIFT);
    if (((_upper_cut) && (eq >= 0.f)) || ((!_upper_cut) && (eq <= 0.f)))
        return true;
    else
        return false;
}

AbstractDesign* CuttedDesign::cloneDesign() const
{
    return new CuttedDesign(_length,_width,_height,_upper_cut,_name);
}

AbstractDesignCore *CuttedDesign::cloneCore() const
{
    return new CuttedDesign(_length,_width,_height,_upper_cut);
}

bool CuttedDesign::isSimple() const
{
    return false;
}

bool CuttedDesign::isCuttedUpper() const
{
    return _upper_cut;
}

bool CuttedDesign::isCuttedLower() const
{
    return !_upper_cut;
}
