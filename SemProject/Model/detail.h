#ifndef DETAIL_H
#define DETAIL_H

#include "Abstract/abstractdetail.h"

class Detail : public AbstractDetail
{
public:
    Detail(const AbstractDesign *design, const QColor& color);
    Detail(const AbstractDesign *design, const QColor& color, const QString& name);
    ~Detail();

    int getLength() const;
    int getWidth() const;
    int getHeight() const;

    bool isUpperSpaceFree(int x, int y, int z) const;
    bool isLowerSpaceFree(int x, int y, int z) const;
    bool isMidSpaceFree(int x, int y, int z) const;
    bool isSimple() const;
    bool isCuttedUpper() const;
    bool isCuttedLower() const;

    const AbstractDesign* getDesign() const;
    void setColor(const QColor& color);
    const QColor& getColor() const;

    void setName(const QString& name);
    const QString& getName() const;

    const AbstractSubdesign* getSubdesign(int index) const;
    int getSubdesignsNum() const;

    AbstractDesignCore* cloneCore() const;
    AbstractDesign* cloneDesign() const;
    AbstractDetail* cloneDetail() const;

protected:
    Detail();

private:
    QColor _color;
    const AbstractDesign* _design;
    QString _name;
};

#endif // DETAIL_H
