#ifndef POSITION_H
#define POSITION_H

#include "Abstract/abstractposition.h"
#include <QVector3D>

class Position : public AbstractPosition
{
public:
    Position();
    Position(int x, int y, int z, int rotation);
    Position(const AbstractPosition& parent);
    Position(const AbstractPosition *parent);
    Position(const QVector3D& pos, int rotation, bool round=false);
    ~Position();

    int getX() const;
    int getY() const;
    int getZ() const;
    int getRotation() const;

    void setX(int value);
    void setY(int value);
    void setZ(int value);
    void setRotation(int value);

    AbstractPosition *clonePosition() const;

    const AbstractPosition& operator =(const AbstractPosition& new_pos);
    const AbstractPosition& operator +=(const AbstractPosition& pos);

protected:
    int _x;
    int _y;
    int _z;
    int _rotation;

    int GetRotationNormalView(int rotation);
    void RotationCheck(int rotation);
};


#endif // POSITION_H
