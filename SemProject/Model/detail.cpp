#include "detail.h"
#include <stdexcept>

Detail::Detail(const AbstractDesign *design, const QColor &color)
{
    _design = design;
    _color = color;
}

Detail::Detail(const AbstractDesign *design, const QColor &color, const QString &name)
{
    _design = design;
    _color = color;
    _name = name;
}

Detail::~Detail()
{
}

int Detail::getLength() const
{
    return _design->getLength();
}

int Detail::getWidth() const
{
    return _design->getWidth();
}

int Detail::getHeight() const
{
    return _design->getHeight();
}

bool Detail::isUpperSpaceFree(int x, int y, int z) const
{
    return _design->isUpperSpaceFree(x,y,z);
}

bool Detail::isLowerSpaceFree(int x, int y, int z) const
{
    return _design->isLowerSpaceFree(x,y,z);
}

bool Detail::isMidSpaceFree(int x, int y, int z) const
{
    return _design->isMidSpaceFree(x,y,z);
}

bool Detail::isSimple() const
{
    return _design->isSimple();
}

bool Detail::isCuttedUpper() const
{
    return _design->isCuttedUpper();
}

bool Detail::isCuttedLower() const
{
    return _design->isCuttedLower();
}

AbstractDesignCore *Detail::cloneCore() const
{
    return _design->cloneCore();
}

void Detail::setColor(const QColor &color)
{
    _color = color;
}

const QColor& Detail::getColor() const
{
    return _color;
}

void Detail::setName(const QString &name)
{
    _name = name;
}

const QString &Detail::getName() const
{
    return _name;
}

const AbstractSubdesign *Detail::getSubdesign(int index) const
{
    return _design->getSubdesign(index);
}

int Detail::getSubdesignsNum() const
{
    return _design->getSubdesignsNum();
}

AbstractDesign *Detail::cloneDesign() const
{
    return _design->cloneDesign();
}

const AbstractDesign* Detail::getDesign() const
{
    return _design;
}

AbstractDetail* Detail::cloneDetail() const
{
    return new Detail(_design,_color);
}
