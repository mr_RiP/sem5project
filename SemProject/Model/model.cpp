#include "model.h"
#include <stdexcept>
#include "simpledesign.h"
#include "cutteddesign.h"
#include "detail.h"
#include "part.h"
#include "Service/logicchecker.h"
#include "position.h"

Model::Model()
{
}

Model::~Model()
{
    FreeLists();
}

void Model::FreeLists()
{
    // Можно было бы распараллелить (нужно узнать как)
    FreeConcreteList(_parts);

    FreeConcreteList(_details);

    FreeConcreteList(_designs);
}

void Model::SetPartsList(QList<const AbstractPart *> &parts,
                         const QList<int> &parts_indexes) const
{
    parts.clear();
    int n = parts_indexes.size();
    for (int i=0; i<n; i++)
        parts.append(getPart(parts_indexes[i]));
}

void Model::SortIndexList(QList<int> &list)
{
    int n = list.size() - 1;
    for (int i=0; i<n; i++)
    {
        int m = n-i;
        for (int j=0; j<m; j++)
            if (list[j] < list[j+1])
                list.swap(j,j+1);
    }
}

void Model::SetPosedDesignsList(QList<const AbstractPosedDesign *> &parts,
                         const QList<int> &parts_indexes) const
{
    parts.clear();
    int n = parts_indexes.size();
    for (int i=0; i<n; i++)
        parts.append(getPart(parts_indexes[i]));
}

int Model::getDesignsNum() const
{
    return _designs.size();
}

int Model::getDetailsNum() const
{
    return _details.size();
}

int Model::getPartsNum() const
{
    return _parts.size();
}

const AbstractDesign* Model::getDesign(int index) const
{
    if ((index < 0) || (index >= _designs.size()))
        throw std::invalid_argument("Trying to get design with invalid index.");

    return _designs[index];
}

const AbstractDetail* Model::getDetail(int index) const
{
    if ((index < 0) || (index >= _details.size()))
        throw std::invalid_argument("Trying to get detail with invalid index.");

    return _details[index];
}

const AbstractPart* Model::getPart(int index) const
{
    if ((index < 0) || (index >= _parts.size()))
        throw std::invalid_argument("Trying to get part with invalid index.");

    return _parts[index];
}

void Model::setDesignName(int index, const QString &name)
{
    if ((index < 0) || (index >= _designs.size()))
        throw std::invalid_argument("Trying to set name for non-existent design.");

    _designs[index]->setName(name);
    emit designsChanged();
}

void Model::setDetailName(int index, const QString &name)
{
    if ((index < 0) || (index >= _details.size()))
        throw std::invalid_argument("Trying to set name for non-existent detail.");

    _details[index]->setName(name);
    emit detailsChanged();
}

bool Model::isDesignNameExists(const QString &name) const
{
    for (int i=0; i<_designs.size(); i++)
        if (_designs[i]->getName() == name)
            return true;
    return false;
}

bool Model::isDetailNameExists(const QString &name) const
{
    for (int i=0; i<_details.size(); i++)
        if (_details[i]->getName() == name)
            return true;
    return false;
}

int Model::getDesignIndex(const AbstractDesign* design) const
{
    for (int i=0; i<_designs.size(); i++)
        if (design == _designs[i])
            return i;
    return -1;
}

int Model::getDetailIndex(const AbstractDetail* detail) const
{
    for (int i=0; i<_details.size(); i++)
        if (detail == _details[i])
            return i;
    return -1;
}

int Model::getPartIndex(const AbstractPart* part) const
{
    for (int i=0; i<_parts.size(); i++)
        if (part == _parts[i])
            return i;
    return -1;
}

void Model::setDetailColor(int detail_index, const QColor &color)
{
    if ((detail_index < 0) || (detail_index >= _details.size()))
        throw std::invalid_argument("Trying to set color for non-existent detail.");

    _details[detail_index]->setColor(color);
    emit detailsChanged();
    emit partsChanged();
}

const QColor &Model::getDetailColor(int detail_index) const
{
    if ((detail_index < 0) || (detail_index >= _details.size()))
        throw std::invalid_argument("Trying to set color for non-existent detail.");

    return _details[detail_index]->getColor();
}

void Model::addDesignSimple(int length, int width, int height, const QString& name, bool check)
{
    if (check && isDesignNameExists(name))
        throw std::invalid_argument("Trying to create a simple design which name are used by other design in the model.");

    _designs.append(new SimpleDesign(length,width,height,name));
    emit designsChanged();
}

void Model::addDesignCuttedUpper(int length, int width, int height, const QString& name, bool check)
{
    if (check && isDesignNameExists(name))
        throw std::invalid_argument("Trying to create a upper cutted design which name are used by other design in the model.");

    _designs.append(new CuttedDesign(length, width, height,true,name));
    emit designsChanged();
}

void Model::addDesignCuttedLower(int length, int width, int height, const QString &name, bool check)
{
    if (check && isDesignNameExists(name))
        throw std::invalid_argument("Trying to create a lower cutted design which name are used by other design in the model.");

    _designs.append(new CuttedDesign(length, width, height,false,name));
    emit designsChanged();
}
/*
void Model::addDesignComposite(const QList<int> &parts_indexes, const QString &name, bool check)
{
    QList<const AbstractPart*> parts;
    SetPartsList(parts, parts_indexes);
    _designs.append(new CompositeDesign(parts,name,check));
    emit designsChanged();
}
*/
void Model::addDetail(int design_index, const QColor& color, const QString &name, bool check)
{
    if (check)
    {
        if ((design_index < 0) || (design_index >= _designs.size()))
            throw std::invalid_argument("Trying to create a detail with non-existent design.");
        if (isDetailNameExists(name))
            throw std::invalid_argument("Trying to create a detail which name are used by other design in the model.");
    }

    _details.append(new Detail(_designs[design_index],color,name));
    emit detailsChanged();
}

void Model::addPart(int detail_index, const AbstractPosition& pos, bool check)
{
    if (check)
    {
        if ((detail_index < 0) || (detail_index >= _details.size()))
            throw std::invalid_argument("Trying to create a part from non-existent detail.");
        if (!isDetailCanBePlaced(detail_index,pos))
            throw std::invalid_argument("Trying to create a part that cannot be placed in model.");
    }

    _parts.append(new Part(_details[detail_index],&pos));
    emit partsChanged();
}

bool Model::isDesignCanBeDeleted(int design_index) const
{
    if ((design_index < 0) || (design_index >= _designs.size()))
        return false;

    for (int i=0; i<_details.size(); i++)
    {
        if (_details[i]->getDesign() == _designs[design_index])
            return false;
    }

    return true;
}

bool Model::isDetailCanBeDeleted(int detail_index) const
{
    if ((detail_index < 0) || (detail_index >= _details.size()))
        return false;

    for (int i=0; i<_parts.size(); i++)
    {
        if (_parts[i]->getDetail() == _details[detail_index])
            return false;
    }

    return true;
}

bool Model::isPartCanBeDeleted(int part_index) const
{
    if ((part_index < 0) || (part_index >= _parts.size()))
        return false;

    for (int i=0; i<_parts.size(); i++)
    {
        if ((part_index != i) && (_parts[part_index]->isSupportFor(_parts[i])))
            return false;
    }

    return true;
}

bool Model::isPartsCanBeDeleted(const QList<int> &parts_indexes) const
{
    int n = parts_indexes.size();
    int m = _parts.size();
    for (int i=0; i<n; i++)
        for (int j=0; j<m; j++)
            if ((!parts_indexes.contains(j)) &&
                    _parts[parts_indexes[i]]->isSupportFor(_parts[j]))
                return false;
    return true;
}

bool Model::isDetailCanBePlaced(int detail_index, const AbstractPosition& position) const
{
    if ((detail_index < 0) || (detail_index >= _details.size()))
        return false;

    Part tmp(_details[detail_index],&position);
    int n = _parts.size();

    for (int i=0; i<n; i++)
        if (_parts[i]->isConflictingWith(&tmp))
            return false;

    if (position.getZ() == 0)
        return true;
    else
        for (int i=0; i<n; i++)
            if (_parts[i]->isSupportFor(&tmp))
                return true;

    return false;
}

bool Model::isDetailCanBePlaced(int detail_index, int x, int y, int z, int rotation) const
{
    return isDetailCanBePlaced(detail_index,Position(x,y,z,rotation));
}

template <typename T>
void Model::FreeConcreteList(QList<T*>& list)
{
    while (!list.isEmpty())
    {
        delete list[0];
        list.removeFirst();
    }
}

void Model::clearParts()
{
    FreeConcreteList(_parts);

    emit partsChanged();
}

void Model::clearAll()
{
    FreeLists();

    emit partsChanged();
    emit detailsChanged();
    emit designsChanged();
}

int Model::loadDesign(AbstractDesign *design)
{
    if (design == NULL)
        throw std::invalid_argument("Trying to load a NULL design to Model.");

    _designs.append(design);
    emit designsChanged();
    return _designs.size()-1;
}

void Model::deleteDesign(int index, bool check)
{
    if (check)
    {
        if ((index < 0) || (index >= _designs.size()))
            throw std::invalid_argument("Trying to delete non-existent design.");
        if (!isDesignCanBeDeleted(index))
            throw std::invalid_argument("Trying to delete design that can't "
                                        "be deleted due model logic.");
    }

    delete _designs[index];
    _designs.removeAt(index);
    emit designDeleted(index);
    emit designsChanged();
}

void Model::deleteDetail(int index, bool check)
{
    if (check)
    {
        if ((index < 0) || (index >= _details.size()))
            throw std::invalid_argument("Trying to delete non-existent detail.");
        if (!isDetailCanBeDeleted(index))
            throw std::invalid_argument("Trying to delete detail that can't "
                                        "be deleted due model logic.");
    }

    delete _details[index];
    _details.removeAt(index);
    emit detailDeleted(index);
    emit detailsChanged();
}

void Model::deletePart(int index, bool check)
{
    if (check)
    {
        if ((index < 0) || (index >= _parts.size()))
            throw std::invalid_argument("Trying to delete non-existent part.");
        if (!isPartCanBeDeleted(index))
            throw std::invalid_argument("Trying to delete part that can't "
                                        "be deleted due model logic.");
    }

    delete _parts[index];
    _parts.removeAt(index);
    emit partDeleted(index);
    emit partsChanged();
}

void Model::deleteParts(const QList<int> &index_list, bool check)
{
    if (check && !isPartsCanBeDeleted(index_list))
        throw std::invalid_argument("Trying to delete parts that can't be deleted.");

    QList<int> sorted(index_list);
    SortIndexList(sorted);

    int n = sorted.size();
    for (int i=0; i<n; i++)
        deletePart(sorted[i]);
}

bool Model::isPartsConnected(const QList<int>& parts_indexes) const
{
    QList<const AbstractPart*> parts;
    SetPartsList(parts, parts_indexes);
    return LogicChecker::isPartsListConnected(parts);
}


void Model::addPart(int detail_index, int x, int y, int z, int rotation, bool check)
{
    addPart(detail_index,Position(x,y,z,rotation),check);
}
