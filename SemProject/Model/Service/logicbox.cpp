#include "logicbox.h"
#include <stddef.h>

LogicBox::LogicBox()
{
    setBoxNull();
}

LogicBox::LogicBox(int x, int y, int z, int length, int width, int height)
{
    _x = x;
    _y = y;
    _z = z;
    _length = length;
    _width = width;
    _height = height;
}

LogicBox::LogicBox(const LogicBox &box)
{
    setBox(box);
}

LogicBox::LogicBox(const AbstractPosedDesign *design)
{
    setBox(design);
}

LogicBox::LogicBox(const AbstractPosedDesign *a, const AbstractPosedDesign *b)
{
    setBox(a,b);
}

LogicBox::~LogicBox()
{

}

int LogicBox::getX() const
{
    return _x;
}

int LogicBox::getY() const
{
    return _y;
}

int LogicBox::getZ() const
{
    return _z;
}

int LogicBox::getLength() const
{
    return _length;
}

int LogicBox::getWidth() const
{
    return _width;
}

int LogicBox::getHeight() const
{
    return _height;
}

int LogicBox::getMaxX() const
{
    return _x + _length - 1;
}

int LogicBox::getMaxY() const
{
    return _y + _width - 1;
}

int LogicBox::getMaxZ() const
{
    return _z + _height - 1;
}

int LogicBox::getGlobalLength() const
{
    return _x + _length;
}

int LogicBox::getGlobalWidth() const
{
    return _y + _width;
}

int LogicBox::getGlobalHeight() const
{
    return _z + _height;
}

void LogicBox::setX(int value)
{
    _x = value;
}

void LogicBox::setY(int value)
{
    _y = value;
}

void LogicBox::setZ(int value)
{
    _z = value;
}

void LogicBox::setLength(int value)
{
    _length = value;
}

void LogicBox::setWidth(int value)
{
    _width = value;
}

void LogicBox::setHeight(int value)
{
    _height = value;
}

void LogicBox::setBox(const LogicBox &box)
{
    _x = box._x;
    _y = box._y;
    _z = box._z;
    _length = box._length;
    _width = box._width;
    _height = box._height;
}

void LogicBox::setBox(const AbstractPosedDesign *design)
{
    if (design == NULL)
        setBoxNull();
    else
    {
        const AbstractPosition* pos = design->getPosition();
        _x = pos->getX();
        _y = pos->getY();
        _z = pos->getZ();

        _length = design->getLength();
        _width = design->getWidth();
        _height = design->getHeight();
    }
}

void LogicBox::setBox(const AbstractPosedDesign *a, const AbstractPosedDesign *b)
{
    setBox(a);
    *this -= LogicBox(b);
}

void LogicBox::setBoxNull()
{
    _x = 0;
    _y = 0;
    _z = 0;
    _length = 0;
    _width = 0;
    _height = 0;
}

bool LogicBox::isNull() const
{
    return (_length == 0) && (_width == 0) && (_height == 0);
}

bool LogicBox::isFlat() const
{
    return (_length == 0) || (_width == 0) || (_height == 0);
}

const LogicBox &LogicBox::operator =(const LogicBox &box)
{
    if (&box != this)
        setBox(box);

    return *this;
}

const LogicBox &LogicBox::operator -=(const LogicBox &box)
{
    SetDiffX(box);
    SetDiffY(box);
    SetDiffZ(box);
    return *this;
}

LogicBox operator -(const LogicBox &a, const LogicBox &b)
{
    LogicBox tmp = a;
    tmp -= b;
    return tmp;
}

void LogicBox::SetDiffX(const LogicBox &box)
{
    int x = (getX() < box.getX())? box.getX(): getX();
    int gl = (getGlobalLength() < box.getGlobalLength())? getGlobalLength(): box.getGlobalLength();
    int l = gl-x;

    if (l < 0)
    {
        _x = 0;
        _length = 0;
    }
    else
    {
        _x = x;
        _length = l;
    }
}

void LogicBox::SetDiffY(const LogicBox &box)
{
    int y = (getY() < box.getY())? box.getY(): getY();
    int gw = (getGlobalWidth() < box.getGlobalWidth())? getGlobalWidth(): box.getGlobalWidth();
    int w = gw-y;

    if (w < 0)
    {
        _y = 0;
        _width = 0;
    }
    else
    {
        _y = y;
        _width = w;
    }
}

void LogicBox::SetDiffZ(const LogicBox &box)
{
    int z = (getZ() < box.getZ())? box.getZ(): getZ();
    int gh = (getGlobalHeight() < box.getGlobalHeight())? getGlobalHeight(): box.getGlobalHeight();
    int h = gh-z;

    if (h < 0)
    {
        _z = 0;
        _height = 0;
    }
    else
    {
        _z = z;
        _height = h;
    }
}
