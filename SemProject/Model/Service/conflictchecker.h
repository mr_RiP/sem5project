#ifndef CONFLICTCHECKER_H
#define CONFLICTCHECKER_H

#include "abstractchecker.h"
#include "logicbox.h"

class ConflictChecker : public AbstractChecker
{
public:
    ConflictChecker();
    ConflictChecker(const AbstractPosedDesign* a, const AbstractPosedDesign* b);
    ~ConflictChecker();

    bool check();
    void setData(const AbstractPosedDesign *a, const AbstractPosedDesign *b);

private:
    const AbstractPosedDesign* _a;
    const AbstractPosedDesign* _b;

protected:
    bool DeepCheck(const LogicBox& box) const;
    bool PointCheck(int x, int y, int z) const;
};

#endif // CONFLICTCHECKER_H
