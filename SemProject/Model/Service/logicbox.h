#ifndef LOGICBOX_H
#define LOGICBOX_H

#include "../Abstract/abstractposeddesign.h"

class LogicBox
{
public:
    LogicBox();
    LogicBox(int x, int y, int z, int length, int width, int height);
    LogicBox(const LogicBox& box);
    LogicBox(const AbstractPosedDesign* design);
    LogicBox(const AbstractPosedDesign* a, const AbstractPosedDesign* b);
    ~LogicBox();

    int getX() const;
    int getY() const;
    int getZ() const;
    int getLength() const;
    int getWidth() const;
    int getHeight() const;
    int getMaxX() const;
    int getMaxY() const;
    int getMaxZ() const;
    int getGlobalLength() const;
    int getGlobalWidth() const;
    int getGlobalHeight() const;

    void setX(int value);
    void setY(int value);
    void setZ(int value);
    void setLength(int value);
    void setWidth(int value);
    void setHeight(int value);

    void setBox(const LogicBox& box);
    void setBox(const AbstractPosedDesign* design);
    void setBox(const AbstractPosedDesign* a, const AbstractPosedDesign* b);
    void setBoxNull();

    bool isNull() const;
    bool isFlat() const;

    const LogicBox& operator = (const LogicBox& box);
    const LogicBox& operator -= (const LogicBox& box);

    friend LogicBox operator - (const LogicBox& a, const LogicBox& b);

private:
    int _x;
    int _y;
    int _z;
    int _length;
    int _width;
    int _height;

protected:
    void SetDiffX(const LogicBox& box);
    void SetDiffY(const LogicBox& box);
    void SetDiffZ(const LogicBox& box);
};

#endif // LOGICBOX_H
