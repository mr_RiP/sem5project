#include "globalposeddesign.h"
#include "innercsswitcher.h"
#include <stdexcept>

GlobalPosedDesign::GlobalPosedDesign()
{
}

void GlobalPosedDesign::CreatePos(const AbstractPosition* global)
{
    if (global != NULL)
    {
        const AbstractPosition* subpos = _design->getPosition();
        _pos = global->clonePosition();
        _pos->setX(subpos->getX() + _pos->getX());
        _pos->setY(subpos->getY() + _pos->getY());
        _pos->setZ(subpos->getZ() + _pos->getZ());
        _pos->setRotation(subpos->getRotation() + _pos->getRotation());

        int rot = global->getRotation();
        if (rot > 0)
        {
            if (rot < 270)
                _pos->setX(_pos->getX() - InnerCSSwitcher::getLength(rot,_design) + 1);
            if (rot > 90)
                _pos->setY(_pos->getY() - InnerCSSwitcher::getWidth(rot,_design) + 1);
        }
    }
    else
    {
        _pos = _design->getPosition()->clonePosition();
    }
}

GlobalPosedDesign::GlobalPosedDesign(const AbstractPosedDesign *design, const AbstractPosition *global_pos)
{
    if (design == NULL)
        throw std::invalid_argument("Trying to create Global Posed Design from NULL design.");

    _design = design;
    CreatePos(global_pos);
}

GlobalPosedDesign::~GlobalPosedDesign()
{
    delete _pos;
}

int GlobalPosedDesign::getLength() const
{
    return InnerCSSwitcher::getLength(_pos->getRotation(), _design->getDesignCore());
}

int GlobalPosedDesign::getWidth() const
{
    return InnerCSSwitcher::getWidth(_pos->getRotation(), _design->getDesignCore());
}

int GlobalPosedDesign::getHeight() const
{
    return _design->getHeight();
}

bool GlobalPosedDesign::isUpperSpaceFree(int x, int y, int z) const
{
    InnerCSSwitcher(_design->getDesignCore(),_pos).switchCS(x,y,z);
    return _design->isUpperSpaceFree(x,y,z);
}

bool GlobalPosedDesign::isLowerSpaceFree(int x, int y, int z) const
{
    InnerCSSwitcher(_design->getDesignCore(),_pos).switchCS(x,y,z);
    return _design->isLowerSpaceFree(x,y,z);
}

bool GlobalPosedDesign::isMidSpaceFree(int x, int y, int z) const
{
    InnerCSSwitcher(_design->getDesignCore(),_pos).switchCS(x,y,z);
    return _design->isMidSpaceFree(x,y,z);
}

bool GlobalPosedDesign::isSimple() const
{
    return _design->isSimple();
}

bool GlobalPosedDesign::isCuttedUpper() const
{
    return _design->isCuttedUpper();
}

bool GlobalPosedDesign::isCuttedLower() const
{
    return _design->isCuttedLower();
}

AbstractDesignCore *GlobalPosedDesign::cloneCore() const
{
    return _design->cloneCore();
}

const AbstractPosition *GlobalPosedDesign::getPosition() const
{
    return _pos;
}

void GlobalPosedDesign::setPosition(const AbstractPosition &position)
{
    delete _pos;
    _pos = position.clonePosition();
}

const AbstractDesignCore *GlobalPosedDesign::getDesignCore() const
{
    return _design->getDesignCore();
}
