#include "innercsswitcher.h"

InnerCSSwitcher::InnerCSSwitcher(const AbstractDesignCore *design, const AbstractPosition *position)
{
    _design = design;
    _pos = position;
}

InnerCSSwitcher::InnerCSSwitcher()
{

}

void InnerCSSwitcher::Substract(int &x, int &y, int& z) const
{
    x -= _pos->getX();
    y -= _pos->getY();
    z -= _pos->getZ();
}

void InnerCSSwitcher::Rotate(int &x, int &y) const
{
    int old_x = x;
    int old_y = y;

    int rot = _pos->getRotation();
    if (rot > 0)
        rot -= 360;

    switch (rot)
    {
    case 0:
        break;
    case 90:
        x = _design->getWidth()-1 - old_y;
        y = old_x;
        break;
    case 180:
        x = _design->getLength()-1 - old_x;
        y = _design->getWidth()-1 - old_y;
        break;
    case 270:
        x = old_y;
        y = _design->getLength()-1 - old_x;
        break;
    default:
        break;
    }
}

InnerCSSwitcher::~InnerCSSwitcher()
{

}

void InnerCSSwitcher::switchCS(int &x, int &y, int &z) const
{
    Substract(x,y,z);
    Rotate(x,y);
}

int InnerCSSwitcher::getWidth(int rotation, const AbstractDesignCore *design)
{
    if ((rotation == 0) || (rotation == 180))
        return design->getWidth();
    else
        return design->getLength();
}

int InnerCSSwitcher::getLength(int rotation, const AbstractDesignCore *design)
{
    if ((rotation == 0) || (rotation == 180))
        return design->getLength();
    else
        return design->getWidth();
}
