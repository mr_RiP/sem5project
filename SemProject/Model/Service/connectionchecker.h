#ifndef CONNECTIONCHECKER_H
#define CONNECTIONCHECKER_H

#include "abstractchecker.h"
#include "logicbox.h"

class ConnectionChecker : public AbstractChecker
{
public:
    ConnectionChecker();
    ConnectionChecker(const AbstractPosedDesign* a, const AbstractPosedDesign* b);
    ~ConnectionChecker();

    bool check();
    void setData(const AbstractPosedDesign *a, const AbstractPosedDesign *b);

private:
    const AbstractPosedDesign* _a;
    const AbstractPosedDesign* _b;

protected:
    bool DeepCheck(const LogicBox& box) const;

    bool CheckX(const LogicBox& box) const;
    bool CheckY(const LogicBox& box) const;
    bool CheckZ(const LogicBox& box) const;

    bool PointCheckX(int x_a, int x_b, int y, int z) const;
    bool PointCheckY(int x, int y_a, int y_b, int z) const;
    bool PointCheckZ(int x, int y, int z, bool upper_floor_of_a) const;
};

#endif // CONNECTIONCHECKER_H
