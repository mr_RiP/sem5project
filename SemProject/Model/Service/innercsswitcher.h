#ifndef INNERCSSWITCHER_H
#define INNERCSSWITCHER_H

#include "../Abstract/abstractposition.h"
#include "../Abstract/abstractdesigncore.h"
#include <QPoint>

class InnerCSSwitcher
{
public:
    InnerCSSwitcher(const AbstractDesignCore* design, const AbstractPosition* position);
    ~InnerCSSwitcher();

    void switchCS(int& x, int& y, int& z) const;

    static int getWidth(int rotation, const AbstractDesignCore* design);
    static int getLength(int rotation, const AbstractDesignCore* design);

protected:
    InnerCSSwitcher();

    const AbstractDesignCore* _design;
    const AbstractPosition* _pos;

    void Substract(int& x, int& y, int &z) const;
    void Rotate(int& x, int& y) const;
};

#endif // INNERCSSWITCHER_H
