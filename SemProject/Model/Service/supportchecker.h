#ifndef SUPPORTCHECKER_H
#define SUPPORTCHECKER_H

#include "abstractchecker.h"
#include "logicbox.h"

class SupportChecker : public AbstractChecker
{
public:
    SupportChecker();
    SupportChecker(const AbstractPosedDesign* supporting, const AbstractPosedDesign* supported);
    ~SupportChecker();

    bool check();
    void setData(const AbstractPosedDesign *a, const AbstractPosedDesign *b);

private:
    const AbstractPosedDesign* _a;
    const AbstractPosedDesign* _b;

protected:
    bool DeepCheck(const LogicBox& box) const;

};

#endif // SUPPORTCHECKER_H
