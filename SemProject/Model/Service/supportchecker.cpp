#include "supportchecker.h"
#include <stddef.h>

SupportChecker::SupportChecker()
{
    _a = NULL;
    _b = NULL;
}

SupportChecker::SupportChecker(const AbstractPosedDesign* supporting, const AbstractPosedDesign* supported)
{
    setData(supporting,supported);
}

SupportChecker::~SupportChecker()
{
}

bool SupportChecker::check()
{
    if ((_a == NULL) || (_b == NULL))
        return false;

    if ((_a->getHeight() + _a->getPosition()->getZ()) == _b->getPosition()->getZ())
    {
        LogicBox box(_a,_b);
        if ((box.getLength() > 0) && (box.getWidth() > 0))
        {
            if (_a->isSimple() && _b->isSimple())
                return true;
            else
                return DeepCheck(box);
        }
    }

    return false;
}

void SupportChecker::setData(const AbstractPosedDesign *a, const AbstractPosedDesign *b)
{
    _a = a;
    _b = b;
}

bool SupportChecker::DeepCheck(const LogicBox &box) const
{
    int n = box.getGlobalLength();
    int m = box.getGlobalWidth();

    int z_b = _b->getPosition()->getZ();
    int z_a = z_b - 1;

    for (int i=box.getX(); i<n; i++)
        for (int j=box.getY(); j<m; j++)
            if (!_a->isUpperSpaceFree(i,j,z_a) && !_b->isLowerSpaceFree(i,j,z_b))
                return true;

    return false;
}

