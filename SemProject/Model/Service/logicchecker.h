#ifndef LOGICCHECKER_H
#define LOGICCHECKER_H

#include "../Abstract/abstractpart.h"
#include "abstractchecker.h"
#include <QList>

class LogicChecker
{
public:
    ~LogicChecker();

    static bool isPartsConflicting(const AbstractPart* a, const AbstractPart* b);
    static bool isDesignsConflicting(const AbstractPosedDesign* a, const AbstractPosedDesign* b);

    static bool isPartsConnected(const AbstractPart* a, const AbstractPart* b);
    static bool isDesignsConnected(const AbstractPosedDesign* a, const AbstractPosedDesign* b);

    static bool isPartASupportingB(const AbstractPart* a, const AbstractPart* b);
    static bool isDesignASupportingB(const AbstractPosedDesign* a, const AbstractPosedDesign* b);

    static bool isPartsListConnected(const QList<const AbstractPart*>& list);
    static bool isSubdesignListConnected(const QList<AbstractSubdesign*>& list);
    static bool isSubdesignListNotConflicting(const QList<AbstractSubdesign*>& list);

protected:
    LogicChecker();
    static bool PartCheck(const AbstractPart* a, const AbstractPart* b, AbstractChecker& checker);

    static bool DoubleCompositeCheck(const AbstractPart* a, const AbstractPart* b, AbstractChecker& checker);
    static bool FirstCompositeCheck(const AbstractPart* a, const AbstractPart* b, AbstractChecker& checker);
    static bool SecondCompositeCheck(const AbstractPart* a, const AbstractPart* b, AbstractChecker& checker);
    static bool NonCompositeCheck(const AbstractPart* a, const AbstractPart* b, AbstractChecker& checker);
};

#endif // LOGICCHECKER_H
