#include "conflictchecker.h"
#include "logicbox.h"
#include <stddef.h>

ConflictChecker::ConflictChecker(const AbstractPosedDesign *a, const AbstractPosedDesign *b)
{
    setData(a,b);
}

void ConflictChecker::setData(const AbstractPosedDesign *a, const AbstractPosedDesign *b)
{
    _a = a;
    _b = b;
}

bool ConflictChecker::DeepCheck(const LogicBox &box) const
{
    if (!box.isFlat()) // надо удалить
    {
        int n = box.getGlobalLength();
        int m = box.getGlobalWidth();
        int l = box.getGlobalHeight();

        for (int i=box.getX(); i<n; i++)
            for (int j=box.getY(); j<m; j++)
                for (int k=box.getZ(); k<l; k++)
                    if (PointCheck(i,j,k))
                        return true;
    }
    return false;
}

bool ConflictChecker::PointCheck(int x, int y, int z) const
{
    return (!_a->isUpperSpaceFree(x,y,z) && !_b->isUpperSpaceFree(x,y,z)) ||
            (!_a->isLowerSpaceFree(x,y,z) && !_b->isLowerSpaceFree(x,y,z)) ||
            (!_a->isMidSpaceFree(x,y,z) && !_b->isMidSpaceFree(x,y,z));
}

ConflictChecker::ConflictChecker()
{
    _a = NULL;
    _b = NULL;
}

ConflictChecker::~ConflictChecker()
{
}

bool ConflictChecker::check()
{
    LogicBox conflict_space(_a,_b);
    if (conflict_space.isFlat())
        return false;
    else
    {
        if (_a->isSimple() && _b->isSimple())
            return true;
        else
            return DeepCheck(conflict_space);
    }
}
