#include "logicchecker.h"
#include "globalposeddesign.h"
#include "supportchecker.h"
#include "conflictchecker.h"
#include "connectionchecker.h"

LogicChecker::LogicChecker()
{

}

LogicChecker::~LogicChecker()
{

}

bool LogicChecker::isPartsConflicting(const AbstractPart *a, const AbstractPart *b)
{
    ConflictChecker checker;
    return PartCheck(a,b,checker);
}

bool LogicChecker::isDesignsConflicting(const AbstractPosedDesign *a, const AbstractPosedDesign *b)
{
    return ConflictChecker(a,b).check();
}

bool LogicChecker::isPartsConnected(const AbstractPart *a, const AbstractPart *b)
{
    ConnectionChecker checker;
    return PartCheck(a,b,checker);
}

bool LogicChecker::isDesignsConnected(const AbstractPosedDesign *a, const AbstractPosedDesign *b)
{
    return ConnectionChecker(a,b).check();
}

bool LogicChecker::isPartASupportingB(const AbstractPart *a, const AbstractPart *b)
{
    SupportChecker checker;
    return PartCheck(a,b,checker);
}

bool LogicChecker::isDesignASupportingB(const AbstractPosedDesign *a, const AbstractPosedDesign *b)
{
    return SupportChecker(a,b).check();
}

bool LogicChecker::isPartsListConnected(const QList<const AbstractPart *> &list)
{
    int num = list.size();
    bool connected[num];
    for (int i=0; i<num; i++)
        connected[i] = false;
    QList<int> quene;

    int counter = 1;
    connected[0] = true;
    quene.append(0);

    while ((!quene.isEmpty()) && (counter != num))
    {
        for (int i=0; i<num; i++)
            if ((!connected[i]) && (isPartsConnected(list[i],list[quene.first()])))
            {
                connected[i] = true;
                quene.append(i);
                counter++;
            }
        quene.removeFirst();
    }

    return counter == num;
}

bool LogicChecker::isSubdesignListConnected(const QList<AbstractSubdesign *> &list)
{
    int num = list.size();
    bool connected[num];
    for (int i=0; i<num; i++)
        connected[i] = false;
    QList<int> quene;

    int counter = 1;
    connected[0] = true;
    quene.append(0);

    while ((!quene.isEmpty()) && (counter != num))
    {
        for (int i=0; i<num; i++)
            if ((!connected[i]) && (isDesignsConnected(list[i],list[quene.first()])))
            {
                connected[i] = true;
                quene.append(i);
                counter++;
            }
        quene.removeFirst();
    }

    return counter == num;
}

bool LogicChecker::isSubdesignListNotConflicting(const QList<AbstractSubdesign *> &list)
{
    int n = list.size() - 1;
    int m = list.size();
    for (int i=0; i<n; i++)
        for (int j=i+1; j<m; j++)
            if (isDesignsConflicting(list[i],list[j]))
                return false;
    return true;
}

bool LogicChecker::PartCheck(const AbstractPart *a, const AbstractPart *b, AbstractChecker &checker)
{
    bool a_composite = a->isComposite();
    bool b_composite = b->isComposite();

    if (a_composite && b_composite)
        return DoubleCompositeCheck(a,b,checker);
    else
    {
        if (!a_composite && !b_composite)
            return NonCompositeCheck(a,b,checker);
        else
        {
            if (a_composite && !b_composite)
                return FirstCompositeCheck(a,b,checker);
            else
                return SecondCompositeCheck(a,b,checker);
        }
    }
}

bool LogicChecker::DoubleCompositeCheck(const AbstractPart *a, const AbstractPart *b, AbstractChecker &checker)
{
    int n = a->getDetail()->getSubdesignsNum();
    int m = b->getDetail()->getSubdesignsNum();

    for (int i=0; i<n; i++)
    {
        GlobalPosedDesign sub_a(a->getDetail()->getSubdesign(i),a->getPosition());
        for (int j=0; j<m; j++)
        {
            GlobalPosedDesign sub_b(a->getDetail()->getSubdesign(i),a->getPosition());
            checker.setData(&sub_a,&sub_b);
            if (checker.check())
                return true;
        }
    }

    return false;
}

bool LogicChecker::FirstCompositeCheck(const AbstractPart *a, const AbstractPart *b, AbstractChecker &checker)
{
    int n = a->getDetail()->getSubdesignsNum();

    for (int i=0; i<n; i++)
    {
        GlobalPosedDesign sub_a(a->getDetail()->getSubdesign(i),a->getPosition());
        checker.setData(&sub_a, b);
        if (checker.check())
            return true;
    }
    return false;
}

bool LogicChecker::SecondCompositeCheck(const AbstractPart *a, const AbstractPart *b, AbstractChecker &checker)
{
    int n = b->getDetail()->getSubdesignsNum();

    for (int i=0; i<n; i++)
    {
        GlobalPosedDesign sub_b(b->getDetail()->getSubdesign(i),b->getPosition());
        checker.setData(a, &sub_b);
        if (checker.check())
            return true;
    }
    return false;
}

bool LogicChecker::NonCompositeCheck(const AbstractPart *a, const AbstractPart *b, AbstractChecker &checker)
{
    checker.setData(a,b);
    return checker.check();
}
