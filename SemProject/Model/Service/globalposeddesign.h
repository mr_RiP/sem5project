#ifndef GLOBALPOSEDSUBDESIGN_H
#define GLOBALPOSEDSUBDESIGN_H

#include "../Abstract/abstractposeddesign.h"

class GlobalPosedDesign : public AbstractPosedDesign
{
public:
    GlobalPosedDesign(const AbstractPosedDesign* design, const AbstractPosition* global_pos);
    ~GlobalPosedDesign();

    int getLength() const;
    int getWidth() const;
    int getHeight() const;
    bool isUpperSpaceFree(int x, int y, int z) const;
    bool isLowerSpaceFree(int x, int y, int z) const;
    bool isMidSpaceFree(int x, int y, int z) const;
    bool isSimple() const;
    bool isCuttedUpper() const;
    bool isCuttedLower() const;

    AbstractDesignCore *cloneCore() const;

    const AbstractPosition* getPosition() const;
    void setPosition(const AbstractPosition &position);
    const AbstractDesignCore *getDesignCore() const;

private:
    const AbstractPosedDesign* _design;
    AbstractPosition* _pos;

    void CreatePos(const AbstractPosition* global);

protected:
    GlobalPosedDesign();


};

#endif // GLOBALPOSEDSUBDESIGN_H
