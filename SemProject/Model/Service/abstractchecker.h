#ifndef ABSTRACTCHECKER
#define ABSTRACTCHECKER

#include "../Abstract/abstractposeddesign.h"

class AbstractChecker
{
public:
    virtual ~AbstractChecker() {}

    virtual bool check() = 0;
    virtual void setData(const AbstractPosedDesign *a, const AbstractPosedDesign *b) = 0;
};

#endif // ABSTRACTCHECKER

