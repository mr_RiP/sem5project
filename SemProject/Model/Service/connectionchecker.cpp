#include "connectionchecker.h"
#include <stddef.h>

ConnectionChecker::ConnectionChecker()
{
    _a = NULL;
    _b = NULL;
}

ConnectionChecker::ConnectionChecker(const AbstractPosedDesign* a, const AbstractPosedDesign* b)
{
    setData(a,b);
}

ConnectionChecker::~ConnectionChecker()
{
}

bool ConnectionChecker::check()
{
    LogicBox box(_a,_b);
    if (box.isNull() || !box.isFlat())
        return false;
    else
    {
        if (_a->isSimple() && _b->isSimple())
            return true;
        else
            return DeepCheck(box);
    }
}

void ConnectionChecker::setData(const AbstractPosedDesign *a, const AbstractPosedDesign *b)
{
    _a = a;
    _b = b;
}

bool ConnectionChecker::DeepCheck(const LogicBox &box) const
{
    if (box.getLength() == 0)
        return CheckX(box);
    else
    {
        if (box.getWidth() == 0)
            return CheckY(box);
        else
            return CheckZ(box);
    }
}

bool ConnectionChecker::CheckX(const LogicBox &box) const
{
    int x_a = _a->getPosition()->getX();
    int x_b = _b->getPosition()->getX();

    if ((x_a + _a->getLength()) == x_b)
        x_a = x_b - 1;
    else
        x_b = x_a - 1;

    int n = box.getGlobalWidth();
    int m = box.getGlobalHeight();

    for (int i=box.getY(); i<n; i++)
        for (int j=box.getZ(); j<m; j++)
            if (PointCheckX(x_a, x_b, i, j))
                return true;

    return false;
}

bool ConnectionChecker::CheckY(const LogicBox &box) const
{
    int y_a = _a->getPosition()->getY();
    int y_b = _b->getPosition()->getY();

    if ((y_a + _a->getWidth()) == y_b)
        y_a = y_b - 1;
    else
        y_b = y_a - 1;

    int n = box.getGlobalLength();
    int m = box.getGlobalHeight();

    for (int i=box.getX(); i<n; i++)
        for (int j=box.getZ(); j<m; j++)
            if (PointCheckY(i, y_a, y_b, j))
                return true;

    return false;
}

bool ConnectionChecker::CheckZ(const LogicBox &box) const
{
    bool upper_floor_of_a = (_a->getPosition()->getZ() + _a->getHeight()) == _b->getPosition()->getZ();

    int z = (upper_floor_of_a)? _b->getPosition()->getZ()-1: _a->getPosition()->getZ()-1;

    int n = box.getGlobalLength();
    int m = box.getGlobalWidth();

    for (int i=box.getX(); i<n; i++)
        for (int j=box.getY(); j<m; j++)
            if (PointCheckZ(i, j, z, upper_floor_of_a))
                return true;

    return false;
}

bool ConnectionChecker::PointCheckX(int x_a, int x_b, int y, int z) const
{
    return (!_a->isUpperSpaceFree(x_a,y,z) && !_b->isUpperSpaceFree(x_b,y,z)) ||
            (!_a->isLowerSpaceFree(x_a,y,z) && !_b->isLowerSpaceFree(x_b,y,z)) ||
            (!_a->isMidSpaceFree(x_a,y,z) && !_b->isMidSpaceFree(x_b,y,z));
}

bool ConnectionChecker::PointCheckY(int x, int y_a, int y_b, int z) const
{
    return (!_a->isUpperSpaceFree(x,y_a,z) && !_b->isUpperSpaceFree(x,y_b,z)) ||
            (!_a->isLowerSpaceFree(x,y_a,z) && !_b->isLowerSpaceFree(x,y_b,z)) ||
            (!_a->isMidSpaceFree(x,y_a,z) && !_b->isMidSpaceFree(x,y_b,z));
}

bool ConnectionChecker::PointCheckZ(int x, int y, int z, bool upper_floor_of_a) const
{
    if (upper_floor_of_a)
        return !_a->isUpperSpaceFree(x,y,z) && !_b->isLowerSpaceFree(x,y,z+1);
    else
        return !_a->isLowerSpaceFree(x,y,z+1) && !_b->isUpperSpaceFree(x,y,z);
}
