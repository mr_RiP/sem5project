#include "simpledesign.h"
#include <stdexcept>

SimpleDesign::SimpleDesign()
{

}

SimpleDesign::SimpleDesign(int length, int width, int height)
{
    if ((length <= 0) || (width <= 0) || (height <= 0))
        throw std::invalid_argument("Trying to create irreal design.");
    _length = length;
    _width = width;
    _height = height;
}

SimpleDesign::SimpleDesign(int length, int width, int height, const QString &name)
{
    if ((length <= 0) || (width <= 0) || (height <= 0))
        throw std::invalid_argument("Trying to create irreal design.");
    _length = length;
    _width = width;
    _height = height;

    _name = name;
}

int SimpleDesign::getHeight() const
{
    return _height;
}

int SimpleDesign::getLength() const
{
    return _length;
}

int SimpleDesign::getWidth() const
{
    return _width;
}

bool SimpleDesign::IsSpaceFree(int x, int y, int z) const
{
    if (((x < 0) || (x >= _length)) &&
            ((y < 0) || (y >= _width)) &&
            ((z < 0) || (z >= _height)))
        return true;
    else
        return false;
}

bool SimpleDesign::isLowerSpaceFree(int x, int y, int z) const
{
    return IsSpaceFree(x,y,z);
}

bool SimpleDesign::isUpperSpaceFree(int x, int y, int z) const
{
    return IsSpaceFree(x,y,z);
}

bool SimpleDesign::isMidSpaceFree(int x, int y, int z) const
{
    return IsSpaceFree(x,y,z);
}

AbstractDesign* SimpleDesign::cloneDesign() const
{
    return new SimpleDesign(_length,_width,_height,_name);
}

bool SimpleDesign::isSimple() const
{
    return true;
}

bool SimpleDesign::isCuttedUpper() const
{
    return false;
}

bool SimpleDesign::isCuttedLower() const
{
    return false;
}

const AbstractSubdesign* SimpleDesign::getSubdesign(int) const
{
    throw std::invalid_argument("Trying to get subdesign from non-composite design");
    return 0;
}

int SimpleDesign::getSubdesignsNum() const
{
    return 0;
}

void SimpleDesign::setName(const QString& name)
{
    _name = name;
}

const QString &SimpleDesign::getName() const
{
    return _name;
}

AbstractDesignCore* SimpleDesign::cloneCore() const
{
    return new SimpleDesign(_length,_width,_height);
}

