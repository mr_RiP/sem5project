#ifndef SIMPLEDESIGN_H
#define SIMPLEDESIGN_H

#include "Abstract/abstractdesign.h"

class SimpleDesign : public AbstractDesign
{
public:
    SimpleDesign(int length, int width, int height);
    SimpleDesign(int length, int width, int height, const QString& name);
    int getLength() const;
    int getWidth() const;
    int getHeight() const;
    bool isUpperSpaceFree(int x, int y, int z) const;
    bool isLowerSpaceFree(int x, int y, int z) const;
    bool isMidSpaceFree(int x, int y, int z) const;
    bool isSimple() const;
    bool isCuttedUpper() const;
    bool isCuttedLower() const;
    AbstractDesignCore* cloneCore() const;
    AbstractDesign* cloneDesign() const;
    const AbstractSubdesign* getSubdesign(int) const;
    int getSubdesignsNum() const;
    void setName(const QString& name);
    const QString& getName() const;

protected:
    SimpleDesign();
    bool IsSpaceFree(int x, int y, int z) const;

    int _length;
    int _height;
    int _width;

    QString _name;
};

#endif // SIMPLEDESIGN_H
