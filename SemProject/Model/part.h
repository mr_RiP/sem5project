#ifndef PART_H
#define PART_H

#include "Abstract/abstractpart.h"
#include "Service/abstractchecker.h"

class Part : public AbstractPart
{
public:
    Part(const AbstractDetail *detail, const AbstractPosition *position);
    ~Part();

    const AbstractDetail* getDetail() const;
    bool isConnectedWith(const AbstractPart* part) const;
    bool isSupportedBy(const AbstractPart* part) const;
    bool isSupportFor(const AbstractPart* part) const;
    bool isConflictingWith(const AbstractPart* part) const;

    bool isComposite() const;

    const AbstractPosition* getPosition() const;
    void setPosition(const AbstractPosition& position);
    const AbstractDesignCore* getDesignCore() const;

    int getLength() const;
    int getWidth() const;
    int getHeight() const;
    bool isUpperSpaceFree(int x, int y, int z) const;
    bool isLowerSpaceFree(int x, int y, int z) const;
    bool isMidSpaceFree(int x, int y, int z) const;
    bool isSimple() const;
    bool isCuttedUpper() const;
    bool isCuttedLower() const;

    AbstractDesignCore* cloneCore() const;

    AbstractPart* clonePart() const;

protected:
    const AbstractDetail* _detail;
    AbstractPosition* _pos;
};

#endif // PART_H
