#-------------------------------------------------
#
# Project created by QtCreator 2015-09-07T18:35:45
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SemProject
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    Model/cutteddesign.cpp \
    Model/detail.cpp \
    Model/position.cpp \
    Model/simpledesign.cpp \
    Model/model.cpp \
    Model/part.cpp \
    Model/Service/innercsswitcher.cpp \
    Model/Service/connectionchecker.cpp \
    Model/Service/conflictchecker.cpp \
    Model/Service/supportchecker.cpp \
    View/geometricmodel.cpp \
    View/block.cpp \
    View/Service/blockfactory.cpp \
    View/Service/pointscreator.cpp \
    View/face.cpp \
    View/graphicmodel.cpp \
    View/Abstract/abstractface.cpp \
    View/Abstract/abstractblock.cpp \
    View/camera.cpp \
    View/view.cpp \
    Controller/modelscreen.cpp \
    View/Service/zerofloorpainter.cpp \
    View/anchormark.cpp \
    Model/Abstract/abstractposition.cpp \
    Controller/newdesigndialog.cpp \
    Controller/newdetaildialog.cpp \
    Controller/modelpalette.cpp \
    Controller/modelpalettewidget.cpp \
    Controller/designpalettecontroller.cpp \
    Controller/Service/colorbox.cpp \
    Controller/detailpalettecontroller.cpp \
    Controller/settingswidget.cpp \
    Controller/settingstable.cpp \
    Controller/toolboxwidget.cpp \
    View/Service/facescreator.cpp \
    View/modelpainter.cpp \
    View/Service/facepainter.cpp \
    Controller/selectiontool.cpp \
    Controller/detailplacementtool.cpp \
    Controller/Service/cameraactions.cpp \
    Model/Service/globalposeddesign.cpp \
    Model/Service/logicbox.cpp \
    Model/Service/logicchecker.cpp \
    Controller/filemenu.cpp \
    Controller/Service/modelxmlwriter.cpp \
    Controller/Service/modelxmlreader.cpp

HEADERS  += mainwindow.h \
    Model/Abstract/abstractdesign.h \
    Model/Abstract/abstractdesigncore.h \
    Model/Abstract/abstractpart.h \
    Model/Abstract/abstractposition.h \
    Model/Abstract/abstractsubdesign.h \
    Model/cutteddesign.h \
    Model/detail.h \
    Model/position.h \
    Model/simpledesign.h \
    Model/Abstract/abstractdetail.h \
    Model/Abstract/abstractmodel.h \
    Model/model.h \
    Model/part.h \
    Model/Service/innercsswitcher.h \
    Model/Abstract/abstractposeddesign.h \
    Model/Service/connectionchecker.h \
    Model/Service/conflictchecker.h \
    Model/Service/supportchecker.h \
    Model/Service/abstractchecker.h \
    View/Abstract/abstractgeometricmodel.h \
    View/Abstract/abstractblock.h \
    View/Abstract/abstractcamera.h \
    View/geometricmodel.h \
    View/block.h \
    View/Service/blockfactory.h \
    View/Service/personalspace.h \
    View/Service/pointscreator.h \
    View/Abstract/abstractface.h \
    View/face.h \
    View/Abstract/abstractgraphicmodel.h \
    View/graphicmodel.h \
    View/Abstract/abstractview.h \
    View/view.h \
    Controller/modelscreen.h \
    View/Service/zerofloorpainter.h \
    View/anchormark.h \
    Controller/Abstract/abstracttool.h \
    Controller/Abstract/abstractfilemanager.h \
    Controller/newdesigndialog.h \
    Controller/newdetaildialog.h \
    Controller/modelpalette.h \
    Controller/Abstract/abstractpalettecontroller.h \
    Controller/modelpalettewidget.h \
    Controller/designpalettecontroller.h \
    Controller/Service/colorbox.h \
    Controller/detailpalettecontroller.h \
    Controller/settingswidget.h \
    Controller/settingstable.h \
    Controller/toolboxwidget.h \
    View/camera.h \
    View/Service/facescreator.h \
    View/modelpainter.h \
    View/Abstract/abstractmodelpainter.h \
    View/Template/buffer.h \
    View/Service/facepainter.h \
    Controller/selectiontool.h \
    Controller/detailplacementtool.h \
    Controller/Service/cameraactions.h \
    Model/Service/globalposeddesign.h \
    Model/Service/logicbox.h \
    Model/Service/logicchecker.h \
    Controller/filemenu.h \
    Controller/Service/modelxmlwriter.h \
    Controller/Service/modelxmlreader.h

FORMS    += mainwindow.ui \
    Controller/newdesigndialog.ui \
    Controller/newdetaildialog.ui \
    Controller/modelpalettewidget.ui \
    Controller/settingswidget.ui \
    Controller/toolboxwidget.ui \
    Controller/detailplacementtool.ui \
    Controller/selectiontool.ui
