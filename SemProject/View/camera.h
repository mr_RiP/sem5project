#ifndef CAMERA_H
#define CAMERA_H

#include "Abstract/abstractcamera.h"

class Camera : public AbstractCamera
{
public:
    Camera();
    Camera(const QVector3D& focus_point, const QVector3D& observer_point, float scale);
    ~Camera();

    const QVector3D& getFocusPoint() const;

    void setFocusPoint(const QVector3D& focus_point);
    void setObserverPoint(const QVector3D& observer_point);

    bool isNull() const;

    const QVector3D& getVectorX() const;
    const QVector3D& getVectorY() const;
    const QVector3D& getVectorZ() const;

    void rotateX(int deg);
    void rotateY(int deg);

    float getScale() const;
    void setScale(float scale);

    void setOffset(QPoint offset);
    QPoint getOffset() const;

    QVector3D toView(const QVector3D& geometric_model_point) const;
    QVector3D toModel(const QVector3D& graphic_model_point) const;

    void switchToView(QVector3D& point) const;
    void switchToModel(QVector3D& point) const;

    QVector3D toDifferentScale(const QVector3D& point, float new_scale) const;
    void switchToDifferentScale(QVector3D& point, float new_scale) const;

    QVector3D toDifferentOffset(const QVector3D& point, const QPoint& new_offset) const;
    void switchToDifferentOffset(QVector3D& point, const QPoint& new_offset) const;

private:
    QVector3D _focus;
    QVector3D _x;
    QVector3D _y;
    QVector3D _z;
    QVector3D _offset;
    float _scale;

    float _z_turn;
    float _x_turn;

protected:
    void CreateVectorZ(const QVector3D& observer_point);
    void CreateVectorX();
    void CreateVectorY();

    float DegToRad(int deg) const;
    float GetXTurn() const;
    void RotateCamY(int deg);
    void RotateCamX(int deg);

    void SetZTurn();
    void SetXTurn();


    void TurnZ(QVector3D& point, bool back=false) const;
    void TurnX(QVector3D& point, bool back=false) const;

    void SetSwitcherModifiers();
    void SetVectorsByTurns();
    bool zIsVertical() const;
    void SwitchVectorToModel(QVector3D& vector) const;
    void SetDefaultVectors();
};

/*
 * Алгоритм перехода к графической модели
 * 1. Смещение центра координат к фокусу_камеры
 * 2. Поворот по оси z, на угол между y и проекцией z_камеры на Oxy
 * 3. Поворот по оси x, на угол между z и z_камеры
 * 4. Умножение координат на масштаб
 */

#endif // CAMERA_H
