#include "pointscreator.h"
#include <stdexcept>
#include "../../Model/Service/globalposeddesign.h"
#include <cstdio>

void PointsCreator::create(QList<QVector3D *> &points, const AbstractPart *part)
{
    if (part == NULL)
        throw std::invalid_argument("Trying to create points of NULL part.");

    points.clear();

    if (part->isComposite())
        CreatePointsOfComposite(points,part);
    else
        if (part->isSimple())
            CreatePointsOfSimple(points,part);
        else
            CreatePointsOfCutted(points,part);

}

int PointsCreator::getPointDataIndex(const QList<QVector3D *> &points, const QVector3D &point)
{
    int n = points.size();
    for (int i=0; i<n; i++)
        if (*(points[i]) == point)
            return i;
    return -1;
}

void PointsCreator::createBoxPoints(QList<QVector3D *> &points, const AbstractPart *part)
{
    if (part == NULL)
        throw std::invalid_argument("Trying to create selection box points of NULL part.");

    CreatePointsOfSimple(points,part);
}

void PointsCreator::CreatePointsOfComposite(QList<QVector3D *> &points, const AbstractPart *part)
{
    const AbstractDesign* design = part->getDetail()->getDesign();
    const AbstractPosition* pos = part->getPosition();
    int n = design->getSubdesignsNum();
    for (int i=0; i<n; i++)
    {
        const AbstractSubdesign* subdesign = design->getSubdesign(i);
        if (subdesign->isSimple())
            CreatePointsOfSimple(points,subdesign,pos);
        else
            CreatePointsOfCutted(points,subdesign,pos);
    }
}

void PointsCreator::CreatePointsOfSimple(QList<QVector3D *> &points, const AbstractPosedDesign *part,
                                         const AbstractPosition *global_pos)
{   
    GlobalPosedDesign tmp(part,global_pos);
    const AbstractPosition* pos = tmp.getPosition();
    float x = pos->getX();
    float y = pos->getY();
    float z = pos->getZ();
    float length = tmp.getLength();
    float width = tmp.getWidth();
    float height = tmp.getHeight();

    try
    {
        points.append(new QVector3D(x,y,z));                        //  Индексы точек
        points.append(new QVector3D(x+length,y,z));                 // 3-2              y
        points.append(new QVector3D(x+length,y+width,z));           // | |  при z = 0   L x
        points.append(new QVector3D(x,y+width,z));                  // 0-1
        points.append(new QVector3D(x,y,z+height));                 //
        points.append(new QVector3D(x+length,y,z+height));          // 7-6                    y
        points.append(new QVector3D(x+length,y+width,z+height));    // | |  при z = height    L x
        points.append(new QVector3D(x,y+width,z+height));           // 4-5
    }
    catch (std::exception &err)
    {
        ClearPointsList(points);
        throw err;
    }

}

void PointsCreator::CreatePointsOfCutted(QList<QVector3D *> &points, const AbstractPosedDesign *part, const AbstractPosition *global_pos)
{
    CreatePointsOfSimple(points,part,global_pos);
    GlobalPosedDesign tmp(part,global_pos);

    const AbstractDesignCore* design = tmp.getDesignCore();
    int rot = tmp.getPosition()->getRotation();
    int side = GetSideIndexOffset(rot);
    int type = GetCutTypeIndexOffset(tmp.getDesignCore());

    int i = points.size() - 8 + side + type;
    int j = (side != 3)? i+1: type;

    switch (rot)
    {
    case 0:
        points[i]->setX(points[i]->x() - design->getLength() + 1);
        points[j]->setX(points[i]->x());
        break;
    case 90:
        points[i]->setY(points[i]->y() - design->getLength() + 1);
        points[j]->setY(points[i]->y());
        break;
    case 180:
        points[i]->setX(points[i]->x() + design->getLength() - 1);
        points[j]->setX(points[i]->x());
        break;
    case 270:       
        points[i]->setY(points[i]->y() + design->getLength() - 1);
        points[j]->setY(points[i]->y());
        break;
    default:
        throw std::invalid_argument("Trying to create points of cutted design with invalid rotation parameter.");
    }
}

void PointsCreator::ClearPointsList(QList<QVector3D *> &points)
{
    int n = points.size();
    for (int i=0; i<n; i++)
        delete points[i];
    points.clear();
}

int PointsCreator::GetCorrectRotation(int rot)
{
    if (rot > 270)
    {
        int mod = rot / 360;
        rot -= mod * 360;
    }
    return rot;
}

int PointsCreator::GetSideIndexOffset(int rot)
{
    return (rot == 270)? 0: rot/90 + 1;
}

int PointsCreator::GetCutTypeIndexOffset(const AbstractDesignCore *design)
{

    return (design->isCuttedLower())? 0: 4;
}
