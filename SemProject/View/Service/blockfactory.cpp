#include "blockfactory.h"
#include "../block.h"
#include "../face.h"
#include "pointscreator.h"
#include "facescreator.h"

#ifdef DEBUG
#include <QFile>
#include <QString>

void LogString(const QString& text)
{
    QFile logfile("DEBUG_LOG_BlockFactory.txt");
    logfile.open(QIODevice::Append);
    logfile.write(text.toUtf8());
    logfile.close();
}

#endif

BlockFactory::BlockFactory(QList<QVector3D *> *points, QList<AbstractFace *> *faces, QList<AbstractBlock *> *blocks)
{
    _points = points;
    _faces = faces;
    _blocks = blocks;
}

BlockFactory::~BlockFactory()
{

}

void BlockFactory::createBlock(const AbstractPart* part)
{
#ifdef DEBUG
    LogString("Block Factory initialized.\n");
#endif

    QList<int> index_list;
    CreatePoints(index_list, part);
    CreateFaces(index_list);
    CreateBlock(index_list, part);
}

void BlockFactory::CreatePoints(QList<int> &index_list, const AbstractPart *part)
{
    QList<QVector3D*> points;
    PointsCreator::create(points, part);

#ifdef DEBUG
    LogString(QString("New Points [%1]\n").arg(points.size()));
#endif

    SetPointsIndexList(index_list, points);

#ifdef DEBUG
    LogString(QString("Overall Points [%1]\n").arg(_points->size()));
#endif
}

void BlockFactory::CreateFaces(QList<int>& index_list)
{
    QList<AbstractFace*> faces;
    FacesCreator::create(faces, index_list);

#ifdef DEBUG
    LogString(QString("New Faces [%1]\n").arg(faces.size()));
#endif

    SetFacesIndexList(index_list, faces);

#ifdef DEBUG
    LogString(QString("Overall Faces [%1]\n").arg(_faces->size()));
#endif
}

void BlockFactory::CreateBlock(QList<int> &index_list, const AbstractPart *part)
{
    _blocks->append(new Block(index_list,part));

#ifdef DEBUG
    LogString("Block Added.\n\n");
#endif
}

void BlockFactory::SetPointsIndexList(QList<int> &index_list, QList<QVector3D *> &new_points)
{
    index_list.clear();

    int n = new_points.size();
    for (int i=0; i<n; i++)
    {
        int index = PointsCreator::getPointDataIndex(*_points, *(new_points[i]));
        if (index < 0)
        {
            index_list.append(_points->size());
            _points->append(new_points[i]);
        }
        else
        {
            index_list.append(index);
            delete new_points[i];
        }
    }
}

void BlockFactory::SetFacesIndexList(QList<int> &index_list, QList<AbstractFace *> &new_faces)
{
    index_list.clear();

    int n = new_faces.size();
    for (int i=0; i<n; i++)
    {
        int index = FacesCreator::getEqualFaceIndex(*_faces, *(new_faces[i]));
        if (index < 0)
        {
            index_list.append(_faces->size());
            _faces->append(new_faces[i]);
        }
        else
        {
            index_list.append(index);
            delete new_faces[i];
        }
    }
}
