#ifndef FACEPAINTER_H
#define FACEPAINTER_H

#include <QImage>
#include <QVector3D>
#include <QColor>
#include "../Template/buffer.h"

class FacePainter
{
public:
    FacePainter(QImage& image, Buffer<float>& z_buffer, Buffer<int>& block_buffer, Buffer<bool>& anchor);

    void setBlockData(int index, const QColor& color);
    void draw(const QVector3D& a, const QVector3D& b, const QVector3D& c, bool horizontal);

private:
    QImage* _img;
    Buffer<float>* _z;
    Buffer<int>* _block;
    Buffer<bool>* _anchor;

    int _index;
    QColor _color;
    QList<QVector3D> _list;
    bool _horizontal;

protected:
    FacePainter();
    void SetNullBlockData();

    bool FaceIsInvisible(const QVector3D& a, const QVector3D& b, const QVector3D& c) const;
    bool PointIsInvisible(const QVector3D& point, const QSize& size) const;

    bool FaceIsDegenerate(const QVector3D& a, const QVector3D& b, const QVector3D& c) const;
    void SetSortedTriangle(const QVector3D& a, const QVector3D& b, const QVector3D& c);
    int GetColorModifier(const QVector3D& point_a, const QVector3D& point_b,
                         const QVector3D& point_c) const;
    void DrawFace(const QColor& color);

    void SetList(const QVector3D &a, const QVector3D &b, const QVector3D &c);
    void SortList();

    void DrawSegment(int index_offset, const QColor& color);

    QVector3D GetRounded(const QVector3D& point) const;
};

#endif // FACEPAINTER_H
