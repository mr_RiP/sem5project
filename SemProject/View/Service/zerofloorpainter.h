#ifndef ZEROFLOORPAINTER_H
#define ZEROFLOORPAINTER_H

#include "../Abstract/abstractgraphicmodel.h"
#include <QColor>
#include <QVector2D>
#include <QRect>

class ZeroFloorPainter
{
public:
    ZeroFloorPainter(QImage *image, const AbstractGraphicModel* model, const QColor& color);
    ~ZeroFloorPainter();

    void draw();

    void setColor(const QColor &color);
    const QColor& getColor() const;

private:
    QImage *_img;
    QColor _color;

    QVector2D _in;
    QVector2D _x;
    QVector2D _y;

protected:
    ZeroFloorPainter();
    void SetInPoint(const AbstractGraphicModel* model);
    void SetMoveVectors(const AbstractGraphicModel* model);

    void DrawLines(QVector2D line_vector, QVector2D move_vector);

    void DrawVerticalLines(QVector2D line_vector, QVector2D move_vector);
    void DrawHorizontalLines(QVector2D line_vector, QVector2D move_vector);

    float GetMoveX(QVector2D line_vector) const;
    float GetStepX(QVector2D line_vector, QVector2D move_vector) const;
    float GetStartX(QVector2D line_vector, float move, float step) const;

    float GetMoveY(QVector2D line_vector) const;
    float GetStepY(QVector2D line_vector, QVector2D move_vector) const;
    float GetStartY(QVector2D line_vector, float move, float step) const;

    bool IsVectorVertical(QVector2D vector) const;

    QVector2D GetYNormalized(QVector2D vector) const;
    QVector2D GetXNormalized(QVector2D vector) const;
};

#endif // ZEROFLOORPAINTER_H
