#ifndef POINTSCREATOR_H
#define POINTSCREATOR_H

#include <QList>
#include <QVector3D>
#include "../../Model/Abstract/abstractpart.h"

class PointsCreator
{
public:
    static void create(QList<QVector3D*>& points, const AbstractPart* part);
    static int getPointDataIndex(const QList<QVector3D*>& points, const QVector3D& point);

    static void createBoxPoints(QList<QVector3D*>& points, const AbstractPart* part);

protected:
    static void CreatePointsOfComposite(QList<QVector3D*>& points, const AbstractPart* part);
    static void CreatePointsOfSimple(QList<QVector3D *>& points,
                                     const AbstractPosedDesign* part, const AbstractPosition* global_pos=NULL);
    static void CreatePointsOfCutted(QList<QVector3D *>& points,
                                     const AbstractPosedDesign* part, const AbstractPosition* global_pos=NULL);
    static void ClearPointsList(QList<QVector3D*>& points);

private:
    PointsCreator();

    static int GetCorrectRotation(int rot);
    static int GetSideIndexOffset(int rot);
    static int GetCutTypeIndexOffset(const AbstractDesignCore* design);
};

#endif // POINTSCREATOR_H
