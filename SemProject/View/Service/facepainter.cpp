#include "facepainter.h"
#include <cmath>

FacePainter::FacePainter()
{
    _img = NULL;
    _z = NULL;
    _block = NULL;
    _anchor = NULL;

    SetNullBlockData();
}

FacePainter::FacePainter(QImage &image, Buffer<float> &z_buffer, Buffer<int> &block_buffer, Buffer<bool>& anchor)
{
    _img = &image;
    _z = &z_buffer;
    _block = &block_buffer;
    _anchor = &anchor;

    SetNullBlockData();
}

void FacePainter::setBlockData(int index, const QColor &color)
{
    _index = index;
    _color = color;
}

void FacePainter::draw(const QVector3D &a, const QVector3D &b, const QVector3D &c, bool horizontal)
{
    if (FaceIsDegenerate(a,b,c))
        return;

    int mod = GetColorModifier(a,b,c);
    if (mod < 0)
        return;

    _horizontal = horizontal;
    QColor color = _color.lighter(mod);
    SetSortedTriangle(a,b,c);

    DrawFace(color);
}

void FacePainter::SetNullBlockData()
{
    _index = -1;
    _color = Qt::white;
}

bool FacePainter::FaceIsInvisible(const QVector3D &a, const QVector3D &b, const QVector3D &c) const
{
    QSize size = _img->size();
    return PointIsInvisible(a,size) && PointIsInvisible(b,size) && PointIsInvisible(c,size);
}

bool FacePainter::PointIsInvisible(const QVector3D &point, const QSize& size) const
{
    return (point.x() < 0) || (point.y() < 0) ||
           (point.x() >= size.width()) || (point.y() >= size.height());
}

bool FacePainter::FaceIsDegenerate(const QVector3D &a, const QVector3D &b, const QVector3D &c) const
{
    return ((a.x() == b.x()) && (b.x() == c.x())) || ((a.y() == b.y()) && (b.y() == c.y()));
}

void FacePainter::SetSortedTriangle(const QVector3D &a, const QVector3D &b, const QVector3D &c)
{
    SetList(a,b,c);
    SortList(); 
}

int FacePainter::GetColorModifier(const QVector3D &point_a, const QVector3D &point_b, const QVector3D &point_c) const
{
    QVector3D n = QVector3D::crossProduct(point_a-point_b, point_c-point_b).normalized();
    float val = n.z() * 100.f;
    return val;
}

void FacePainter::DrawFace(const QColor &color)
{
    float overall_height = _list[2].y() - _list[0].y();
    float seg_height = _list[1].y() - _list[0].y();

    if ((overall_height == seg_height) || (seg_height == 0.f))
        DrawSegment(0, color);
    else
    {
        QVector3D o = _list[2]-_list[0];
        o *= seg_height / fabs(o.y());
        o += _list[0];
        o = GetRounded(o);

        if (_list[1].x() > o.x())
            _list.insert(1,o);

        else
            _list.insert(2,o);

        DrawSegment(0, color);
        DrawSegment(1, color);
    }
}

void FacePainter::SetList(const QVector3D &a, const QVector3D &b, const QVector3D &c)
{
    _list.clear();
    _list.append(GetRounded(a));
    _list.append(GetRounded(b));
    _list.append(GetRounded(c));
}

void FacePainter::SortList()
{
    // сортируем по y
    if (_list[0].y() > _list[1].y())
        _list.swap(0,1);
    if (_list[0].y() > _list[2].y())
        _list.swap(0,2);
    if (_list[1].y() > _list[2].y())
        _list.swap(1,2);

    // сортируем равные y по x
    if ((_list[0].y() == _list[1].y()) && (_list[0].x() > _list[1].x()))
        _list.swap(0,1);
    if ((_list[1].y() == _list[2].y()) && (_list[1].x() > _list[2].x()))
        _list.swap(1,2);
}

void FacePainter::DrawSegment(int index_offset, const QColor &color)
{
    const QVector3D& a = _list[index_offset];
    const QVector3D& b = _list[index_offset + 1];
    const QVector3D& c = _list[index_offset + 2];

    int height = c.y() - a.y();
    bool upper_seg = b.y() == c.y();

    QVector3D vector_a = (upper_seg? (b-a): (c-a)) / (float)height;
    QVector3D vector_b = (upper_seg? (c-a): (c-b)) / (float)height;

    for (int i=0; i<height; i++)
    {
        QVector3D ai = GetRounded((vector_a * (float)i) + a);
        QVector3D bi = GetRounded((vector_b * (float)i) + (upper_seg? a: b));

        int width = roundf(bi.x() - ai.x());
        QVector3D vector_ab = (bi-ai) / (float)width;

        for (int j=0; j<=width; j++)
        {
            QVector3D abj = (vector_ab * (float)j) + ai;
            QPoint p = abj.toPoint();

            if (_z->getValue(p) > abj.z())
            {
                _z->setValue(p, abj.z());
                _block->setValue(p, _index);
                _anchor->setValue(p,_horizontal);
                _img->setPixel(p,color.rgb());
            }
        }
    }
}

QVector3D FacePainter::GetRounded(const QVector3D &point) const
{
    return QVector3D(roundf(point.x()), roundf(point.y()), point.z());
}
