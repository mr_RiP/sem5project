#include "facescreator.h"
#include "../face.h"
#include <stdexcept>

void FacesCreator::create(QList<AbstractFace *> &faces, const QList<int> &points)
{
    faces.clear();
    int n = points.size() / 8;
    for (int i=0; i<n; i++)
        CreateFacePack(faces, points, i);
}

void FacesCreator::CreateFacePack(QList<AbstractFace *> &faces, const QList<int> &points, int index)
{
    try
    {
        int start = index * 8;
        CreateWallFaces(faces,points,start);
        CreateFloorFaces(faces,points,start);
    }
    catch (std::exception& err)
    {
        ClearFaceList(faces);
        throw err;
    }
}

FacesCreator::FacesCreator()
{

}

void FacesCreator::CreateWallFaces(QList<AbstractFace *> &faces, const QList<int> &points, int start)
{
    int n = start + 3;
    for (int i=start; i<n; i++)     // 3 боковые грани
    {
        CreateFace(faces, points[i], points[i+1], points[i+5]);
        CreateFace(faces, points[i], points[i+5], points[i+4]);
    }
    CreateFace(faces, points[start+3], points[start], points[start+4]);     // замыкающая боковая грань
    CreateFace(faces, points[start+3], points[start+4], points[start+7]);
}

void FacesCreator::CreateFloorFaces(QList<AbstractFace *> &faces, const QList<int> &points, int start)
{
    // Верхние треугольники
    CreateFace(faces, points[start+4], points[start+5], points[start+6]);
    CreateFace(faces, points[start+4], points[start+6], points[start+7]);

    // Нижние треугольники
    CreateFace(faces, points[start+2], points[start+1], points[start]);
    CreateFace(faces, points[start+3], points[start+2], points[start]);
}

void FacesCreator::CreateFace(QList<AbstractFace *> &faces, int a, int b, int c)
{
    AbstractFace* face = new Face(a,b,c);
    if (getEqualFaceIndex(faces,*face) == -1)
        faces.append(face);
    else
        delete face;
}

void FacesCreator::ClearFaceList(QList<AbstractFace *> &faces)
{
    int n = faces.size();
    for (int i=0; i<n; i++)
        delete faces[i];
    faces.clear();
}

int FacesCreator::getEqualFaceIndex(const QList<AbstractFace *> &faces, const AbstractFace &face)
{
    int n = faces.size();
    for (int i=0; i<n; i++)
        if (faces[i]->isEqual(face))
            return i;
    return -1;
}

