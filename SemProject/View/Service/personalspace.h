#ifndef PERSONALSPACE_H
#define PERSONALSPACE_H

#include <QList>

template <typename T>
class PersonalSpace
{
public:
    PersonalSpace(QList<T>& list, QList<int> *index_space);
    ~PersonalSpace();
    T& operator [](int index);
    const T& operator [](int index) const;
    int size() const;

private:
    QList<T>& _list;
    QList<int>* _index;
};

// implementation

template <typename T>
PersonalSpace<T>::PersonalSpace(QList<T>& list, QList<int> *index_space)
{
    _list = list;
    _index = index_space;
}

template <typename T>
PersonalSpace<T>::~PersonalSpace()
{
}

template <typename T>
T& PersonalSpace<T>::operator [](int index)
{
    return _list[*(_index)[index]];
}

template <typename T>
const T& PersonalSpace<T>::operator [](int index) const
{
    return _list[*(_index)[index]];
}

template <typename T>
int PersonalSpace<T>::size()
{
    return _index->size();
}

#endif // PERSONALSPACE_H
