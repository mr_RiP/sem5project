#include "zerofloorpainter.h"
#include <QPainter>
#include <cmath>
#include <stdexcept>

ZeroFloorPainter::ZeroFloorPainter(QImage *image, const AbstractGraphicModel* model, const QColor& color)
{
    _img = image;
    _color = color;
    SetMoveVectors(model);
    SetInPoint(model);
}

ZeroFloorPainter::~ZeroFloorPainter()
{
}

void ZeroFloorPainter::draw()
{
    DrawLines(_x,_y);
    DrawLines(_y,_x);
}

void ZeroFloorPainter::setColor(const QColor& color)
{
    _color = color;
}

const QColor &ZeroFloorPainter::getColor() const
{
    return _color;
}

void ZeroFloorPainter::SetInPoint(const AbstractGraphicModel *model)
{
    const AbstractCamera* cam = model->getCamera();
    QVector3D z = cam->getVectorZ();
    if (z.z() == 0.f)
        _in = cam->toView(QVector3D(0.f,0.f,0.f)).toVector2D();
    else
    {
        const QVector3D& f = model->getCamera()->getFocusPoint();
        z += f;
        float h = f.z() / z.z();
        z *= h;

        z.setX(roundf(z.x()));
        z.setY(roundf(z.y()));
        z.setZ(roundf(z.z()));

        _in = cam->toView(z).toVector2D();
    }
}

void ZeroFloorPainter::SetMoveVectors(const AbstractGraphicModel *model)
{
    const AbstractCamera* cam = model->getCamera();
    _x = cam->toView(QVector3D(1.f, 0.f, 0.f)).toVector2D();
    _y = cam->toView(QVector3D(0.f, 1.f, 0.f)).toVector2D();

}

void ZeroFloorPainter::DrawLines(QVector2D line_vector, QVector2D move_vector)
{
    if (IsVectorVertical(line_vector))
        DrawVerticalLines(line_vector,move_vector);
    else
        DrawHorizontalLines(line_vector,move_vector);
}

void ZeroFloorPainter::DrawVerticalLines(QVector2D line_vector, QVector2D move_vector)
{
    QVector2D offset(_img->offset());
    QSize size = _img->size();
    QPainter p(_img);
    p.setPen(_color);

    float move = GetMoveX(line_vector);
    float step = GetStepX(line_vector,move_vector);
    float start = GetStartX(line_vector,move,step);

    int bot = -offset.y();
    int top = size.height() + bot;

    if (step != 0.f)
    {
        float end = (float)size.width() - offset.x() + fabs(move);
        for (float cur = start; cur < end; cur += step)
            p.drawLine(roundf(cur), bot, roundf(cur+move), top);
    }
    else
    {
        int x = roundf(start);
        p.drawLine(x, bot, x, top);
    }
}

void ZeroFloorPainter::DrawHorizontalLines(QVector2D line_vector, QVector2D move_vector)
{
    QVector2D offset(_img->offset());
    QSize size = _img->size();
    QPainter p(_img);
    p.setPen(_color);

    float move = GetMoveY(line_vector);
    float step = GetStepY(line_vector,move_vector);
    float start = GetStartY(line_vector,move,step);

    int left = -offset.x();
    int right = size.width() + left;

    if (step != 0.f)
    {
        float end = (float)size.height() - offset.y() + fabs(move);
        for (float cur = start; cur < end; cur += step)
            p.drawLine(left, roundf(cur), right, roundf(cur+move));
    }
    else
    {
        int y = roundf(start);
        p.drawLine(left,y,right,y);
    }
}

float ZeroFloorPainter::GetMoveX(QVector2D line_vector) const
{
    QVector2D x_vector = GetYNormalized(line_vector) * _img->size().height();
    return x_vector.x();
}

float ZeroFloorPainter::GetStepX(QVector2D line_vector, QVector2D move_vector) const
{
    QVector2D x_vector = (GetYNormalized(line_vector) * (-move_vector.y())) + move_vector;
    float step = fabs(x_vector.x());
    return ((step > 0.f) && (step < 1.f))? 1.f: step;
}

float ZeroFloorPainter::GetMoveY(QVector2D line_vector) const
{
    QVector2D y_vector = GetXNormalized(line_vector) * _img->size().width();
    return y_vector.y();
}

float ZeroFloorPainter::GetStepY(QVector2D line_vector, QVector2D move_vector) const
{
    QVector2D y_vector = (GetXNormalized(line_vector) * (-move_vector.x())) + move_vector;
    float step = fabs(y_vector.y());
    return ((step > 0.f) && (step < 1.f))? 1.f: step;
}

float ZeroFloorPainter::GetStartX(QVector2D line_vector, float move, float step) const
{
    QVector2D offset(_img->offset());
    QVector2D global_in = _in + offset;

    global_in += GetYNormalized(line_vector) * (-global_in.y());

    if (step != 0.f)
    {
        float min_start = (move > 0.f)? -move: 0.f;
        float c = (global_in.x() - min_start) / step;

        if ((global_in.x() > min_start) && ((c - truncf(c)) != 0))
            c += 1.f;
        return global_in.x() - step * truncf(c) - offset.x();
    }
    else
        return global_in.x() - offset.x();
}

float ZeroFloorPainter::GetStartY(QVector2D line_vector, float move, float step) const
{
        QVector2D offset(_img->offset());
        QVector2D global_in = _in + offset;

        global_in += GetXNormalized(line_vector) * (-global_in.x());

        if (step != 0.f)
        {
            float min_start = (move > 0.f)? -move: 0.f;
            float c = (global_in.y() - min_start) / step;
            if ((global_in.y() > min_start) && ((c - truncf(c)) != 0))
                c += 1.f;
            return global_in.y() - step * truncf(c) - offset.y();
        }
        else
            return global_in.y() - offset.y();
}

bool ZeroFloorPainter::IsVectorVertical(QVector2D vector) const
{
    return (fabs(vector.y()) > fabs(vector.x()));
}

QVector2D ZeroFloorPainter::GetYNormalized(QVector2D vector) const
{
    return vector / vector.y();
}

QVector2D ZeroFloorPainter::GetXNormalized(QVector2D vector) const
{
    return vector / vector.x();
}
