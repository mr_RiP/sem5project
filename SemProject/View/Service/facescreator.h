#ifndef FACESCREATOR_H
#define FACESCREATOR_H

#include <QList>
#include "../Abstract/abstractface.h"
#include "../../Model/Abstract/abstractpart.h"

class FacesCreator
{
public:
    static void create(QList<AbstractFace*>& faces, const QList<int>& points);
    static int getEqualFaceIndex(const QList<AbstractFace*>& faces, const AbstractFace &face);

protected:
    static void CreateFacePack(QList<AbstractFace*>& faces, const QList<int>& points, int index);

    static void CreateWallFaces(QList<AbstractFace*>& faces, const QList<int>& points, int start);
    static void CreateFloorFaces(QList<AbstractFace*>& faces, const QList<int>& points, int start);

    static void CreateFace(QList<AbstractFace*>& faces, int a, int b, int c);
    static void ClearFaceList(QList<AbstractFace*>& faces);

private:
    FacesCreator();
};

#endif // FACESCREATOR_H
