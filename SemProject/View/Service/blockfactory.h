#ifndef BLOCKFACTORY_H
#define BLOCKFACTORY_H

#include "../Abstract/abstractblock.h"
#include <QList>
#include <QVector3D>

class BlockFactory
{
public:
    BlockFactory(QList<QVector3D *>* points, QList<AbstractFace *>* faces, QList<AbstractBlock *>* blocks);
    ~BlockFactory();

    void createBlock(const AbstractPart* part);

    template <typename T>
    static int getListIndexOfData(const QList<T*>& list, const T* item);

protected:
    BlockFactory();

    void CreatePoints(QList<int>& index_list, const AbstractPart* part);
    void CreateFaces(QList<int>& index_list);
    void CreateBlock(QList<int>& index_list, const AbstractPart* part);

    void SetPointsIndexList(QList<int> &index_list, QList<QVector3D *> &new_points);
    void SetFacesIndexList(QList<int> &index_list, QList<AbstractFace *> &new_faces);

private:
    QList<QVector3D*>* _points;
    QList<AbstractFace*>* _faces;
    QList<AbstractBlock*>* _blocks;
};

#endif // BLOCKFACTORY_H
