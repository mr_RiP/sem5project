#ifndef GEOMETRICMODEL_H
#define GEOMETRICMODEL_H

#include "Abstract/abstractgeometricmodel.h"
#include <QList>

class GeometricModel : public AbstractGeometricModel
{
public:
    GeometricModel(const AbstractModel* model);
    ~GeometricModel();

    void update();
    void reset();

    const QVector3D& getPoint(int index) const;
    int getPointsNum() const;
    const AbstractFace& getFace(int index) const;
    int getFacesNum() const;
    const AbstractBlock& getBlock(int index) const;
    int getBlocksNum() const;

    const AbstractModel* getLogicModel() const;

    const QVector3D& getBoundingBoxMin() const;
    const QVector3D& getBoundingBoxMax() const;

    const AbstractFace& getBlockFace(int block_index, int face_index) const;
    const QVector3D& getBlockPoint(int block_index, int face_index, int point_index) const;
    const QVector3D& getFacePoint(int face_index, int point_index) const;

protected:
    const AbstractModel* _model;
    QList<QVector3D*> _points;
    QList<AbstractFace*> _faces;
    QList<AbstractBlock*> _blocks;
    QVector3D _min;
    QVector3D _max;

    GeometricModel();

    template<typename T>
    static void ClearListData(QList<T*>& list);

    void ClearLists();
    void SetBoundingBox();

    template <typename T>
    static int GetIndexOf(const QList<T*>& list, const T& item);
};

#endif // GEOMETRICMODEL_H
