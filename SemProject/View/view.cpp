#include "view.h"
#include "graphicmodel.h"
#include "geometricmodel.h"
#include "modelpainter.h"
#include <stdexcept>

#define DEBUG

#ifdef DEBUG
#include <cstdio>
#endif

View::View(const AbstractModel *model, QSize size)
{
    SetAllNull();
    SetData(model,size);
}

View::~View()
{
    clear();
}

bool View::isReady() const
{
    return _structure != NULL;
}

void View::SetData(const AbstractModel *model, QSize size)
{
    if (isReady())
        clear();

    try
    {
        _structure = new GeometricModel(model);
        _view = new GraphicModel(_structure, size);
        _painter = new ModelPainter(_view, size);
    }
    catch (std::exception& err)
    {
        clear();
        throw err;
    }
}

const QImage &View::getImage() const
{
    return _painter->getImage();
}

const AbstractModel *View::getModel() const
{
    if (isReady())
        return _structure->getLogicModel();
    else
        return NULL;
}

const AbstractCamera *View::getCamera() const
{
    if (isReady())
        return _view->getCamera();
    else
        return NULL;
}

void View::rotateX(int deg)
{
    if (isReady())
        _view->rotateX(deg);
}

void View::rotateY(int deg)
{
    if (isReady())
        _view->rotateY(deg);
}

void View::setScale(float scale)
{
    if (isReady())
        _view->setScale(scale);
}

float View::getScale() const
{
    if (isReady())
        return _view->getScale();
    else
        return -1;
}

int View::getPartIndex(int x, int y) const
{
    if (isReady())
    {
        int index = _painter->getBlockIndex(x,y);
        if (index >= 0)
        {
            const AbstractPart* part = _structure->getBlock(index).getPart();
            return _structure->getLogicModel()->getPartIndex(part);
        }
    }
    return -1;
}

int View::getPartIndex(QPoint point) const
{
    return getPartIndex(point.x(),point.y());
}

float View::getZ(int x, int y) const
{
    if (isReady())
        return _painter->getZ(x,y);
    else
        return -1;
}

float View::getZ(QPoint point) const
{
    if (isReady())
        return _painter->getZ(point);
    else
        return -1;
}

bool View::isPointOnAnchor(int x, int y) const
{
    if (isReady())
        return _painter->getAnchor(x,y);
    else
        return false;
}

bool View::isPointOnAnchor(QPoint point) const
{
    if (isReady())
        return _painter->getAnchor(point);
    else
        return false;
}

void View::resetStructure()
{
    if (isReady())
    {
        _structure->reset();
        resetView();
    }
}

void View::resetView()
{
    if (isReady())
    {
        _view->reset();
        resetColoring();
    }
}

void View::resetColoring()
{
    if (isReady())
    {
        _painter->redraw();
        emit imageChanged();
    }
}

void View::resetAll()
{
    resetStructure();
}

void View::resize(QSize size)
{
    if (isReady())
    {
        _view->resize(size);
        _painter->resize(size);
        emit imageChanged();
    }
}

void View::SetAllNull()
{
    _structure = NULL;
    _view = NULL;
    _painter = NULL;
}

void View::clear()
{
    if (_painter != NULL)
    {
        delete _painter;
        _painter = NULL;
    }
    if (_view != NULL)
    {
        delete _view;
        _view = NULL;
    }
    if (_structure != NULL)
    {
        delete _structure;
        _structure = NULL;
    }
}
