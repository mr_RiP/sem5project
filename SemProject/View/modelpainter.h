#ifndef MODELPAINTER_H
#define MODELPAINTER_H

#include "Abstract/abstractmodelpainter.h"
#include "Abstract/abstractgraphicmodel.h"
#include "Template/buffer.h"

class ModelPainter : public AbstractModelPainter
{
public:
    ModelPainter(const AbstractGraphicModel* graphics, QSize size);
    ~ModelPainter();

    const QImage &getImage() const;
    float getZ(int x, int y) const;
    float getZ(QPoint point) const;
    int getBlockIndex(int x, int y) const;
    int getBlockIndex(QPoint point) const;
    bool getAnchor(int x, int y) const;
    bool getAnchor(QPoint point) const;

    void resize(QSize size);
    void redraw();
    void flush();

    const QColor& getBackgroundColor() const;
    const QColor& getZeroFloorColor() const;

    void setBackgroundColor(const QColor& color);
    void setZeroFloorColor(const QColor& color);

private:
    const AbstractGraphicModel* _model;
    QImage _img;
    Buffer<float> _z;
    Buffer<int> _block;
    Buffer<bool> _anchor;

    QColor _background;
    QColor _zero;

protected:
    ModelPainter();

    void SetBufferConstants();
    void DrawZeroFloor();
    void DrawModel();
    void SetDefaultColors();

    bool FaceIsAnchor(int index) const;
};

#endif // MODELPAINTER_H
