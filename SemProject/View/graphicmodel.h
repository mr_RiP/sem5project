#ifndef GRAPHICMODEL_H
#define GRAPHICMODEL_H

#include "Abstract/abstractgeometricmodel.h"
#include "Abstract/abstractgraphicmodel.h"

class GraphicModel : public AbstractGraphicModel
{
public:
    GraphicModel(const AbstractGeometricModel* parent_model, QSize size);
    ~GraphicModel();

    void update();
    void reset();
    void resize(QSize size);

    const QVector3D& getPoint(int index) const;
    int getPointsNum() const;
    const AbstractFace& getFace(int index) const;
    int getFacesNum() const;
    const AbstractBlock& getBlock(int index) const;
    int getBlocksNum() const;

    const AbstractFace& getBlockFace(int block_index, int face_index) const;
    const QVector3D& getBlockPoint(int block_index, int face_index, int point_index) const;
    const QVector3D &getFacePoint(int face_index, int point_index) const;

    const AbstractCamera* getCamera() const;

    void rotateX(int deg);
    void rotateY(int deg);

    void setScale(float scale);
    float getScale() const;

    void setSize(QSize size);
    QSize getSize() const;

    const QVector3D& getBoundingBoxMin() const;
    const QVector3D& getBoundingBoxMax() const;

    const AbstractModel* getLogicModel() const;

private:
    const AbstractGeometricModel* _parent;
    QList<QVector3D> _points;
    QVector3D _max;
    QVector3D _min;
    AbstractCamera* _cam;
    QSize _size;

protected:
    GraphicModel();
    void SetDefaultCam();
    void SetDefaultFocus();
    void SetDefaultScale();
    void UpdateCam();
    void ResetPoints();
    void ResetBoundingBox();
};

#endif // GRAPHICMODEL_H
