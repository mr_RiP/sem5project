#include "block.h"
#include <stdexcept>

Block::Block(const QList<int> &faces, const AbstractPart* part)
{
    _faces = faces;
    _part = part;
}

Block::~Block()
{
}

Block::Block()
{

}

int Block::getFaceIndex(int index) const
{
    if ((index < 0) || (index >= _faces.size()))
        throw std::invalid_argument("Trying to get face index from Block using invalid index.");
    return _faces[index];
}

int Block::getFacesNum() const
{
    return _faces.size();
}

const QColor& Block::getColor() const
{
    return _part->getDetail()->getColor();
}

const AbstractPart* Block::getPart() const
{
    return _part;
}
