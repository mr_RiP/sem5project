#include "camera.h"
#include <cmath>

#define CAM_PI 3.14159265358979f

Camera::Camera()
{
    _focus = QVector3D(0.f, 0.f, 0.f);
    _scale = 1.f;

    setObserverPoint(QVector3D(-1.f,0.f,0.f));
    SetSwitcherModifiers();
}

Camera::Camera(const QVector3D& focus_point, const QVector3D& observer_point, float scale)
{
    _focus = focus_point;
    _scale = 1.f;

    setScale(scale);
    setObserverPoint(observer_point);

    SetSwitcherModifiers();
}

Camera::~Camera()
{

}

const QVector3D& Camera::getFocusPoint() const
{
    return _focus;
}

void Camera::setFocusPoint(const QVector3D& focus_point)
{
    _focus = focus_point;
}

void Camera::setObserverPoint(const QVector3D& observer_point)
{
    if (observer_point == _focus)
        throw std::invalid_argument("Trying to set Camera with observer point equal to the focus point.");
    if ((observer_point.x() == _focus.x()) && (observer_point.y() == _focus.y()))
        throw std::invalid_argument("Trying to set Camera with observer point that makes vertical z-vector.");

    CreateVectorZ(observer_point);
    SetSwitcherModifiers();
    SetVectorsByTurns();
}

bool Camera::isNull() const
{
    return (_x.isNull() && _y.isNull() && _z.isNull());
}

const QVector3D& Camera::getVectorX() const
{
    return _x;
}

const QVector3D& Camera::getVectorY() const
{
    return _y;
}

const QVector3D& Camera::getVectorZ() const
{
    return _z;
}

void Camera::CreateVectorZ(const QVector3D &observer_point)
{
    _z = (_focus - observer_point).normalized();
}

void Camera::CreateVectorX()
{
    _x.setX(_z.y());
    _x.setY(-_z.x());
    _x.setZ(0.f);
}

void Camera::CreateVectorY()
{
    _y.setX(0.f);
    _y.setZ(-_z.y());
    _y.setY(-_z.z());
}

void Camera::rotateX(int deg)
{
    if (deg != 0.f)
    {
        _z_turn += DegToRad(deg);
        SetVectorsByTurns();
    }
}

void Camera::rotateY(int deg)
{
    if (deg != 0.f)
    {
        float turn = _x_turn += DegToRad(deg);
        if (turn > CAM_PI)
            _x_turn = CAM_PI;
        else
        {
            if (turn < 0.f)
                _x_turn = 0.f;
            else
                _x_turn = turn;
        }

        SetVectorsByTurns();
    }
}

float Camera::getScale() const
{
    return _scale;
}

void Camera::setScale(float scale)
{
    if (scale > 0)
        _scale = scale;
}

void Camera::setOffset(QPoint offset)
{
    _offset = QVector3D(offset);
}

QPoint Camera::getOffset() const
{
    return _offset.toPoint();
}

float Camera::DegToRad(int deg) const
{

    float rad = deg;
    rad -= truncf(rad / 360.f) * 360.f;
    rad *= CAM_PI / 180.f;
    return rad;
}

void Camera::SetSwitcherModifiers()
{
    SetZTurn();
    SetXTurn();
}

void Camera::SetVectorsByTurns()
{
    SetDefaultVectors();
    SwitchVectorToModel(_x);
    SwitchVectorToModel(_y);
    SwitchVectorToModel(_z);
}

bool Camera::zIsVertical() const
{
    return (_z.x() == 0.f) && (_z.y() == 0.f);
}

void Camera::SwitchVectorToModel(QVector3D &vector) const
{
    TurnX(vector,true);
    TurnZ(vector,true);
    vector.normalize();
}

void Camera::SetDefaultVectors()
{
    _x = QVector3D(1.f,0.f,0.f);
    _y = QVector3D(0.f,1.f,0.f);
    _z = QVector3D(0.f,0.f,1.f);
}

QVector3D Camera::toView(const QVector3D &geometric_model_point) const
{
    QVector3D point = geometric_model_point;
    switchToView(point);
    return point;
}

QVector3D Camera::toModel(const QVector3D &graphic_model_point) const
{
    QVector3D point = graphic_model_point;
    switchToModel(point);
    return point;
}

void Camera::switchToView(QVector3D &point) const
{
    point -= _focus;
    TurnZ(point,false);
    TurnX(point,false);
    point *= _scale;
    point += _offset;
}

void Camera::switchToModel(QVector3D &point) const
{
    point -= _offset;
    point /= _scale;
    TurnX(point,true);
    TurnZ(point,true);
    point += _focus;
}

QVector3D Camera::toDifferentScale(const QVector3D &point, float new_scale) const
{
    QVector3D new_point = point;
    switchToDifferentScale(new_point, new_scale);
    return new_point;
}

void Camera::switchToDifferentScale(QVector3D &point, float new_scale) const
{
    point -= _offset;
    point /= _scale;
    point *= new_scale;
    point += _offset;
}

QVector3D Camera::toDifferentOffset(const QVector3D &point, const QPoint &new_offset) const
{
    QVector3D new_point = point;
    switchToDifferentOffset(new_point,new_offset);
    return new_point;
}

void Camera::switchToDifferentOffset(QVector3D &point, const QPoint &new_offset) const
{
    point -= _offset;
    point += QVector3D(new_offset);
}

void Camera::SetZTurn()
{
    bool z_is_vertical = zIsVertical();
    float x = z_is_vertical? _y.x(): _z.x();
    float y = z_is_vertical? _y.x():_z.y();
    float len = sqrtf(x*x + y*y);
    float cos_z = (len != 0.f)? y/len: 0.f;
    _z_turn = (x > 0.f)? -acos(cos_z): acos(cos_z);
}

void Camera::SetXTurn()
{
    _x_turn = acos(_z.z());
}

void Camera::TurnZ(QVector3D &point, bool back) const
{
    float alpha = back? -_z_turn: _z_turn;
    float x = point.x();
    float y = point.y();
    float cosa = cos(alpha);
    float sina = sin(alpha);

    point.setX(x*cosa - y*sina);
    point.setY(x*sina + y*cosa);
}

void Camera::TurnX(QVector3D &point, bool back) const
{
    float alpha = back? -_x_turn: _x_turn;
    float y = point.y();
    float z = point.z();
    float cosa = cos(alpha);
    float sina = sin(alpha);

    point.setY(y*cosa - z*sina);
    point.setZ(y*sina + z*cosa);
}
