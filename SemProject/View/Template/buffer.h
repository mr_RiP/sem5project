#ifndef BUFFER_H
#define BUFFER_H

#include <QList>
#include <QPoint>
#include <QtGui/QImage>
#include <QSize>
#include <stdexcept>

template <typename T>
class Buffer
{
public:
    Buffer();
    Buffer(const QImage& image, const T& null_value, const T& restrict_value);
    ~Buffer();

    const T& getValue(int i, int j) const;
    void setValue(int i, int j, const T& value);

    const T& getValue(QPoint point) const;
    void setValue(QPoint point, const T& value);

    const T& at(int i, int j) const;
    T& at(int i, int j);

    void setSize(QSize size);
    QSize getSize() const;

    int getPixlesNum() const;

    void flush();

    void setup(const QImage &image);
    void setup(const QImage &image, const T& null_value, const T& restrict_value);

    void setNullValue(const T& null_value);
    const T& getNullValue() const;

    void setRestrictValue(const T& restrict_value);
    const T& getRestrictValue() const;

    void clear();
    bool isNull() const;

protected:
    int ListIndex(int i, int j) const;
    void ChangeListSize(int change);
    bool CoordinatesAreInvalid(int x, int y) const;

private:
    QList<T> _list;
    QSize _size;
    T _null;
    T _restrict;
};

// implementation

template<typename T>
Buffer<T>::Buffer()
{
}

template<typename T>
Buffer<T>::~Buffer()
{
}

template<typename T>
const T &Buffer<T>::getValue(int i, int j) const
{
    if (CoordinatesAreInvalid(i,j))
        return _restrict;
    else
        return _list[ListIndex(i,j)];
}

template<typename T>
void Buffer<T>::setValue(int i, int j, const T &value)
{
    if (CoordinatesAreInvalid(i,j))
        return;
    else
        _list[ListIndex(i,j)] = value;
}

template<typename T>
const T& Buffer<T>::getValue(QPoint point) const
{
    if (CoordinatesAreInvalid(point.x(),point.y()))
        return _restrict;
    else
        return _list[ListIndex(point.x(),point.y())];
}

template<typename T>
void Buffer<T>::setValue(QPoint point, const T& value)
{
    if (CoordinatesAreInvalid(point.x(),point.y()))
        return;
    else
        _list[ListIndex(point.x(),point.y())] = value;
}

template<typename T>
int Buffer<T>::ListIndex(int i, int j) const
{
    return j*_size.width() + i;
}

template<typename T>
Buffer<T>::Buffer(const QImage &image, const T &null_value, const T &restrict_value)
{
    setup(image,null_value,restrict_value);
}

template<typename T>
const T& Buffer<T>::at(int i, int j) const
{
    return _list[ListIndex(i,j)];
}

template<typename T>
T& Buffer<T>::at(int i, int j)
{
    return _list[ListIndex(i,j)];
}

template<typename T>
void Buffer<T>::setSize(QSize size)
{
    if (size != _size)
    {
        _size = size;
        ChangeListSize((_size.width() * _size.height()) - _list.size());
    }
}

template<typename T>
QSize Buffer<T>::getSize() const
{
    return _size;
}

template<typename T>
int Buffer<T>::getPixlesNum() const
{
    return _list.size();
}

template<typename T>
void Buffer<T>::flush()
{
    int n = _list.size();
    for (int i=0; i<n; i++)
        _list[i] = _null;
}

template<typename T>
void Buffer<T>::setup(const QImage &image)
{
    setSize(image.size());
}

template<typename T>
void Buffer<T>::setup(const QImage &image, const T& null_value, const T &restrict_value)
{
    setup(image);
    setNullValue(null_value);
    setRestrictValue(restrict_value);
}

template<typename T>
void Buffer<T>::setNullValue(const T& null_value)
{
    _null = null_value;
}

template<typename T>
const T& Buffer<T>::getNullValue() const
{
    return _null;
}

template<typename T>
void Buffer<T>::setRestrictValue(const T &restrict_value)
{
    _restrict = restrict_value;
}

template<typename T>
const T &Buffer<T>::getRestrictValue() const
{
    return _restrict;
}

template<typename T>
void Buffer<T>::clear()
{
    _size *= 0;
    _list.clear();
}

template<typename T>
bool Buffer<T>::isNull() const
{
    return _list.isEmpty();
}

template<typename T>
void Buffer<T>::ChangeListSize(int change)
{
    for (int i=0; i<change; i++)
        _list.append(_null);
    for (int i=0; i>change; i--)
        _list.removeLast();
}

template<typename T>
bool Buffer<T>::CoordinatesAreInvalid(int x, int y) const
{
    return (x < 0) || (y < 0) || (x >= _size.width()) || (y >=_size.height());
}

#endif // BUFFER_H
