#ifndef VIEW_H
#define VIEW_H

#include "Abstract/abstractview.h"
#include "Abstract/abstractgeometricmodel.h"
#include "Abstract/abstractgraphicmodel.h"
#include "Abstract/abstractmodelpainter.h"

class View : public AbstractView
{
    Q_OBJECT
public:
    View(const AbstractModel* model, QSize size);
    ~View();

    void SetData(const AbstractModel* model, QSize size);
    bool isReady() const;

    const QImage &getImage() const;
    const AbstractModel *getModel() const;
    const AbstractCamera *getCamera() const;

    void rotateX(int deg);
    void rotateY(int deg);

    void setScale(float scale);
    float getScale() const;

    int getPartIndex(int x, int y) const;
    int getPartIndex(QPoint point) const;
    float getZ(int x, int y) const;
    float getZ(QPoint point) const;

    bool isPointOnAnchor(int x, int y) const;
    bool isPointOnAnchor(QPoint point) const;

public slots:
    void resetStructure();
    void resetView();
    void resetColoring();
    void resetAll();

    void resize(QSize size);

    void clear();

private:
    AbstractGeometricModel* _structure;
    AbstractGraphicModel* _view;
    AbstractModelPainter* _painter;

protected:
    void SetAllNull();
};

#endif // VIEW_H
