#include "graphicmodel.h"
#include "camera.h"
#include <stdexcept>

GraphicModel::GraphicModel()
{
    _cam = new Camera;
    _cam->setScale(1.f);
}

void GraphicModel::SetDefaultCam()
{
    SetDefaultFocus();
    _cam->setObserverPoint(QVector3D(-3.f, -3.f, 2.f));
}

void GraphicModel::SetDefaultFocus()
{
    _cam->setFocusPoint(QVector3D(0.5f, 0.5f, 0.f));
}

void GraphicModel::SetDefaultScale()
{
    _cam->setScale((_size.width() < _size.height())? _size.width() / 5: _size.height() /5);
}

void GraphicModel::UpdateCam()
{
    if (_parent->getPointsNum() == 0)
        SetDefaultFocus();
    else
        _cam->setFocusPoint((_parent->getBoundingBoxMin() + _parent->getBoundingBoxMax()) / 2.f);
}

void GraphicModel::ResetPoints()
{
    _points.clear();
    int n = _parent->getPointsNum();
    for (int i=0; i<n; i++)
        _points.append(_cam->toView(_parent->getPoint(i)));
}

void GraphicModel::ResetBoundingBox()
{
    if (_points.isEmpty())
    {
        _max *= 0.f;
        _min = _max;
    }
    else
    {
        _max = _points[0];
        _min = _max;

        int n = _points.size();
        for (int i=1; i<n; i++)
            for (int j=0; j<3; j++)
            {
                if (_points[i][j] > _max[j])
                    _max[j] = _points[i][j];
                if (_points[i][j] < _min[j])
                    _min[j] = _points[i][j];
            }
    }
}

GraphicModel::GraphicModel(const AbstractGeometricModel *parent_model, QSize size)
{
    if (parent_model == NULL)
        throw std::invalid_argument("Trying to create a Graphic Model with NULL parent.");

    _parent = parent_model;
    _cam = new Camera;
    setSize(size);
    SetDefaultScale();
    SetDefaultCam();
    reset();
}

GraphicModel::~GraphicModel()
{
    delete _cam;
}

void GraphicModel::update()
{
    reset();
}

void GraphicModel::reset()
{
    UpdateCam();
    ResetPoints();
    ResetBoundingBox();
}

void GraphicModel::resize(QSize size)
{
    _size = size;

    QPoint offset(_size.width()/2, _size.height()/2);

    int n = _points.size();
    for (int i=0; i<n; i++)
        _cam->switchToDifferentOffset(_points[i],offset);

    _cam->switchToDifferentOffset(_min,offset);
    _cam->switchToDifferentOffset(_max,offset);

    _cam->setOffset(offset);
}

const QVector3D &GraphicModel::getPoint(int index) const
{
    if ((index < 0) || (index >= _points.size()))
        throw std::invalid_argument("Trying to get point value from Graphic Model using invalid index.");
    return _points[index];
}

int GraphicModel::getPointsNum() const
{
    return _points.size();
}

const AbstractFace &GraphicModel::getFace(int index) const
{
    if ((index < 0) || (index >= _parent->getFacesNum()))
        throw std::invalid_argument("Trying to get face value from Graphic Model using invalid index.");
    return _parent->getFace(index);
}

int GraphicModel::getFacesNum() const
{
    return _parent->getFacesNum();
}

const AbstractBlock &GraphicModel::getBlock(int index) const
{
    if ((index < 0) || (index >= _parent->getBlocksNum()))
        throw std::invalid_argument("Trying to get block value from Graphic Model using invalid index.");
    return _parent->getBlock(index);
}

int GraphicModel::getBlocksNum() const
{
    return _parent->getBlocksNum();
}

const AbstractFace &GraphicModel::getBlockFace(int block_index, int face_index) const
{
    return getFace(getBlock(block_index).getFaceIndex(face_index));
}

const QVector3D &GraphicModel::getBlockPoint(int block_index, int face_index, int point_index) const
{
    int face = getBlock(block_index).getFaceIndex(face_index);
    int point = getFace(face).getPoint(point_index);
    return getPoint(point);
}

const QVector3D &GraphicModel::getFacePoint(int face_index, int point_index) const
{
    return getPoint(getFace(face_index).getPoint(point_index));
}

const AbstractCamera *GraphicModel::getCamera() const
{
    return _cam;
}

void GraphicModel::rotateX(int deg)
{
    _cam->rotateX(deg);
}

void GraphicModel::rotateY(int deg)
{
    _cam->rotateY(deg);
}

void GraphicModel::setScale(float scale)
{
    _cam->setScale(scale);
}

float GraphicModel::getScale() const
{
    return _cam->getScale();
}

void GraphicModel::setSize(QSize size)
{
    _size = size;
    _cam->setOffset(QPoint(size.width()/2,size.height()/2));
}

QSize GraphicModel::getSize() const
{
    return _size;
}

const QVector3D &GraphicModel::getBoundingBoxMin() const
{
    return _min;
}

const QVector3D &GraphicModel::getBoundingBoxMax() const
{
    return _max;
}

const AbstractModel *GraphicModel::getLogicModel() const
{
    return _parent->getLogicModel();
}
