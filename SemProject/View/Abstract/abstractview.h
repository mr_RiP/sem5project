#ifndef ABSTRACTVIEW_H
#define ABSTRACTVIEW_H

#include "../../Model/Abstract/abstractmodel.h"
#include "abstractcamera.h"
#include <QImage>
#include <QObject>

class AbstractView : public QObject
{
    Q_OBJECT
public:
    virtual ~AbstractView() {}

    virtual void SetData(const AbstractModel* model, QSize size) = 0;
    virtual bool isReady() const = 0;

    virtual const QImage& getImage() const = 0;
    virtual const AbstractModel* getModel() const = 0;
    virtual const AbstractCamera* getCamera() const = 0;

    virtual void rotateX(int deg) = 0;
    virtual void rotateY(int deg) = 0;

    virtual void setScale(float scale) = 0;
    virtual float getScale() const = 0;

    virtual int getPartIndex(int x, int y) const = 0;
    virtual int getPartIndex(QPoint point) const = 0;
    virtual float getZ(int x, int y) const = 0;
    virtual float getZ(QPoint point) const = 0;

    virtual bool isPointOnAnchor(int x, int y) const = 0;
    virtual bool isPointOnAnchor(QPoint point) const = 0;

public slots:
    virtual void resetStructure() = 0;
    virtual void resetView() = 0;
    virtual void resetColoring() = 0;
    virtual void resetAll() = 0;

    virtual void resize(QSize size) = 0;

    virtual void clear() = 0;

signals:
    void imageChanged();
};

#endif // ABSTRACTVIEW_H

