#ifndef ABSTRACTGEOMETRICMODEL_H
#define ABSTRACTGEOMETRICMODEL_H

#include <QVector3D>
#include "abstractblock.h"
#include "abstractface.h"
#include "../../Model/Abstract/abstractmodel.h"

class AbstractGeometricModel
{
public:
    virtual ~AbstractGeometricModel() {}

    virtual void update() = 0;
    virtual void reset() = 0;

    virtual const QVector3D& getPoint(int index) const = 0;
    virtual int getPointsNum() const = 0;
    virtual const AbstractFace& getFace(int index) const = 0;
    virtual int getFacesNum() const = 0;
    virtual const AbstractBlock& getBlock(int index) const = 0;
    virtual int getBlocksNum() const = 0;

    virtual const AbstractFace& getBlockFace(int block_index, int face_index) const = 0;
    virtual const QVector3D& getBlockPoint(int block_index, int face_index, int point_index) const = 0;
    virtual const QVector3D& getFacePoint(int face_index, int point_index) const = 0;

    virtual const AbstractModel* getLogicModel() const = 0;

    virtual const QVector3D& getBoundingBoxMin() const = 0;
    virtual const QVector3D& getBoundingBoxMax() const = 0;
};


#endif // ABSTRACTGEOMETRICMODEL_H

