#ifndef ABSTRACTGRAPHICMODEL_H
#define ABSTRACTGRAPHICMODEL_H

#include <QVector3D>
#include <QSize>
#include "abstractcamera.h"
#include "abstractgeometricmodel.h"

class AbstractGraphicModel : public AbstractGeometricModel
{
public:
    virtual ~AbstractGraphicModel() {}

    virtual void update() = 0;
    virtual void reset() = 0;
    virtual void resize(QSize size) = 0;

    virtual void rotateX(int deg) = 0;
    virtual void rotateY(int deg) = 0;

    virtual void setScale(float scale) = 0;
    virtual float getScale() const = 0;

    virtual void setSize(QSize size) = 0;
    virtual QSize getSize() const = 0;

    virtual const AbstractCamera* getCamera() const = 0;
};

#endif // ABSTRACTGRAPHICMODEL_H

