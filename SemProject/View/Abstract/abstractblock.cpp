#include "abstractblock.h"

int AbstractBlock::operator [](int index) const
{
    return getFaceIndex(index);
}

int AbstractBlock::size() const
{
    return getFacesNum();
}

