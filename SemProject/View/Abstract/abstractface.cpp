#include "abstractface.h"
#include <stdexcept>

bool operator ==(const AbstractFace& a, const AbstractFace& b)
{
    return a.isEqual(b);
}

int AbstractFace::operator [](int index)
{
    return getPoint(index);
}
