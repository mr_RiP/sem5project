#ifndef ABSTRACTFACE_H
#define ABSTRACTFACE_H

#include <QList>

class AbstractFace
{
public:
    virtual ~AbstractFace() {}

    virtual int getPoint(int index) const = 0;
    virtual int getPointA() const = 0;
    virtual int getPointB() const = 0;
    virtual int getPointC() const = 0;
    virtual bool isEqual(const AbstractFace& face) const = 0;

    friend bool operator == (const AbstractFace& a, const AbstractFace& b);
    virtual int operator [] (int index);
};


#endif // ABSTRACTFACE_H

