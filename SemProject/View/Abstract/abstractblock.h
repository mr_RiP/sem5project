#ifndef ABSTRACTBLOCK_H
#define ABSTRACTBLOCK_H

#include "../../Model/Abstract/abstractpart.h"
#include "abstractface.h"
#include <QColor>

class AbstractBlock
{
public:
    virtual ~AbstractBlock() {}

    virtual int getFaceIndex(int index) const = 0;
    virtual int getFacesNum() const = 0;

    virtual const QColor& getColor() const = 0;
    virtual const AbstractPart* getPart() const = 0;

    int operator [](int index) const;
    int size() const;
};

#endif // ABSTRACTBLOCK_H

