#ifndef ABSTRACTCAMERA_H
#define ABSTRACTCAMERA_H

#include <QVector3D>

class AbstractCamera
{
public:
    virtual ~AbstractCamera() {}

    virtual const QVector3D& getFocusPoint() const = 0;

    virtual void setFocusPoint(const QVector3D& focus_point) = 0;
    virtual void setObserverPoint(const QVector3D& observer_point) = 0;

    virtual bool isNull() const = 0;

    virtual const QVector3D& getVectorX() const = 0;
    virtual const QVector3D& getVectorY() const = 0;
    virtual const QVector3D& getVectorZ() const = 0;

    virtual void rotateX(int deg) = 0;
    virtual void rotateY(int deg) = 0;

    virtual void setOffset(QPoint offset) = 0;
    virtual QPoint getOffset() const = 0;

    virtual float getScale() const = 0;
    virtual void setScale(float scale) = 0;

    virtual QVector3D toView(const QVector3D& geometric_model_point) const = 0;
    virtual QVector3D toModel(const QVector3D& graphic_model_point) const = 0;

    virtual void switchToView(QVector3D& point) const = 0;
    virtual void switchToModel(QVector3D& point) const = 0;

    virtual QVector3D toDifferentScale(const QVector3D& point, float new_scale) const = 0;
    virtual void switchToDifferentScale(QVector3D& point, float new_scale) const = 0;

    virtual QVector3D toDifferentOffset(const QVector3D& point, const QPoint& new_offset) const = 0;
    virtual void switchToDifferentOffset(QVector3D& point, const QPoint& new_offset) const = 0;
};

/* Заметки
 * Вращение - по Z FocusPoint'а
 */

#endif // ABSTRACTCAMERA_H

