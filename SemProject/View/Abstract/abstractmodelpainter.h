#ifndef ABSTRACTMODELPAINTER_H
#define ABSTRACTMODELPAINTER_H

#include <QImage>
#include <QPoint>
#include <QSize>

class AbstractModelPainter
{
public:
    virtual ~AbstractModelPainter() {}

    virtual const QImage& getImage() const = 0;

    virtual float getZ(int x, int y) const = 0;
    virtual float getZ(QPoint point) const = 0;
    virtual int getBlockIndex(int x, int y) const = 0;
    virtual int getBlockIndex(QPoint point) const = 0;
    virtual bool getAnchor(int x, int y) const = 0;
    virtual bool getAnchor(QPoint point) const = 0;

    virtual void redraw() = 0;
    virtual void resize(QSize size) = 0;
    virtual void flush() = 0;

    virtual const QColor& getBackgroundColor() const = 0;
    virtual const QColor& getZeroFloorColor() const = 0;

    virtual void setBackgroundColor(const QColor& color) = 0;
    virtual void setZeroFloorColor(const QColor& color) = 0;
};

#endif // ABSTRACTMODELPAINTER_H

