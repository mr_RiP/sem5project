#include "geometricmodel.h"
#include "block.h"
#include "Service/blockfactory.h"
#include <stdexcept>

GeometricModel::GeometricModel(const AbstractModel *model)
{
    if (model == NULL)
        throw std::invalid_argument("Trying to create geometric model with null logic model.");

    _model = model;

    reset();
}

GeometricModel::~GeometricModel()
{
    ClearLists();
}

void GeometricModel::update()
{
    reset(); // Позже написать более оптимальные реализации.
}

const QVector3D& GeometricModel::getPoint(int index) const
{
    if ((index < 0) || (index >= _points.size()))
        throw std::invalid_argument("Trying to get point value from Geometic Model using invalid index.");
    return *(_points[index]);
}

int GeometricModel::getPointsNum() const
{
    return _points.size();
}

const AbstractFace& GeometricModel::getFace(int index) const
{
    if ((index < 0) || (index >= _faces.size()))
        throw std::invalid_argument("Trying to get face value from Geometric Model using invalid index.");
    return *(_faces[index]);
}

int GeometricModel::getFacesNum() const
{
    return _faces.size();
}

const AbstractBlock& GeometricModel::getBlock(int index) const
{
    if ((index < 0) || (index >= _blocks.size()))
        throw std::invalid_argument("Trying to get block value from Geometric Model using invalid index.");
    return *(_blocks[index]);
}

int GeometricModel::getBlocksNum() const
{
    return _blocks.size();
}

const AbstractModel *GeometricModel::getLogicModel() const
{
    return _model;
}

const QVector3D &GeometricModel::getBoundingBoxMin() const
{
    return _min;
}

const QVector3D &GeometricModel::getBoundingBoxMax() const
{
    return _max;
}

const AbstractFace &GeometricModel::getBlockFace(int block_index, int face_index) const
{
    return getFace(getBlock(block_index).getFaceIndex(face_index));
}

const QVector3D &GeometricModel::getBlockPoint(int block_index, int face_index, int point_index) const
{
    int face = getBlock(block_index).getFaceIndex(face_index);
    int point = getFace(face).getPoint(point_index);
    return getPoint(point);
}

const QVector3D &GeometricModel::getFacePoint(int face_index, int point_index) const
{
    return getPoint(getFace(face_index).getPoint(point_index));
}

template<typename T>
void GeometricModel::ClearListData(QList<T*>& list)
{
    if (!list.isEmpty())
    {
        int n = list.size();
        for (int i=0; i<n; i++)
            delete list[i];
        list.clear();
    }
}
void GeometricModel::ClearLists()
{
    ClearListData(_points);
    ClearListData(_faces);
    ClearListData(_blocks);
}

void GeometricModel::SetBoundingBox()
{
    if (_points.isEmpty())
    {
        _max *= 0.f;
        _min = _max;
    }
    else
    {
        _max = *(_points[0]);
        _min = _max;

        int n = _points.size();
        for (int i=1; i<n; i++)
        {
            const QVector3D& tmp = *(_points[i]);
            for (int j=0; j<3; j++)
            {
                if (tmp[j] > _max[j])
                    _max[j] = tmp[j];
                if (tmp[j] < _min[j])
                    _min[j] = tmp[j];
            }
        }
    }
}

void GeometricModel::reset()
{
    ClearLists();

    BlockFactory factory(&_points,&_faces,&_blocks);
    int n = _model->getPartsNum();
    for (int i=0; i<n; i++)
        factory.createBlock(_model->getPart(i));

    SetBoundingBox();
}
