/*

#include "anchormark.h"
#include "Service/pointsswitcher.h"
#include <cmath>

AnchorMarker::AnchorMarker(const AbstractScene *scene, const AbstractGraphicModel *model)
{
    _model = model;
    _scene = scene;
    _img = NULL;
}

AnchorMarker::~AnchorMarker()
{
    if (_img != NULL)
        delete _img;
}

void AnchorMarker::setMark(int x, int y)
{
    clear();
    AddIndex(x,y);
    AddAnchor(x,y);
}

void AnchorMarker::setMark(const QPoint &point)
{
    clear();
    AddIndex(point.x(), point.y());
    AddAnchor(point.x(), point.y());
}

void AnchorMarker::addMark(int x, int y)
{
    AddIndex(x,y);
    AddAnchor(x,y);
}

void AnchorMarker::addMark(const QPoint &point)
{
    AddIndex(point.x(), point.y());
    AddAnchor(point.x(), point.y());
}

void AnchorMarker::clear()
{
    _indexes.clear();
    _boxes.clear();
}

void AnchorMarker::redraw()
{
    SetImage();
    for (int i=0; i<_boxes.size(); i++)
        Draw(_boxes[i],_indexes[i]);
}

const QImage &AnchorMarker::getImage() const
{
    return *_img;
}

bool AnchorMarker::isNull() const
{
    return _indexes.isEmpty();
}

const QColor &AnchorMarker::getColor() const
{
    return _color;
}

void AnchorMarker::setColor(const QColor &color)
{
    _color = color;
}

int AnchorMarker::getMarksNum() const
{
    return _indexes.size();
}

AnchorMarker::AnchorMarker()
{
    _img = NULL;
}

void AnchorMarker::AddIndex(int x, int y)
{
    _indexes.append(_scene->getBuffers().getBlock(x,y));
}

void AnchorMarker::AddAnchor(int x, int y)
{   
    QVector3D in = GetInPoint(x,y);

    PointsSwitcher switcher(_model->getCamera(),_model->getScale());
    QList<QPoint> points;
    points.append(switcher.createPointWithCameraCS(in).toPoint());
    in.setX(in.x() + 1.f);
    points.append(switcher.createPointWithCameraCS(in).toPoint());
    in.setX(in.y() + 1.f);
    points.append(switcher.createPointWithCameraCS(in).toPoint());
    in.setX(in.x() - 1.f);
    points.append(switcher.createPointWithCameraCS(in).toPoint());

    QRect box(points[0],points[0]);
    for (int i=0; i<points.size(); i++)
    {
        if (points[i].x() < box.left())
            box.setLeft(points[i].x());
        if (points[i].x() > box.right())
            box.setRight(points[i].x());
        if (points[i].y() < box.top())
            box.setTop(points[i].y());
        if (points[i].y() > box.bottom())
            box.setBottom(points[i].y());
    }

    _boxes.append(box);
}

void AnchorMarker::FixFirstModelPoint(QVector3D &point) const
{
    point.setZ(roundf(point.z()));
    point.setX(truncf((point.x() < 0)? point.x()-1: point.x()));
    point.setY(truncf((point.y() < 0)? point.y()-1: point.y()));
}

void AnchorMarker::SetImage()
{
    if (_img != NULL)
        delete _img;
    _img = new QImage(_scene->getImage());
}

void AnchorMarker::Draw(const QRect& box, int block_index)
{
    const AbstractBuffer& buf = _scene->getBuffers();
    for (int i=box.left(); i<=box.right(); i++)
        for (int j=box.top(); j<=box.bottom(); j++)
            if (buf.getHorizontalPolygon(i,j) && (buf.getBlock(i,j) == block_index))
                _img->setPixel(i,j,_color.value());
}

QVector3D AnchorMarker::GetInPoint(int x, int y) const
{
    PointsSwitcher switcher(_model->getCamera(),_model->getScale());
    QVector3D point = switcher.createModelPoint(x,y,_scene->getBuffers());
    FixFirstModelPoint(point);
    return point;
}

*/
