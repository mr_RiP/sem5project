#ifndef FACE_H
#define FACE_H

#include "Abstract/abstractface.h"
#include <QList>

class Face : public AbstractFace
{
public:
    Face(int a, int b, int c);

    int getPoint(int index) const;
    int getPointA() const;
    int getPointB() const;
    int getPointC() const;
    bool isEqual(const AbstractFace& face) const;

private:
    int _point[3];

    Face();
    bool IsValEqual(int a, int b, int c) const;
};

#endif // FACE_H
