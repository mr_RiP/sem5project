#include "modelpainter.h"
#include "Service/facepainter.h"
#include "Service/zerofloorpainter.h"
#include <stdexcept>
#include <cfloat>

#ifdef DEBUG
#include <cstdio>
#endif

ModelPainter::ModelPainter(const AbstractGraphicModel *graphics, QSize size)
{
    if (graphics == NULL)
        throw std::invalid_argument("Trying to set a Model Painter with NULL graphic model.");

    _model = graphics;

    SetBufferConstants();
    SetDefaultColors();

    resize(size);
}

ModelPainter::~ModelPainter()
{
}

const QImage &ModelPainter::getImage() const
{
    return _img;
}

float ModelPainter::getZ(int x, int y) const
{
#ifdef DEBUG
    printf("z-buffer accessed at (%i,%i)\n",x,y);
#endif

    return _z.getValue(x,y);
}

float ModelPainter::getZ(QPoint point) const
{
#ifdef DEBUG
    printf("z-buffer accessed at (%i,%i)\n",point.x(),point.y());
#endif

    return _z.getValue(point);
}

int ModelPainter::getBlockIndex(int x, int y) const
{
#ifdef DEBUG
    printf("block-buffer accessed at (%i,%i)\n",x,y);
#endif

    return _block.getValue(x,y);
}

int ModelPainter::getBlockIndex(QPoint point) const
{   
#ifdef DEBUG
    printf("block-buffer accessed at (%i,%i)\n",point.x(),point.y());
#endif
    return _block.getValue(point);
}

bool ModelPainter::getAnchor(int x, int y) const
{
    return _anchor.getValue(x,y);
}

bool ModelPainter::getAnchor(QPoint point) const
{
    return _anchor.getValue(point);
}

void ModelPainter::resize(QSize size)
{
    _img = QImage(size,QImage::Format_RGB32);
    _z.setSize(size);
    _block.setSize(size);
    _anchor.setSize(size);

    redraw();
}

void ModelPainter::redraw()
{
    flush();
    //DrawZeroFloor();
    DrawModel();
}

void ModelPainter::flush()
{
    _img.fill(_background);
    _z.flush();
    _block.flush();
    _anchor.flush();
}

const QColor &ModelPainter::getBackgroundColor() const
{
    return _background;
}

const QColor &ModelPainter::getZeroFloorColor() const
{
    return _zero;
}

void ModelPainter::setBackgroundColor(const QColor &color)
{
    _background = color;
}

void ModelPainter::setZeroFloorColor(const QColor &color)
{
    _zero = color;
}

void ModelPainter::SetBufferConstants()
{
    _z.setNullValue(FLT_MAX);
    _z.setRestrictValue(-FLT_MAX);

    _block.setNullValue(-1);
    _block.setRestrictValue(-1);

    _anchor.setNullValue(false);
    _anchor.setRestrictValue(false);
}

void ModelPainter::DrawZeroFloor()
{
    ZeroFloorPainter(&_img, _model, _zero).draw();
}

void ModelPainter::DrawModel()
{
    FacePainter painter(_img, _z, _block, _anchor);

    int blocks_n = _model->getBlocksNum();
    for (int i=0; i<blocks_n; i++)
    {
        int faces_n = _model->getBlock(i).getFacesNum();
        painter.setBlockData(i,_model->getBlock(i).getColor());
        for (int j=0; j<faces_n; j++)
        {
            const AbstractFace& face = _model->getBlockFace(i,j);
            painter.draw(_model->getPoint(face.getPointA()),
                         _model->getPoint(face.getPointB()),
                         _model->getPoint(face.getPointC()),
                         FaceIsAnchor(j));
        }
    }
}

void ModelPainter::SetDefaultColors()
{
    _background = Qt::white;
    _zero = Qt::gray;
}

bool ModelPainter::FaceIsAnchor(int index) const
{
    return (index > 0) && (((index%8) == 0) || ((index%9) == 0));
}
