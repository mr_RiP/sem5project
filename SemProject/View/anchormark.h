/*

#ifndef ANCHORMARK_H
#define ANCHORMARK_H

#include "Abstract/abstractmarker.h"
#include "Abstract/abstractscene.h"
#include "Abstract/abstractgraphicmodel.h"

class AnchorMarker : public AbstractMarker
{
public:
    AnchorMarker(const AbstractScene* scene, const AbstractGraphicModel* model);
    ~AnchorMarker();

    void setMark(int x, int y);
    void setMark(const QPoint& point);

    void addMark(int x, int y);
    void addMark(const QPoint& point);

    void clear();
    void redraw();

    const QImage& getImage() const;
    bool isNull() const;

    const QColor& getColor() const;
    void setColor(const QColor& color);

    int getMarksNum() const;

protected:
    const AbstractScene* _scene;
    const AbstractGraphicModel* _model;
    QImage *_img;
    QList<QRect> _boxes;
    QList<int> _indexes;
    QColor _color;

    AnchorMarker();

    void AddIndex(int x, int y);
    void AddAnchor(int x, int y);
    void FixFirstModelPoint(QVector3D& point) const;
    void SetImage();
    void Draw(const QRect &box, int block_index);
    QVector3D GetInPoint(int x, int y) const;
};

#endif // ANCHORMARK_H

*/
