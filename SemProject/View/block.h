#ifndef BLOCK_H
#define BLOCK_H

#include "Abstract/abstractblock.h"

class Block : public AbstractBlock
{
public:
    Block(const QList<int>& faces, const AbstractPart* part);
    ~Block();

    int getFaceIndex(int index) const;
    int getFacesNum() const;

    const QColor& getColor() const;
    const AbstractPart* getPart() const;

protected:
    QList<int> _faces;
    const AbstractPart* _part;

    Block();
};


#endif // BLOCK_H
