#include "face.h"
#include <stdexcept>

Face::Face(int a, int b, int c)
{
    if ((a == b) || (a == c) || (b == c))
        throw std::invalid_argument("Trying to create a face with equal points.");

    _point[0] = a;
    _point[1] = b;
    _point[2] = c;
}

Face::Face()
{

}

bool Face::IsValEqual(int a, int b, int c) const
{
    return (_point[0] == a) && (_point[1] == b) && (_point[2] == c);
}

int Face::getPoint(int index) const
{
    if ((index < 0) || (index > 2))
        throw std::invalid_argument("Trying to get point from Face using incorrect index.");
    return _point[index];
}

int Face::getPointA() const
{
    return _point[0];
}

int Face::getPointB() const
{
    return _point[1];
}

int Face::getPointC() const
{
    return _point[2];
}

bool Face::isEqual(const AbstractFace& face) const
{
    int a = face.getPointA();
    int b = face.getPointB();
    int c = face.getPointC();

    return  IsValEqual(a,b,c) || IsValEqual(b,c,a) || IsValEqual(c,a,b) ||
            IsValEqual(b,a,c) || IsValEqual(a,c,b) || IsValEqual(c,b,a);

}
