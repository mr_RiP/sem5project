#include "modelpalette.h"

ModelPalette::ModelPalette(QWidget *parent) : QListWidget(parent)
{
    setSelectionMode(QAbstractItemView::SingleSelection);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    _master = NULL;

    connect(this,&QListWidget::itemChanged,this,&ModelPalette::FixName);
}

ModelPalette::~ModelPalette()
{
    clear();
}

void ModelPalette::setController(const AbstractPaletteController *master)
{   
    if (_master != NULL)
        disconnect(_master,SIGNAL(destroyed()),this,SLOT(ControllerDestroyedEvent()));

    _master = master;

    if (_master != NULL)
        connect(_master,SIGNAL(destroyed()),this,SLOT(ControllerDestroyedEvent()));
}

bool ModelPalette::isReady() const
{
    return (_master != NULL) && (_master->isReady());
}

int ModelPalette::getIndex() const
{
    return currentRow();
}

void ModelPalette::update()
{
    clear();
    PrintItemList();
}

void ModelPalette::FixName(QListWidgetItem *item)
{
    int i = row(item);
    if ((item->text() != _master->getItem(i)->getName()) && (_master->isNameExists(item->text())))
    {
        item->setText(_master->getItem(i)->getName());
        emit nameNotChanged(i);
    }
    else
    {
        emit nameChanged(i);
    }
}

void ModelPalette::ControllerDestroyedEvent()
{
    _master = NULL;
}

void ModelPalette::PrintItemList()
{
    int n = _master->getItemsNum();
    for (int i=0; i<n; i++)
        insertItem(i,GetNewItem(i));
}

QListWidgetItem *ModelPalette::GetNewItem(int index) const
{
    QListWidgetItem *item = new QListWidgetItem;
    item->setText(_master->getItem(index)->getName());
    item->setFlags(item->flags()|Qt::ItemIsEditable);
    return item;
}
