#ifndef SELECTIONTOOL_H
#define SELECTIONTOOL_H

#include <QWidget>
#include "Abstract/abstracttool.h"
#include <QInputDialog>
#include <QColorDialog>

namespace Ui {
class SelectionTool;
}

class SelectionTool : public AbstractTool
{
    Q_OBJECT

public:
    explicit SelectionTool(QWidget *parent = 0);
    ~SelectionTool();

    void setData(AbstractModel *model, AbstractView *view);
    bool isReady() const;
    void clear();

    const QImage &getImage() const;

    void mousePressAction(QPoint pos);

public slots:
    void redraw();

private slots:
    void ViewDestroyedEvent();
    void ColorChangeEvent();

    void on_clearButton_clicked();

    void on_deleteButton_clicked();

    void on_pushButton_clicked();

protected:
    void SetDefaultColor();
    void ResetImage();
    void DrawSelections();
    void DrawBlockBox(int index);
    void SetBoxPoints(QList<QPoint> &points, const AbstractPart *part);
    void DrawBoxLines(QList<QPoint> &points);

    void ClearSelection();
    void DeleteSelectedParts();
    void ChangeDetailColor();

    void ErrMsgCantDelete();

private:
    Ui::SelectionTool *ui;

    AbstractModel* _model;
    AbstractView* _view;

    QImage _img;
    QList<int> _selected;
    QColor _color;
    QColorDialog _color_dlg;

    void SetCompositeNameDialog();
    void SetColorDialog();
};

#endif // SELECTIONTOOL_H
