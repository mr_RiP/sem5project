#ifndef DETAILPALETTECONTROLLER_H
#define DETAILPALETTECONTROLLER_H

#include "Abstract/abstractpalettecontroller.h"
#include <QObject>
#include "newdetaildialog.h"

class DetailPaletteController : public AbstractPaletteController
{
    Q_OBJECT

public:
    DetailPaletteController();
    DetailPaletteController(AbstractModel* model);
    ~DetailPaletteController();

    void setModel(AbstractModel* model);
    bool isReady() const;

    void showCreationDialog();

    bool isIndexCorrect(int index) const;

    const AbstractDesign *getItem(int index) const;
    int getItemsNum() const;
    bool isNameExists(const QString& name) const;
    bool isItemCanBeDeleted(int index) const;
    void deleteItem(int index);
    void setItemName(int index, const QString& name) const;
    bool isItemCanBeCreated() const;

    const AbstractModel* getModel() const;

    void errMsgIncorrectName(QWidget* parent) const;
    void errMsgCantDelete(QWidget* parent) const;
    void errMsgCantCreate(QWidget* parent) const;

private slots:
    void CheckDialogData();
    void ModelDestroyedEvent();
    void UpdateEvent();

private:
    AbstractModel* _model;
    NewDetailDialog _dlg;

protected:
    void UpdateDialog();
    void AddNewDetail();
};

#endif // DETAILPALETTECONTROLLER_H
