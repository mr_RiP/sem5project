#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>
#include "modelpalettewidget.h"
#include "toolboxwidget.h"
#include "../Model/Abstract/abstractmodel.h"

namespace Ui {
class SettingsWidget;
}

class SettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsWidget(QWidget *parent = 0);
    ~SettingsWidget();
    void setModel(const AbstractModel* model);
    void setDesignPalette(const ModelPaletteWidget* widget);
    void setDetailPalette(const ModelPaletteWidget* widget);
    void setModelScreen(const ModelScreen* widget);

    bool isReady();

public slots:
    void update();

private slots:
    void DesignPaletteDestroyedEvent();
    void DetailPaletteDestroyedEvent();
    void ModelScreenDestroyedEvent();
    void ModelDestroyedEvent();

    void PartSelectedEvent(int index);
    void DetailSelectedEvent(int index);
    void DesignSelectedEvent(int index);
    void ObjectDeletedEvent(int index);

protected:
    enum ObjType
    {
        None,
        Design,
        Detail,
        Part
    };

    int GetObjectsNum() const;
    void PrintData();
    void ChangeObjectType(ObjType type);

private:
    Ui::SettingsWidget *ui;
    const AbstractModel* _model;
    const ModelPaletteWidget* _designs;
    const ModelPaletteWidget* _details;
    const ModelScreen* _parts;

    ObjType _type;
    int _index;

    void ConnectObject();
    void DisconnectObject();
};

#endif // SETTINGSWIDGET_H
