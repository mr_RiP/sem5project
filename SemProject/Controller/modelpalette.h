#ifndef MODELPALETTE_H
#define MODELPALETTE_H

#include <QListWidget>
#include "../Model/Abstract/abstractmodel.h"
#include "Abstract/abstractpalettecontroller.h"

class ModelPalette : public QListWidget
{
    Q_OBJECT
public:
    explicit ModelPalette(QWidget *parent = 0);
    ~ModelPalette();

    void setController(const AbstractPaletteController* master);
    bool isReady() const;
    int getIndex() const;

signals:
    void nameChanged(int index);
    void nameNotChanged(int index);

public slots:
    void update();

private slots:
    void FixName(QListWidgetItem* item);
    void ControllerDestroyedEvent();

protected:
    void PrintItemList();
    QListWidgetItem* GetNewItem(int index) const;

private:
    const AbstractPaletteController* _master;
};

#endif // MODELPALETTE_H
