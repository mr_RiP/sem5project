#include "newdesigndialog.h"
#include "ui_newdesigndialog.h"

NewDesignDialog::NewDesignDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewDesignDialog)
{
    ui->setupUi(this);
    setModal(true);
}

NewDesignDialog::~NewDesignDialog()
{
    delete ui;
}

int NewDesignDialog::getLength() const
{
    return ui->length->value();
}

int NewDesignDialog::getWidth() const
{
    return ui->width->value();
}

int NewDesignDialog::getHeight() const
{
    return ui->height->value();
}

NewDesignDialog::Type NewDesignDialog::getType() const
{
    if (ui->simpleDesign->isChecked())
        return Simple;
    else
    {
        if (ui->upperCut->isChecked())
            return CuttedUpper;
        else
            return CuttedLower;
    }
}

QString NewDesignDialog::getName() const
{
    return ui->nameLineEdit->text();
}

void NewDesignDialog::setName(const QString &name)
{
    ui->nameLineEdit->setText(name);
}

void NewDesignDialog::on_simpleDesign_clicked()
{
    ui->cuttedDesign->setChecked(false);
    ui->additionalSettingsBox->setEnabled(false);
    ui->length->setMinimum(1);
}

void NewDesignDialog::on_cuttedDesign_clicked()
{
    ui->simpleDesign->setChecked(false);
    ui->additionalSettingsBox->setEnabled(true);
    ui->upperCut->setChecked(true);
    ui->lowerCut->setChecked(false);
    ui->length->setMinimum(2);
}

void NewDesignDialog::on_upperCut_clicked()
{
    ui->lowerCut->setChecked(false);
}

void NewDesignDialog::on_lowerCut_clicked()
{
    ui->upperCut->setChecked(false);
}
