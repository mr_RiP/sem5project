#ifndef NEWDESIGNDIALOG_H
#define NEWDESIGNDIALOG_H

#include <QDialog>
#include "../Model/Abstract/abstractmodel.h"
#include "modelpalette.h"

namespace Ui {
class NewDesignDialog;
}

class NewDesignDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewDesignDialog(QWidget *parent = 0);
    ~NewDesignDialog();

    enum Type
    {
        Simple,
        CuttedUpper,
        CuttedLower
    };

    int getLength() const;
    int getWidth() const;
    int getHeight() const;
    Type getType() const;

    QString getName() const;
    void setName(const QString& name);

private slots:
    void on_simpleDesign_clicked();

    void on_cuttedDesign_clicked();

    void on_upperCut_clicked();

    void on_lowerCut_clicked();

private:
    Ui::NewDesignDialog *ui;
};

#endif // NEWDESIGNDIALOG_H
