#ifndef MODELSCREEN_H
#define MODELSCREEN_H

#include <QWidget>
#include <QMouseEvent>
#include "../View/Abstract/abstractview.h"
#include "../Model/Abstract/abstractmodel.h"
#include "Abstract/abstracttool.h"

class ModelScreen : public QWidget
{
    Q_OBJECT
public:
    explicit ModelScreen(QWidget *parent = 0);
    ~ModelScreen();

    void setModel(AbstractModel *model);
    const AbstractModel* getModel() const;
    const AbstractView* getView() const;

    bool isReady() const;
    bool isToolReady() const;

    void setTool(AbstractTool* tool);

signals:
    void partSelected(int index);

public slots:
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void wheelEvent(QWheelEvent *event);
    void reset();

private slots:
    void ModelDestroyedEvent();
    void ToolDestroyedEvent();

protected: 
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
    void SetToolConnections();

private:
    AbstractModel* _model;

    AbstractView* _view;
    AbstractTool* _tool;

    QPoint _prev;
    bool _camera;
};

#endif // MODELSCREEN_H
