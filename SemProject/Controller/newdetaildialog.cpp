#include "newdetaildialog.h"
#include "ui_newdetaildialog.h"
#include "Service/colorbox.h"

NewDetailDialog::NewDetailDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewDetailDialog)
{
    ui->setupUi(this);
    setModal(true);
    _dlg = new QColorDialog(this);
    setColorDialogRus();
    _dlg->setCurrentColor(ui->colorValue->getColor());
    connect(_dlg,&QDialog::accepted,this,&NewDetailDialog::SetColorFromDialog);
}

NewDetailDialog::~NewDetailDialog()
{
    delete ui;
    delete _dlg;
}

QString NewDetailDialog::getName() const
{
    return ui->nameLine->text();
}

int NewDetailDialog::getDesignIndex() const
{
    return ui->designBox->currentIndex();
}

const QColor &NewDetailDialog::getColor() const
{
    return ui->colorValue->getColor();
}

void NewDetailDialog::clearList()
{
    ui->designBox->clear();
}

void NewDetailDialog::insertNameToList(int index, const QString &name)
{
    ui->designBox->insertItem(index,name);
}

void NewDetailDialog::setDesignIndex(int index)
{
    ui->designBox->setCurrentIndex(index);
}

void NewDetailDialog::SetColorFromDialog()
{
    ui->colorValue->setColor(_dlg->currentColor());
}

void NewDetailDialog::on_changeColorButton_clicked()
{
    _dlg->show();
}

void NewDetailDialog::setColorDialogRus()
{
    _dlg->setLocale(QLocale::Russian);
    _dlg->setWindowTitle("Выбор цвета");
}
