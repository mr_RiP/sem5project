#include "settingstable.h"

SettingsTable::SettingsTable(QWidget *parent) : QTableWidget(parent)
{
    setEditTriggers(QTableWidget::NoEditTriggers);
    setNull();
}

void SettingsTable::setData(const AbstractDesign *design)
{
    setNull();
    PrintDesignData(design);
    emit dataPrinted();
}

void SettingsTable::setData(const AbstractDetail *detail)
{
    setNull();
    PrintDetailData(detail);
    PrintDesignData(detail->getDesign());
    emit dataPrinted();
}

void SettingsTable::setData(const AbstractPart *part)
{
    setNull();
    PrintPartData(part);
    PrintDetailData(part->getDetail());
    PrintDesignData(part->getDetail()->getDesign());
    emit dataPrinted();
}

void SettingsTable::setNull()
{
    clearContents();
    setRowCount(0);
}

void SettingsTable::PrintDesignData(const AbstractDesign *design)
{
    int begin = rowCount();
    int len = 2;
    setRowCount(begin+len);
    int i = begin;
    PrintHeaderRow(i++,"Дизайн",true);
    PrintRow(i++,"Имя",design->getName());
    PrintDesignTypeData(design);
    PrintDesignStatsData(design);
}

void SettingsTable::PrintDetailData(const AbstractDetail *detail)
{
    int begin = rowCount();
    int len = 3;
    setRowCount(begin+len);
    int i = begin;

    PrintHeaderRow(i++,"Деталь",true);
    PrintRow(i++,"Имя",detail->getName());
    PrintRow(i,"Цвет","");
    item(i,1)->setBackgroundColor(detail->getColor());
}

void SettingsTable::PrintPartData(const AbstractPart *part)
{
    int row = rowCount();
    setRowCount(row+1);
    PrintHeaderRow(row,"Часть модели",true);
    PrintPosData(part);
}

void SettingsTable::PrintPosData(const AbstractPosedDesign *posed_design)
{
    int begin = rowCount();
    int len = 5;
    setRowCount(begin+len);
    int i = begin;
    const AbstractPosition* pos = posed_design->getPosition();

    PrintHeaderRow(i++,"Расположение",false);
    PrintRow(i++,"x",QString::number(pos->getX()));
    PrintRow(i++,"y",QString::number(pos->getY()));
    PrintRow(i++,"z",QString::number(pos->getZ()));
    PrintRow(i,"Поворот",QString::number(pos->getRotation()));
}

void SettingsTable::PrintHeaderRow(int row, const QString &text, bool bold)
{
    PrintRow(row,text,"");
    item(row,0)->setBackgroundColor(Qt::lightGray);
    item(row,1)->setBackgroundColor(Qt::lightGray);
    if (bold)
    {
        QFont font = item(row,0)->font();
        font.setBold(true);
        item(row,0)->setFont(font);
    }
}

void SettingsTable::PrintRow(int row, const QString &property, const QString &value)
{
    setItem(row,0,new QTableWidgetItem(property));
    setItem(row,1,new QTableWidgetItem(value));
}

void SettingsTable::PrintDesignTypeData(const AbstractDesign *design)
{
    if (design->getSubdesignsNum() == 0)
        PrintDesignCoreTypeData(design);
    else
    {
        int begin = rowCount();
        int len = 2;
        setRowCount(begin+len);
        int i = begin;
        PrintRow(i++,"Тип","Cоставной");
        PrintRow(i,"Кол-во частей",QString::number(design->getSubdesignsNum()));
    }
}

void SettingsTable::PrintDesignStatsData(const AbstractDesign *design)
{
    if (design->getSubdesignsNum() == 0)
        PrintDesignCoreStatsData(design);
    else
    {
        int n = design->getSubdesignsNum();
        for (int i=0; i<n; i++)
        {
            int row = rowCount();
            setRowCount(row+1);
            PrintHeaderRow(row,QString("Часть дизайна %1").arg(i+1),true);
            PrintDesignCoreTypeData(design->getSubdesign(i)->getDesignCore());
            PrintDesignCoreStatsData(design->getSubdesign(i)->getDesignCore());
            PrintPosData(design->getSubdesign(i));
        }
    }
}

void SettingsTable::PrintDesignCoreTypeData(const AbstractDesignCore *design)
{
    if (design->isSimple())
    {
        int i = rowCount();
        setRowCount(i + 1);
        PrintRow(i,"Тип","Простой");
    }
    else
    {
        int begin = rowCount();
        int len = 2;
        setRowCount(begin+len);
        int i = begin;
        PrintRow(i++,"Тип","Со срезанной гранью");
        PrintRow(i,"Тип среза",(design->isCuttedLower())? "Нижний": "Верхний");
    }
}

void SettingsTable::PrintDesignCoreStatsData(const AbstractDesignCore *design)
{
    int begin = rowCount();
    int len = 4;
    setRowCount(begin + len);
    int i = begin;
    PrintHeaderRow(i++,"Характеристики",false);
    PrintRow(i++,"Длина",QString::number(design->getLength()));
    PrintRow(i++,"Ширина",QString::number(design->getWidth()));
    PrintRow(i,"Высота",QString::number(design->getHeight()));
}
