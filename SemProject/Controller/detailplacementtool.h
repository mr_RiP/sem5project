#ifndef DETAILPLACEMENTTOOL_H
#define DETAILPLACEMENTTOOL_H

#include "Abstract/abstracttool.h"
#include "modelpalettewidget.h"

namespace Ui {
class DetailPlacementTool;
}

class DetailPlacementTool : public AbstractTool
{
    Q_OBJECT
public:
    explicit DetailPlacementTool(QWidget *parent = 0);
    ~DetailPlacementTool();

    void setData(AbstractModel *model, AbstractView *view);
    bool isReady() const;
    void clear();
    const QImage &getImage() const;
    void mousePressAction(QPoint pos);

    void setPalette(const ModelPaletteWidget* palette);

public slots:
    void redraw();    

private slots:
    void PaletteDestroyedEvent();
    void SettingsChangedEvent(int index);
    void PosChangedEvent(int val);

    void on_resetButton_clicked();
    void on_unselectButton_clicked();
    void on_placeButton_clicked();

    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::DetailPlacementTool *ui;
    AbstractModel* _model;
    AbstractView* _view;
    QImage _img;

    const ModelPaletteWidget* _palette;
    bool _selected;
    bool _valid;
    QVector3D _point;

    void SetPosConnections();

protected:
    QVector3D GetZeroFloorPoint(QPoint point);
    QVector3D GetAnchorPoint(QPoint point);
    void DrawSelection();

    QList<QPoint> GetVectorsList() const;
    QColor GetSelectionColor() const;
    void DrawLines(const QList<QPoint>& list, const QColor& color);

    void SetSelectionValidation();
    void PlaceSelectedDetail();
    void ClearSelectionData();

    void ErrMsgInvalidAnchor();
    void ErrMsgNoAnchor();
};

#endif // DETAILPLACEMENTTOOL_H
