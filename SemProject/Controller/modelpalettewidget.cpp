#include "modelpalettewidget.h"
#include "ui_modelpalettewidget.h"

ModelPaletteWidget::ModelPaletteWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ModelPaletteWidget)
{
    ui->setupUi(this);
    _controller = NULL;

    connect(ui->paletteList,&ModelPalette::nameNotChanged,
            this,&ModelPaletteWidget::NameChangeFailed);
    connect(ui->paletteList,&ModelPalette::nameChanged,
            this,&ModelPaletteWidget::ChangeName);
}

ModelPaletteWidget::~ModelPaletteWidget()
{
    delete ui;
    if (_controller != NULL)
        delete _controller;
}

void ModelPaletteWidget::setController(AbstractPaletteController *controller)
{
    if (_controller != NULL)
        delete _controller;

    _controller = controller;
    ui->paletteList->setController(_controller);

    if (_controller != NULL)
    {
        connect(_controller,SIGNAL(updateNeeded()),this,SLOT(update()));
        connect(_controller,SIGNAL(destroyed()),this,SLOT(ControllerDestroyedEvent()));
    }
}

bool ModelPaletteWidget::isReady() const
{
    return (_controller != NULL) && (_controller->isReady()) && (ui->paletteList->isReady());
}

const AbstractPaletteController *ModelPaletteWidget::getController() const
{
    return _controller;
}

const AbstractModel *ModelPaletteWidget::getModel() const
{
    return _controller->getModel();
}

QString ModelPaletteWidget::getItemText(int i) const
{
    return ui->paletteList->item(i)->text();
}

int ModelPaletteWidget::getItemsNum() const
{
    return ui->paletteList->count();
}

int ModelPaletteWidget::getIndex() const
{
    return ui->paletteList->getIndex();
}

void ModelPaletteWidget::update()
{
    if (isReady())
    {
        ui->paletteList->update();
        emit paletteUpdated();
    }
}

void ModelPaletteWidget::ControllerDestroyedEvent()
{
    _controller = NULL;
}

void ModelPaletteWidget::on_createButton_clicked()
{
    if (isReady())
    {
        if(_controller->isItemCanBeCreated())
            _controller->showCreationDialog();
        else
            _controller->errMsgCantCreate(this);
    }
}

void ModelPaletteWidget::on_deleteButton_clicked()
{
    if (isReady())
    {
        int index = ui->paletteList->currentRow();
        if (_controller->isIndexCorrect(index))
        {
            if (_controller->isItemCanBeDeleted(index))
            {
                _controller->deleteItem(index);
                ui->paletteList->update();
                emit deleteSuccess(index);
            }
            else
            {   _controller->errMsgCantDelete(this);
                emit deleteFailure(index);
            }
        }
    }
}

void ModelPaletteWidget::NameChangeFailed(int index)
{
    if (isReady())
        _controller->errMsgIncorrectName(this);
}

void ModelPaletteWidget::ChangeName(int index)
{
    if (isReady())
        _controller->setItemName(index,ui->paletteList->item(index)->text());
}

void ModelPaletteWidget::on_paletteList_currentRowChanged(int currentRow)
{
    emit indexChanged(currentRow);
}

void ModelPaletteWidget::on_paletteList_itemClicked(QListWidgetItem *item)
{
    emit itemClicked(ui->paletteList->currentRow());
}
