#ifndef SETTINGSTABLE_H
#define SETTINGSTABLE_H

#include <QTableWidget>
#include "../Model/Abstract/abstractdesign.h"
#include "../Model/Abstract/abstractdetail.h"
#include "../Model/Abstract/abstractpart.h"

class SettingsTable : public QTableWidget
{
    Q_OBJECT
public:
    explicit SettingsTable(QWidget *parent = 0);

    void setData(const AbstractDesign* design);
    void setData(const AbstractDetail* detail);
    void setData(const AbstractPart* part);
    void setNull();

signals:
    void dataPrinted();

public slots:

protected:
    void PrintDesignData(const AbstractDesign* design);
    void PrintDetailData(const AbstractDetail* detail);
    void PrintPartData(const AbstractPart* part);

    void PrintPosData(const AbstractPosedDesign* posed_design);
    void PrintHeaderRow(int row, const QString& text, bool bold=true);
    void PrintRow(int row, const QString& property, const QString& value);

    void PrintDesignTypeData(const AbstractDesign* design);
    void PrintDesignStatsData(const AbstractDesign* design);

    void PrintDesignCoreTypeData(const AbstractDesignCore* design);
    void PrintDesignCoreStatsData(const AbstractDesignCore* design);
};

#endif // SETTINGSTABLE_H
