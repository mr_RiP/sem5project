#include "designpalettecontroller.h"
#include <QMessageBox>

DesignPaletteController::DesignPaletteController()
{
    _model = NULL;
    connect(&_dlg,&QDialog::accepted,this,&DesignPaletteController::DesignCheck);
}

DesignPaletteController::DesignPaletteController(AbstractModel *model)
{
    _model = NULL;
    connect(&_dlg,&QDialog::accepted,this,&DesignPaletteController::DesignCheck);
    setModel(model);
}

DesignPaletteController::~DesignPaletteController()
{

}

void DesignPaletteController::setModel(AbstractModel *model)
{
    if (_model != NULL)
    {
        disconnect(_model,SIGNAL(destroyed()),this,SLOT(ModelDestroyedEvent()));
        disconnect(_model,SIGNAL(designsChanged()),this,SLOT(UpdateEvent()));
    }
    _model = model;

    if (_model != NULL)
    {
        connect(_model,SIGNAL(destroyed()),this,SLOT(ModelDestroyedEvent()));
        connect(_model,SIGNAL(designsChanged()),this,SLOT(UpdateEvent()));
    }
}

bool DesignPaletteController::isReady() const
{
    return (_model != NULL);
}

int DesignPaletteController::getItemsNum() const
{
    if (isReady())
        return _model->getDesignsNum();
    else
        return -1;
}

bool DesignPaletteController::isNameExists(const QString &name) const
{
    if (isReady())
        return _model->isDesignNameExists(name);
    else
        return false;

}

void DesignPaletteController::showCreationDialog()
{
    if (isReady())
        _dlg.show();
}

bool DesignPaletteController::isIndexCorrect(int index) const
{
    if (isReady())
        return (index >= 0) && (index < _model->getDesignsNum());
    else
        return false;
}

const AbstractDesign *DesignPaletteController::getItem(int index) const
{
    if (isReady())
        return _model->getDesign(index);
    else
        return NULL;
}

bool DesignPaletteController::isItemCanBeDeleted(int index) const
{
    if (isReady())
        return _model->isDesignCanBeDeleted(index);
    else
        return false;
}

void DesignPaletteController::deleteItem(int index)
{
    if (isReady())
        _model->deleteDesign(index);
}

void DesignPaletteController::setItemName(int index, const QString &name) const
{
    if (isReady())
        _model->setDesignName(index,name);
}

bool DesignPaletteController::isItemCanBeCreated() const
{
    return true;
}

const AbstractModel *DesignPaletteController::getModel() const
{
    return _model;
}

void DesignPaletteController::DesignCheck()
{
    if (isReady())
    {
        if (_model->isDesignNameExists(_dlg.getName()))
        {
            _dlg.show();
            errMsgIncorrectName(&_dlg);
        }
        else
            AddDesign();
    }
}

void DesignPaletteController::ModelDestroyedEvent()
{
    _model = NULL;
}

void DesignPaletteController::UpdateEvent()
{
    emit updateNeeded();
}

void DesignPaletteController::AddDesign()
{
    if (isReady())
    {
        switch (_dlg.getType())
        {
        case NewDesignDialog::Simple:
            _model->addDesignSimple(_dlg.getLength(),_dlg.getWidth(),_dlg.getHeight(),_dlg.getName());
            break;
        case NewDesignDialog::CuttedLower:
            _model->addDesignCuttedLower(_dlg.getLength(),_dlg.getWidth(),_dlg.getHeight(),_dlg.getName());
            break;
        case NewDesignDialog::CuttedUpper:
            _model->addDesignCuttedUpper(_dlg.getLength(),_dlg.getWidth(),_dlg.getHeight(),_dlg.getName());
            break;
        default:
            break;
        }
    }
}

void DesignPaletteController::errMsgCantDelete(QWidget *parent) const
{
    QString title("Удаление невозможно");
    QString text("Данный дизайн используется в основе некоторой детали.");
    QMessageBox::critical(parent,title,text,QMessageBox::Ok);
}

void DesignPaletteController::errMsgCantCreate(QWidget *parent) const
{
    QString title("Действите невозможно");
    QString text("Произошла необъяснимая ошибка.");
    QMessageBox::critical(parent,title,text,QMessageBox::Ok);
}

void DesignPaletteController::errMsgIncorrectName(QWidget *parent) const
{
    QString title("Ошибка");
    QString text("Выбранное имя уже используется другим дизайном.");
    QMessageBox::critical(parent,title,text,QMessageBox::Ok);
}
