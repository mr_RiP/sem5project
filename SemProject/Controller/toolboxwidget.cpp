#include "toolboxwidget.h"
#include "ui_toolboxwidget.h"
#include "modelpalettewidget.h"

ToolBoxWidget::ToolBoxWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ToolBoxWidget)
{
    ui->setupUi(this);
    _screen = NULL;
}

ToolBoxWidget::~ToolBoxWidget()
{
    delete ui;
}

void ToolBoxWidget::setScreen(ModelScreen *screen)
{
    if (_screen != NULL)
        disconnect(_screen,SIGNAL(destroyed()),this,SLOT(ScreenDestroyedEvent()));

    _screen = screen;

    if (_screen != NULL)
    {
        connect(_screen,SIGNAL(destroyed()),this,SLOT(ScreenDestroyedEvent()));
        SetTool(ui->toolBox->currentIndex());
    }
}

void ToolBoxWidget::setPalette(const ModelPaletteWidget *palette)
{
    ui->placePage->setPalette(palette);
}

bool ToolBoxWidget::isReady() const
{
    return (_screen != NULL);
}

void ToolBoxWidget::ScreenDestroyedEvent()
{
    _screen = NULL;
}

void ToolBoxWidget::SetTool(int index)
{
    switch (index)
    {
    case 0:
        _screen->setTool(ui->selectPage);
        break;
    case 1:
        _screen->setTool(ui->placePage);
        break;
    default:
        _screen->setTool(ui->selectPage);
        break;
    }
}

void ToolBoxWidget::on_toolBox_currentChanged(int index)
{
    SetTool(index);
}
