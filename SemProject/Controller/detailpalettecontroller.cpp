#include "detailpalettecontroller.h"
#include <QMessageBox>

DetailPaletteController::DetailPaletteController()
{
    _model = NULL;
    connect(&_dlg,&QDialog::accepted,this,&DetailPaletteController::CheckDialogData);
}

DetailPaletteController::DetailPaletteController(AbstractModel *model)
{
    _model = NULL;
    connect(&_dlg,&QDialog::accepted,this,&DetailPaletteController::CheckDialogData);
    setModel(model);
}

DetailPaletteController::~DetailPaletteController()
{

}

void DetailPaletteController::setModel(AbstractModel *model)
{
    if (_model != NULL)
    {
        disconnect(_model,SIGNAL(destroyed()),this,SLOT(ModelDestroyedEvent()));
        disconnect(_model,SIGNAL(detailsChanged()),this,SLOT(UpdateEvent()));
    }

    _model = model;

    if (_model != NULL)
    {
        connect(_model,SIGNAL(destroyed()),this,SLOT(ModelDestroyedEvent()));
        connect(_model,SIGNAL(detailsChanged()),this,SLOT(UpdateEvent()));
    }
}

bool DetailPaletteController::isReady() const
{
    return (_model != NULL);
}

void DetailPaletteController::showCreationDialog()
{
    if (isReady())
    {
        UpdateDialog();
        _dlg.show();
    }
}

bool DetailPaletteController::isIndexCorrect(int index) const
{
    if (isReady())
        return (index >= 0) && (index < _model->getDetailsNum());
    else
        return false;
}

const AbstractDesign *DetailPaletteController::getItem(int index) const
{
    if (isReady())
        return _model->getDetail(index);
    else
        return NULL;
}

bool DetailPaletteController::isItemCanBeDeleted(int index) const
{
    if (isReady())
        return _model->isDetailCanBeDeleted(index);
    else
        return false;
}

void DetailPaletteController::deleteItem(int index)
{
    if (isReady())
        _model->deleteDetail(index);
}

void DetailPaletteController::setItemName(int index, const QString &name) const
{
    if (isReady())
        _model->setDetailName(index,name);
}

bool DetailPaletteController::isItemCanBeCreated() const
{
    return (_model->getDesignsNum() > 0);
}

const AbstractModel *DetailPaletteController::getModel() const
{
    return _model;
}

void DetailPaletteController::errMsgCantDelete(QWidget *parent) const
{
    QString title("Удаление невозможно");
    QString text("Данная деталь задействованна в модели.");
    QMessageBox::critical(parent,title,text,QMessageBox::Ok);
}

void DetailPaletteController::errMsgCantCreate(QWidget *parent) const
{
    QString title("Действие невозможно");
    QString text("Для создания детали должен существовать хотя бы один дизайн.");
    QMessageBox::critical(parent,title,text,QMessageBox::Ok);
}

void DetailPaletteController::CheckDialogData()
{
    if (_model->isDetailNameExists(_dlg.getName()))
    {
        _dlg.show();
        errMsgIncorrectName(&_dlg);
    }
    else
        AddNewDetail();
}

void DetailPaletteController::ModelDestroyedEvent()
{
    _model = NULL;
}

void DetailPaletteController::UpdateEvent()
{
    emit updateNeeded();
}

void DetailPaletteController::UpdateDialog()
{
    _dlg.clearList();
    int n = _model->getDesignsNum();
    for (int i=0; i<n; i++)
        _dlg.insertNameToList(i,_model->getDesign(i)->getName());
    _dlg.setDesignIndex(0);
}

void DetailPaletteController::AddNewDetail()
{
    _model->addDetail(_dlg.getDesignIndex(),_dlg.getColor(),_dlg.getName());
}

void DetailPaletteController::errMsgIncorrectName(QWidget *parent) const
{
    QString title("Ошибка");
    QString text("Выбранное имя уже используется другой деталью.");
    QMessageBox::critical(parent,title,text,QMessageBox::Ok);
}

int DetailPaletteController::getItemsNum() const
{
    if (isReady())
        return _model->getDetailsNum();
    else
        return -1;
}

bool DetailPaletteController::isNameExists(const QString &name) const
{
    if (isReady())
        return _model->isDetailNameExists(name);
    else
        return false;

}
