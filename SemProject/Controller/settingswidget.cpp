#include "settingswidget.h"
#include "ui_settingswidget.h"

SettingsWidget::SettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsWidget)
{
    ui->setupUi(this);
    _model = NULL;
    _details = NULL;
    _designs = NULL;
    _parts = NULL;

    _type = None;
    _index = -1;
}

SettingsWidget::~SettingsWidget()
{
    delete ui;
}

void SettingsWidget::setModel(const AbstractModel *model)
{
    if (_model != NULL)
        disconnect(_model,SIGNAL(destroyed()),this,SLOT(ModelDestroyedEvent()));

    _model = model;

    if (_model != NULL)
        connect(_model,SIGNAL(destroyed()),this,SLOT(ModelDestroyedEvent()));
}

void SettingsWidget::setDesignPalette(const ModelPaletteWidget *widget)
{
    if (_designs != NULL)
    {
        disconnect(_designs,SIGNAL(destroyed()),this,SLOT(DesignPaletteDestroyedEvent()));
        disconnect(_designs,SIGNAL(itemClicked(int)),this,SLOT(DesignSelectedEvent(int)));
    }

    _designs = widget;

    if (_designs != NULL)
    {
        connect(_designs,SIGNAL(destroyed()),this,SLOT(DesignPaletteDestroyedEvent()));
        connect(_designs,SIGNAL(itemClicked(int)),this,SLOT(DesignSelectedEvent(int)));
    }
}

void SettingsWidget::setDetailPalette(const ModelPaletteWidget *widget)
{
    if (_details != NULL)
    {
        disconnect(_details,SIGNAL(destroyed()),this,SLOT(DetailPaletteDestroyedEvent()));
        disconnect(_details,SIGNAL(itemClicked(int)),this,SLOT(DetailSelectedEvent(int)));
    }

    _details = widget;

    if (_details != NULL)
    {
        connect(_details,SIGNAL(destroyed()),this,SLOT(DetailPaletteDestroyedEvent()));
        connect(_details,SIGNAL(itemClicked(int)),this,SLOT(DetailSelectedEvent(int)));
    }

}

void SettingsWidget::setModelScreen(const ModelScreen* widget)
{
    if (_parts != NULL)
    {
        disconnect(_parts,SIGNAL(destroyed()),this,SLOT(ModelScreenDestroyedEvent()));
        disconnect(_parts,SIGNAL(partSelected(int)),this,SLOT(PartSelectedEvent(int)));
    }

    _parts = widget;

    if (_parts != NULL)
    {
        connect(_parts,SIGNAL(destroyed()),this,SLOT(ModelScreenDestroyedEvent()));
        connect(_parts,SIGNAL(partSelected(int)),this,SLOT(PartSelectedEvent(int)));
    }
}

bool SettingsWidget::isReady()
{
    return (_model != NULL) && (_designs != NULL) &&
            (_details != NULL) && (_parts != NULL);
}

void SettingsWidget::update()
{
    int n = GetObjectsNum();
    if ((_index < 0) || (_index >= n))
        ChangeObjectType(None);

    PrintData();
}

void SettingsWidget::DesignPaletteDestroyedEvent()
{
    _designs = NULL;
}

void SettingsWidget::DetailPaletteDestroyedEvent()
{
    _details = NULL;
}

void SettingsWidget::ModelScreenDestroyedEvent()
{
    _parts = NULL;
}

void SettingsWidget::ModelDestroyedEvent()
{
    _model = NULL;
}

void SettingsWidget::PartSelectedEvent(int index)
{    
    ChangeObjectType(Part);
    _index = index;
    update();
}

void SettingsWidget::DetailSelectedEvent(int index)
{
    ChangeObjectType(Detail);
    _index = index;
    update();
}

void SettingsWidget::DesignSelectedEvent(int index)
{
    ChangeObjectType(Design);
    _index = index;
    update();
}

void SettingsWidget::ObjectDeletedEvent(int index)
{
    if (_index == index)
    {
        DisconnectObject();
        _type = None;
        update();
    }
}

int SettingsWidget::GetObjectsNum() const
{
    switch (_type)
    {
    case None:
        return 0;
    case Detail:
        return _model->getDetailsNum();
    case Design:
        return _model->getDesignsNum();
    case Part:
        return _model->getPartsNum();
    default:
        return -1;
    }
}

void SettingsWidget::PrintData()
{
    switch (_type)
    {
    case Design:
        ui->table->setData(_model->getDesign(_index));
        break;
    case Detail:
        ui->table->setData(_model->getDetail(_index));
        break;
    case Part:
        ui->table->setData(_model->getPart(_index));
        break;
    case None:
    default:
        ui->table->setNull();
        break;
    }
}

void SettingsWidget::ChangeObjectType(SettingsWidget::ObjType type)
{
    DisconnectObject();
    _type = type;
    ConnectObject();
}

void SettingsWidget::ConnectObject()
{
    switch (_type)
    {
    case Design:
        connect(_model,SIGNAL(designDeleted(int)),this,SLOT(ObjectDeletedEvent(int)));
        connect(_model,SIGNAL(designsChanged()),this,SLOT(update()));
        break;
    case Detail:
        connect(_model,SIGNAL(detailDeleted(int)),this,SLOT(ObjectDeletedEvent(int)));
        connect(_model,SIGNAL(detailsChanged()),this,SLOT(update()));
        break;
    case Part:
        connect(_model,SIGNAL(partDeleted(int)),this,SLOT(ObjectDeletedEvent(int)));
        connect(_model,SIGNAL(partsChanged()),this,SLOT(update()));
        break;
    case None:
    default:
        break;
    }
}

void SettingsWidget::DisconnectObject()
{
    switch (_type)
    {
    case Design:
        disconnect(_model,SIGNAL(designDeleted(int)),this,SLOT(ObjectDeletedEvent(int)));
        disconnect(_model,SIGNAL(designsChanged()),this,SLOT(update()));
        break;
    case Detail:
        disconnect(_model,SIGNAL(detailDeleted(int)),this,SLOT(ObjectDeletedEvent(int)));
        disconnect(_model,SIGNAL(detailsChanged()),this,SLOT(update()));
        break;
    case Part:
        disconnect(_model,SIGNAL(partDeleted(int)),this,SLOT(ObjectDeletedEvent(int)));
        disconnect(_model,SIGNAL(partsChanged()),this,SLOT(update()));
        break;
    case None:
    default:
        break;
    }
}
