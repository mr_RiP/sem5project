#ifndef DESIGNPALETTECONTROLLER_H
#define DESIGNPALETTECONTROLLER_H

#include "Abstract/abstractpalettecontroller.h"
#include "newdesigndialog.h"
#include <QObject>

class DesignPaletteController : public AbstractPaletteController
{
    Q_OBJECT

public:
    DesignPaletteController();
    DesignPaletteController(AbstractModel* model);
    ~DesignPaletteController();

    void setModel(AbstractModel* model);
    bool isReady() const;

    void showCreationDialog();

    bool isIndexCorrect(int index) const;

    const AbstractDesign* getItem(int index) const;
    int getItemsNum() const;
    bool isNameExists(const QString& name) const;
    bool isItemCanBeDeleted(int index) const;
    void deleteItem(int index);
    void setItemName(int index, const QString& name) const;
    bool isItemCanBeCreated() const;

    const AbstractModel* getModel() const;

    void errMsgIncorrectName(QWidget* parent) const;
    void errMsgCantDelete(QWidget* parent) const;
    void errMsgCantCreate(QWidget* parent) const;

private slots:
    void DesignCheck();
    void ModelDestroyedEvent();
    void UpdateEvent();

protected:
    void AddDesign();

private:
    AbstractModel* _model;
    NewDesignDialog _dlg;
};

#endif // DESIGNPALETTECONTROLLER_H
