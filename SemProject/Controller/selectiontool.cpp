#include "selectiontool.h"
#include "ui_selectiontool.h"
#include "../View/Service/pointscreator.h"
#include <QPainter>
#include <QMessageBox>

#define NO_COMPOSITE_DESIGNS

SelectionTool::SelectionTool(QWidget *parent) :
    ui(new Ui::SelectionTool)
{
    _model = NULL;
    _view = NULL;
    ui->setupUi(this);
    SetDefaultColor();
    SetColorDialog();
}

SelectionTool::~SelectionTool()
{
    delete ui;
}

void SelectionTool::redraw()
{
    if (isReady())
    {
        ResetImage();
        DrawSelections();
        emit imageChanged();
    }
}

void SelectionTool::ViewDestroyedEvent()
{
    _view = NULL;
}

void SelectionTool::ColorChangeEvent()
{
    QList<int> details;
    int n = _selected.size();
    for (int i=0; i<n; i++)
    {
        int index = _model->getDetailIndex(_model->getPart(_selected[i])->getDetail());
        if (!details.contains(index))
            details.append(index);
    }

    n = details.size();
    for (int i=0; i<n; i++)
        _model->setDetailColor(details[i],_color_dlg.selectedColor());

    _view->resetColoring();
}

void SelectionTool::SetDefaultColor()
{
    _color = Qt::blue;
}

void SelectionTool::ResetImage()
{
    if (isReady())
        _img = _view->getImage();
}

void SelectionTool::DrawSelections()
{
    int n = _selected.size();
    for (int i=0; i<n; i++)
        DrawBlockBox(_selected[i]);
}

void SelectionTool::DrawBlockBox(int index)
{
    QList<QPoint> points;
    SetBoxPoints(points, _model->getPart(index));
    DrawBoxLines(points);
}

void SelectionTool::SetBoxPoints(QList<QPoint> &points, const AbstractPart *part)
{
    points.clear();

    QList<QVector3D*> tmp;
    PointsCreator::createBoxPoints(tmp,part);
    const AbstractCamera* cam = _view->getCamera();

    int n = tmp.size();
    for (int i=0; i<n; i++)
    {
        points.append(cam->toView(*(tmp[i])).toPoint());
        delete tmp[i];
    }
}

void SelectionTool::DrawBoxLines(QList<QPoint> &points)
{
    QPainter p(&_img);
    p.setPen(_color);

    for (int i=0; i<3; i++)
    {
        p.drawLine(points[i],points[i+1]);
        p.drawLine(points[i+4],points[i+5]);
        p.drawLine(points[i],points[i+4]);
    }

    p.drawLine(points[0],points[3]);
    p.drawLine(points[4],points[7]);
    p.drawLine(points[3],points[7]);
}

void SelectionTool::ClearSelection()
{
    _selected.clear();
    redraw();
}

void SelectionTool::DeleteSelectedParts()
{
    if (_selected.size() > 0)
    {
        if (_selected.size() == 1)
        {
            if (_model->isPartCanBeDeleted(_selected.first()))
            {
                int index = _selected.first();
                _selected.clear();
                _model->deletePart(index);
            }
            else
                ErrMsgCantDelete();
        }
        else
        {
            if (_model->isPartsCanBeDeleted(_selected))
            {
                QList<int> tmp = _selected;
                _selected.clear();
                _model->deleteParts(tmp);
            }
            else
                ErrMsgCantDelete();
        }
    }
}

void SelectionTool::ChangeDetailColor()
{
    if (isReady() && !_selected.isEmpty())
        _color_dlg.show();
}

void SelectionTool::ErrMsgCantDelete()
{
    QString title("Действие невозможно");
    QString text("Невозможно удалить из модели часть, которая является опорой для другой части модели.");
    QMessageBox::critical(this,title,text,QMessageBox::Ok);
}

void SelectionTool::SetColorDialog()
{
    _color_dlg.setModal(true);
    connect(&_color_dlg,SIGNAL(accepted()),this,SLOT(ColorChangeEvent()));
}

void SelectionTool::setData(AbstractModel *model, AbstractView *view)
{
    if (_view != NULL)
        disconnect(_view,&AbstractView::imageChanged,this,&SelectionTool::redraw);

    _model = model;
    _view = view;

    if (_view != NULL)
    {
        connect(_view,&AbstractView::imageChanged,this,&SelectionTool::redraw);
        redraw();
    }
}

bool SelectionTool::isReady() const
{
    return (_model != NULL) && (_view != NULL);
}

void SelectionTool::clear()
{
    if (_view != NULL)
        disconnect(_view,&AbstractView::imageChanged,this,&SelectionTool::redraw);

    _model = NULL;
    _view = NULL;
    _selected.clear();
    _img = QImage();
}

const QImage &SelectionTool::getImage() const
{
    return _img;
}

void SelectionTool::mousePressAction(QPoint pos)
{
    if (isReady())
    {
        int index = _view->getPartIndex(pos);
        if (index < 0)
        {
            _selected.clear();
        }
        else
        {
            if (_selected.contains(index))
                _selected.removeOne(index);
            else
                _selected.append(index);
        }
        redraw();
    }
}

void SelectionTool::on_clearButton_clicked()
{
    ClearSelection();
}

void SelectionTool::on_deleteButton_clicked()
{
    DeleteSelectedParts();
}

void SelectionTool::on_pushButton_clicked()
{
    ChangeDetailColor();
}
