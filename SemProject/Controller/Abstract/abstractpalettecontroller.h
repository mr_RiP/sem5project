#ifndef ABSTRACTPALETTECONTROLLER_H
#define ABSTRACTPALETTECONTROLLER_H

#include "../../Model/Abstract/abstractmodel.h"
#include <QWidget>
#include <QObject>

class AbstractPaletteController : public QObject
{
    Q_OBJECT

public:
    virtual ~AbstractPaletteController() {}

    virtual void setModel(AbstractModel* model) = 0;
    virtual bool isReady() const = 0;

    virtual void showCreationDialog() = 0;

    virtual bool isIndexCorrect(int index) const = 0;

    virtual const AbstractDesign* getItem(int index) const = 0;
    virtual int getItemsNum() const = 0;
    virtual bool isNameExists(const QString& name) const = 0;
    virtual bool isItemCanBeDeleted(int index) const = 0;
    virtual void deleteItem(int index) = 0;
    virtual void setItemName(int index, const QString& name) const = 0;
    virtual bool isItemCanBeCreated() const = 0;

    virtual const AbstractModel* getModel() const = 0;

    virtual void errMsgIncorrectName(QWidget* parent) const = 0;
    virtual void errMsgCantDelete(QWidget* parent) const = 0;
    virtual void errMsgCantCreate(QWidget* parent) const = 0;

signals:
    void updateNeeded();

};

#endif // ABSTRACTPALETTECONTROLLER_H

