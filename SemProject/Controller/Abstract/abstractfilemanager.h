#ifndef ABSTRACTFILEMANAGER_H
#define ABSTRACTFILEMANAGER_H

#include <QString>
#include "../../Model/Abstract/abstractmodel.h"

class AbstractFileManager
{
public:
    virtual ~AbstractFileManager() {}

    virtual void setModel(AbstractModel *model) = 0;
    virtual void setFile(const QString& filename) = 0;

    virtual bool saveModel() = 0;
    virtual bool savePaletteOnly() = 0;
    virtual bool loadModel() = 0;
    virtual bool loadPaletteOnly() = 0;

    virtual bool isReady() const = 0;
    virtual void setNull() = 0;
};

#endif // ABSTRACTFILEMANAGER_H

