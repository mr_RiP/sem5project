#ifndef ABSTRACTTOOL_H
#define ABSTRACTTOOL_H

#include <QWidget>
#include <QImage>
#include <QPoint>

#include "../../Model/Abstract/abstractmodel.h"
#include "../../View/Abstract/abstractview.h"

class AbstractTool : public QWidget
{
    Q_OBJECT
public:
    virtual ~AbstractTool() {}

    virtual void setData(AbstractModel *model, AbstractView *view) = 0;
    virtual bool isReady() const = 0;
    virtual void clear() = 0;

    virtual const QImage& getImage() const = 0;
    virtual void mousePressAction(QPoint pos) = 0;

public slots:
    virtual void redraw() = 0;

signals:
    void imageChanged();
};

#endif // ABSTRACTTOOL_H
