#include "modelscreen.h"
#include <QPainter>
#include "../View/view.h"
#include "Service/cameraactions.h"

#ifdef DEBUG
#include <cstdio>
#endif

ModelScreen::ModelScreen(QWidget *parent) : QWidget(parent)
{
    _model = NULL;
    _view = NULL;
    _tool = NULL;
}

ModelScreen::~ModelScreen()
{   
    if (_view != NULL)
        delete _view;
}

void ModelScreen::setModel(AbstractModel *model)
{
    if (_model != NULL)
    {
        disconnect(_model,SIGNAL(destroyed()),this,SLOT(ModelDestroyedEvent()));
        disconnect(_model,SIGNAL(partsChanged()),_view,SLOT(resetStructure()));
    }

    ModelDestroyedEvent();
    _model = model;

    if (_model != NULL)
    {
        connect(_model,SIGNAL(destroyed()),this,SLOT(ModelDestroyedEvent()));
        _view = new View(_model,size());
        _view->resetAll();
        connect(_model,SIGNAL(partsChanged()),_view,SLOT(resetStructure()));
    }

    if (_tool != NULL)
        _tool->setData(_model,_view);
}

const AbstractModel *ModelScreen::getModel() const
{
    return _model;
}

const AbstractView *ModelScreen::getView() const
{
    return _view;
}

void ModelScreen::setTool(AbstractTool *tool)
{
    if (_tool != NULL)
    {
        _tool->clear();
        disconnect(_tool,SIGNAL(imageChanged()),this,SLOT(repaint()));
    }

    _tool = tool;

    if (_tool != NULL)
    {
        _tool->setData(_model,_view);
        connect(_tool,SIGNAL(imageChanged()),this,SLOT(repaint()));
    }
    repaint();
}

void ModelScreen::ModelDestroyedEvent()
{
    _model = NULL;

    if (_view != NULL)
    {
        delete _view;
        _view = NULL;
    }
}

void ModelScreen::ToolDestroyedEvent()
{
    _tool = NULL;
}

bool ModelScreen::isReady() const
{
    return (_model != NULL) && (_view != NULL) &&
            (_tool != NULL) && (_tool->isReady());
}

bool ModelScreen::isToolReady() const
{
    return (_tool != NULL) && (_tool->isReady());
}


void ModelScreen::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {
        _prev = event->pos();
        _camera = true;
    }

    if (event->button() == Qt::LeftButton)
    {
        emit partSelected(_view->getPartIndex(event->pos()));
        if(isToolReady())
            _tool->mousePressAction(event->pos());
    }
}

void ModelScreen::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {
        _camera = false;
    }
}

void ModelScreen::mouseMoveEvent(QMouseEvent *event)
{
    if (_camera)
    {
        QPoint pos = event->pos();
        CameraActions::move(_view,pos,_prev);
        _prev = event->pos();
    }
}

void ModelScreen::wheelEvent(QWheelEvent *event)
{
    CameraActions::zoom(_view,event);
}

void ModelScreen::reset()
{
    if (isReady())
        _view->resetAll();
}

void ModelScreen::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if (isReady())
        painter.drawImage(0,0,_tool->getImage());
    else if (_view != NULL)
        painter.drawImage(0,0,_view->getImage());
    else
        painter.fillRect(0,0,this->width(),this->height(),Qt::white);
}

void ModelScreen::resizeEvent(QResizeEvent *event)
{
    _view->resize(event->size());

#ifdef DEBUG
    printf("resize %ix%i -> %ix%i\n",
           event->oldSize().width(),event->oldSize().height(),
           event->size().width(), event->size().height());
#endif
}
