#include "detailplacementtool.h"
#include "ui_detailplacementtool.h"
#include <QMessageBox>
#include <QPainter>

DetailPlacementTool::DetailPlacementTool(QWidget *parent) :
    ui(new Ui::DetailPlacementTool)
{
    ui->setupUi(this);
    _model = NULL;
    _view = NULL;
    _img = QImage();
    _selected = false;
    _valid = false;
    _palette = NULL;

    SetPosConnections();
}

DetailPlacementTool::~DetailPlacementTool()
{
    delete ui;
}

void DetailPlacementTool::setData(AbstractModel *model, AbstractView *view)
{
    if (_view != NULL)
        disconnect(_view,&AbstractView::imageChanged,this,&DetailPlacementTool::redraw);

    _model = model;
    _view = view;

    if (_view != NULL)
    {
        connect(_view,&AbstractView::imageChanged,this,&DetailPlacementTool::redraw);
        redraw();
    }
}

bool DetailPlacementTool::isReady() const
{
    return (_model != NULL) && (_view != NULL) && (_palette != NULL);
}

void DetailPlacementTool::clear()
{
    if (_view != NULL)
        disconnect(_view,&AbstractView::imageChanged,this,&DetailPlacementTool::redraw);

    _model = NULL;
    _view = NULL;
    _img = QImage();
    _selected = false;
    _valid = false;
}

const QImage &DetailPlacementTool::getImage() const
{
    return _img;
}

void DetailPlacementTool::mousePressAction(QPoint pos)
{
    if (isReady())
    {
        bool on_part = _view->getPartIndex(pos) >= 0;
        bool on_anchor = _view->isPointOnAnchor(pos);
        if (!on_part || on_anchor)
        {
            QVector3D point = (on_anchor)? GetAnchorPoint(pos): GetZeroFloorPoint(pos);
            if (_selected && (point == _point))
            {
                if (_valid)
                    PlaceSelectedDetail();
            }
            else
            {
                _selected = true;
                _point = point;
                SetSelectionValidation();
            }
        }
        else
        {
            _selected = false;
        }
        redraw();
    }
}

void DetailPlacementTool::setPalette(const ModelPaletteWidget *palette)
{
    if (_palette != NULL)
    {
        disconnect(_palette,SIGNAL(destroyed()),this,SLOT(PaletteDestroyedEvent()));
        disconnect(_palette,SIGNAL(indexChanged(int)),this,SLOT(SettingsChangedEvent(int)));
    }

    _palette = palette;

    if (_palette != NULL)
    {
        connect(_palette,SIGNAL(destroyed()),this,SLOT(PaletteDestroyedEvent()));
        connect(_palette,SIGNAL(indexChanged(int)),this,SLOT(SettingsChangedEvent(int)));
    }
}

void DetailPlacementTool::redraw()
{
    if (isReady())
    {
        _img = _view->getImage();
        DrawSelection();
        emit imageChanged();
    }
}

void DetailPlacementTool::PaletteDestroyedEvent()
{
    _palette = NULL;
}

void DetailPlacementTool::SettingsChangedEvent(int index)
{
    if (isReady() && _selected)
    {
        if ((index >= 0) && (index < _model->getDetailsNum()))
            SetSelectionValidation();
        else
            _selected = false;

        redraw();
    }
}

void DetailPlacementTool::PosChangedEvent(int val)
{
    if (isReady() && _selected)
    {
        SetSelectionValidation();
        redraw();
    }
}

QVector3D DetailPlacementTool::GetZeroFloorPoint(QPoint point)
{
    QVector3D pt = _view->getCamera()->toModel(QVector3D(point));
    QVector3D z = _view->getCamera()->getVectorZ() / fabs(_view->getCamera()->getVectorZ().z());
    pt = (z * pt.z()) + pt;

    pt.setX(truncf((pt.x() < 0)? pt.x()-1.f: pt.x()));
    pt.setY(truncf((pt.y() < 0)? pt.y()-1.f: pt.y()));
    pt.setZ(0.f);

    return pt;
}

QVector3D DetailPlacementTool::GetAnchorPoint(QPoint point)
{
    QVector3D pt(point);
    pt.setZ(_view->getZ(point));
    _view->getCamera()->switchToModel(pt);

    pt.setX(truncf((pt.x() < 0)? pt.x()-1.f: pt.x()));
    pt.setY(truncf((pt.y() < 0)? pt.y()-1.f: pt.y()));
    pt.setZ(roundf(pt.z()));

    return pt;
}

void DetailPlacementTool::DrawSelection()
{
    if (_selected)
    {
        QList<QPoint> list = GetVectorsList();
        QColor color = GetSelectionColor();
        DrawLines(list, color);
    }
}

QList<QPoint> DetailPlacementTool::GetVectorsList() const
{
    QList<QVector3D> vectors;
    for (int i=0; i<4; i++)
        vectors.append(_point);

    vectors[1].setX(_point.x() + 1.f);
    vectors[2].setX(_point.x() + 1.f);
    vectors[2].setY(_point.y() + 1.f);
    vectors[3].setY(_point.y() + 1.f);

    for (int i=0; i<4; i++)
        _view->getCamera()->switchToView(vectors[i]);

    QList<QPoint> points;
    for (int i=0; i<4; i++)
        points.append(vectors[i].toPoint());

    return points;
}

QColor DetailPlacementTool::GetSelectionColor() const
{
    if (_valid)
        return Qt::green;
    else
        return Qt::red;
}

void DetailPlacementTool::DrawLines(const QList<QPoint> &list, const QColor &color)
{
    QPainter p(&_img);
    QPen x_pen(Qt::green);
    x_pen.setWidth(3);
    QPen y_pen(Qt::red);
    y_pen.setWidth(3);
    QPen line_pen(Qt::blue);
    line_pen.setWidth(1);

    p.setPen(x_pen);
    p.drawLine(list[0],list[1]);

    p.setPen(y_pen);
    p.drawLine(list[0],list[3]);

    p.setPen(line_pen);
    p.drawLine(list[1],list[2]);
    p.drawLine(list[2],list[3]);

    p.setPen(color);
    p.drawLine(list[0],list[2]);
    p.drawLine(list[1],list[3]);
}

void DetailPlacementTool::SetSelectionValidation()
{
    int detail = _palette->getIndex();
    int x = _point.x() + ui->xBox->value();
    int y = _point.y() + ui->yBox->value();
    int z = _point.z() + ui->zBox->value();
    int rot = ui->comboBox->currentIndex() * 90;

    _valid = _model->isDetailCanBePlaced(detail,x,y,z,rot);
}

void DetailPlacementTool::PlaceSelectedDetail()
{
    _selected = false;

    int detail = _palette->getIndex();
    int x = _point.x() + ui->xBox->value();
    int y = _point.y() + ui->yBox->value();
    int z = _point.z() + ui->zBox->value();
    int rot = ui->comboBox->currentIndex() * 90;

    _model->addPart(detail,x,y,z,rot);
    //_view->resetStructure();
}

void DetailPlacementTool::ClearSelectionData()
{
    if (isReady())
    {
        ui->xBox->setValue(0);
        ui->yBox->setValue(0);
        ui->zBox->setValue(0);
        ui->comboBox->setCurrentIndex(0);
        redraw();
    }
}

void DetailPlacementTool::ErrMsgInvalidAnchor()
{
    QMessageBox::critical(this,"Ошибка",
                          "Установка выбранной детали в данной точке невозможна",
                          QMessageBox::Ok);
}

void DetailPlacementTool::ErrMsgNoAnchor()
{
    QMessageBox::critical(this,"Ошибка",
                          "Не задана точка установки детали",
                          QMessageBox::Ok);
}

void DetailPlacementTool::on_resetButton_clicked()
{
    ClearSelectionData();
}

void DetailPlacementTool::on_unselectButton_clicked()
{
    if (_selected)
    {
        _selected = false;
        redraw();
    }
    else
        ErrMsgNoAnchor();
}

void DetailPlacementTool::on_placeButton_clicked()
{
    if (_selected)
    {
        if (_valid)
            PlaceSelectedDetail();
        else
            ErrMsgInvalidAnchor();
    }
    else
        ErrMsgNoAnchor();
}

void DetailPlacementTool::SetPosConnections()
{
    connect(ui->xBox, SIGNAL(valueChanged(int)), this, SLOT(PosChangedEvent(int)));
    connect(ui->yBox, SIGNAL(valueChanged(int)), this, SLOT(PosChangedEvent(int)));
    connect(ui->zBox, SIGNAL(valueChanged(int)), this, SLOT(PosChangedEvent(int)));
}

void DetailPlacementTool::on_comboBox_currentIndexChanged(int index)
{
    PosChangedEvent(index * 90);
}
