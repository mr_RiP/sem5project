#ifndef MODELPALETTEWIDGET_H
#define MODELPALETTEWIDGET_H

#include <QWidget>
#include "../Model/Abstract/abstractmodel.h"
#include "Abstract/abstractpalettecontroller.h"
#include "modelpalette.h"
#include <QListWidgetItem>

namespace Ui {
class ModelPaletteWidget;
}

class ModelPaletteWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ModelPaletteWidget(QWidget *parent = 0);
    ~ModelPaletteWidget();
    void setController(AbstractPaletteController* controller);
    bool isReady() const;
    const AbstractPaletteController* getController() const;
    const AbstractModel* getModel() const;
    QString getItemText(int i) const;
    int getItemsNum() const;
    int getIndex() const;

public slots:
    void update();

signals:
    void deleteFailure(int index);
    void deleteSuccess(int index);
    void renameError(int index);
    void indexChanged(int current_index);
    void paletteUpdated();
    void itemClicked(int index);

private slots:
    void on_createButton_clicked();
    void on_deleteButton_clicked();

    void NameChangeFailed(int index);
    void ChangeName(int index);
    void ControllerDestroyedEvent();

    void on_paletteList_currentRowChanged(int currentRow);
    void on_paletteList_itemClicked(QListWidgetItem *item);
private:
    Ui::ModelPaletteWidget *ui;
    AbstractPaletteController* _controller;
};

#endif // MODELPALETTEWIDGET_H
