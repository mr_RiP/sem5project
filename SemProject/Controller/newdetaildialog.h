#ifndef NEWDETAILDIALOG_H
#define NEWDETAILDIALOG_H

#include <QDialog>
#include <QColorDialog>

namespace Ui {
class NewDetailDialog;
}

class NewDetailDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewDetailDialog(QWidget *parent = 0);
    ~NewDetailDialog();

    QString getName() const;
    int getDesignIndex() const;
    const QColor& getColor() const;

    void clearList();
    void insertNameToList(int index, const QString& name);
    void setDesignIndex(int index);

private slots:
    void SetColorFromDialog();

    void on_changeColorButton_clicked();


private:
    Ui::NewDetailDialog *ui;
    QColorDialog* _dlg;

protected:
    void setColorDialogRus();
};

#endif // NEWDETAILDIALOG_H
