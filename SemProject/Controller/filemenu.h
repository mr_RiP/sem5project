#ifndef FILEMENU_H
#define FILEMENU_H

#include "../Model/Abstract/abstractmodel.h"
#include <QMenu>

class FileMenu : public QMenu
{
public:
    explicit FileMenu(QWidget* parent = 0);
    ~FileMenu();

    void setModel(AbstractModel* model);
    bool isReady() const;
    const QString& getFileName() const;

public slots:
    void reset();

private slots:
    void ModelDestroyedEvent();

    void SaveModel();
    void SaveModelAs();
    void LoadModel();

private:
    AbstractModel* _model;
    QString _filename;

    void SaveModelData();
    void SetFileName(bool load);
    void LoadModelData();

    void ErrMsgCantOpenFile();
    void ErrMsgBadData(const QString& wat);
    void WarningMsgModelWillBeCleared();
};

#endif // FILEMENU_H
