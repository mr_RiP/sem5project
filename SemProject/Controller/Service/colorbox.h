#ifndef COLORBOX_H
#define COLORBOX_H

#include <QWidget>
#include <QColor>

class ColorBox : public QWidget
{
    Q_OBJECT
public:
    explicit ColorBox(QWidget *parent = 0);
    ~ColorBox();
    void setColor(const QColor& color);
    const QColor& getColor() const;

signals:

public slots:

protected:
    void paintEvent(QPaintEvent* event = 0);

private:
    QColor _color;
};

#define COLORBOX_DEFAULT_COLOR Qt::red

#endif // COLORBOX_H
