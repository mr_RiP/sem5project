#ifndef MODELXMLREADER_H
#define MODELXMLREADER_H

#include <QXmlStreamReader>
#include <QFile>
#include "../../Model/Abstract/abstractmodel.h"
#include <QColor>

class ModelXMLReader
{
public:
    static void parse(QFile* file, AbstractModel* model);

protected:
    ModelXMLReader();

    static bool StdCheck(const QXmlStreamReader& xml);
    static bool EndElementCheck(const QXmlStreamReader& xml, const QString& name);
    static bool StartElementCheck(const QXmlStreamReader& xml, const QString& name);
    static void EarlyEndError(QXmlStreamReader& xml);

    static void ParseDesigns(QXmlStreamReader& xml, AbstractModel* model);
    static void ParseDetails(QXmlStreamReader& xml, AbstractModel* model);
    static void ParseParts(QXmlStreamReader& xml, AbstractModel* model);

    static void ParseDesign(QXmlStreamReader &xml, AbstractModel *model);
    static void ParseDetail(QXmlStreamReader &xml, AbstractModel *model);
    static void ParsePart(QXmlStreamReader &xml, AbstractModel *model);

    static int GetIntData(QXmlStreamReader &xml);
    static QString GetStringData(QXmlStreamReader &xml);
    static void AddDesign(QXmlStreamReader &xml, AbstractModel *model,
                          const QString& type, const QString& name,
                          int length, int width, int height);

    static void ParseColor(QXmlStreamReader &xml, QColor& color);
    static void AddDetail(QXmlStreamReader &xml, AbstractModel *model,
                          int index, const QString& name, const QColor& color);

    static void ParsePosition(QXmlStreamReader &xml, int& x, int& y, int& z, int& rot);
    static void AddPart(QXmlStreamReader &xml, AbstractModel *model,
                        int index, int& x, int& y, int& z, int& rot);
};

#endif // MODELXMLREADER_H
