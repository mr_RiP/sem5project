#include "modelxmlreader.h"
#include "stdexcept"

void ModelXMLReader::parse(QFile *file, AbstractModel *model)
{
    model->clearAll();
    QXmlStreamReader xml(file);
    while(StdCheck(xml))
    {
        QXmlStreamReader::TokenType token = xml.readNext();
        if (token == QXmlStreamReader::StartDocument)
            continue;

        if (token == QXmlStreamReader::StartElement)
        {
            if (xml.name() == "constructor_model")
                continue;
            else if (xml.name() == "designs")
                ParseDesigns(xml, model);
            else if (xml.name() == "details")
                ParseDetails(xml,model);
            else if (xml.name() == "parts")
                ParseParts(xml,model);
        }
    }

    if (xml.error() != QXmlStreamReader::NoError)
    {
        model->clearAll();
        throw std::invalid_argument(xml.errorString().toStdString());
    }
}

ModelXMLReader::ModelXMLReader()
{

}

bool ModelXMLReader::StdCheck(const QXmlStreamReader &xml)
{
    return !xml.atEnd() && !xml.hasError();
}

bool ModelXMLReader::EndElementCheck(const QXmlStreamReader &xml, const QString &name)
{
    return (xml.tokenType() != QXmlStreamReader::EndElement) || (xml.name().toString() != name);
}

bool ModelXMLReader::StartElementCheck(const QXmlStreamReader &xml, const QString &name)
{
    return (xml.tokenType() == QXmlStreamReader::StartElement) && (xml.name().toString() == name);
}

void ModelXMLReader::EarlyEndError(QXmlStreamReader &xml)
{  
    if (xml.atEnd())
        xml.raiseError("Data is incomplete!");
}

void ModelXMLReader::ParseDesigns(QXmlStreamReader &xml, AbstractModel *model)
{
    while (StdCheck(xml) && EndElementCheck(xml,"designs"))
    {
        xml.readNext();
        if (StartElementCheck(xml,"design"))
            ParseDesign(xml,model);
    }
    EarlyEndError(xml);
}

void ModelXMLReader::ParseDetails(QXmlStreamReader &xml, AbstractModel *model)
{
    while (StdCheck(xml) && EndElementCheck(xml,"details"))
    {
        xml.readNext();
        if (StartElementCheck(xml,"detail"))
            ParseDetail(xml,model);
    }
    EarlyEndError(xml);
}

void ModelXMLReader::ParseParts(QXmlStreamReader &xml, AbstractModel *model)
{
    while (StdCheck(xml) && EndElementCheck(xml,"parts"))
    {
        xml.readNext();
        if (StartElementCheck(xml,"part"))
            ParsePart(xml,model);
    }
    EarlyEndError(xml);
}

void ModelXMLReader::ParseDesign(QXmlStreamReader &xml, AbstractModel *model)
{
    QXmlStreamAttributes attributes = xml.attributes();
    if (attributes.hasAttribute("type"))
    {
        QString type = attributes.value("type").toString();
        int length = -1;
        int width = -1;
        int height = -1;
        QString name;
        while (EndElementCheck(xml,"design") && StdCheck(xml))
        {
            xml.readNext();
            if (xml.name() == "name")
                name = GetStringData(xml);
            if (xml.name() == "length")
                length = GetIntData(xml);
            if (xml.name() == "width")
                width = GetIntData(xml);
            if (xml.name() == "height")
                height = GetIntData(xml);
        }
        AddDesign(xml,model,type,name,length,width,height);
    }
    else
        xml.raiseError("Design data is incomplete (no type)");
}

void ModelXMLReader::ParseDetail(QXmlStreamReader &xml, AbstractModel *model)
{
    QString name;
    int index = -1;
    QColor color;

    while (StdCheck(xml) && EndElementCheck(xml,"detail"))
    {
        xml.readNext();
        if (xml.name() == "index")
            index = GetIntData(xml);
        if (xml.name() == "name")
            name = GetStringData(xml);
        if (StartElementCheck(xml,"color"))
            ParseColor(xml,color);
    }
    AddDetail(xml,model,index,name,color);
}

void ModelXMLReader::ParsePart(QXmlStreamReader &xml, AbstractModel *model)
{
    int index = -1;
    int x = 0;
    int y = 0;
    int z = 0;
    int rot = 0;

    while (StdCheck(xml) && EndElementCheck(xml,"part"))
    {
        xml.readNext();
        if (xml.name() == "index")
            index = GetIntData(xml);
        if (StartElementCheck(xml,"position"))
            ParsePosition(xml,x,y,z,rot);
    }

    AddPart(xml,model,index,x,y,z,rot);
}

int ModelXMLReader::GetIntData(QXmlStreamReader &xml)
{
    xml.readNext();
    int data = xml.text().toInt();
    xml.readNext();
    return data;
}

QString ModelXMLReader::GetStringData(QXmlStreamReader &xml)
{
    xml.readNext();
    QString data = xml.text().toString();
    xml.readNext();
    return data;
}

void ModelXMLReader::AddDesign(QXmlStreamReader &xml, AbstractModel *model,
                               const QString &type, const QString &name,
                               int length, int width, int height)
{
    try
    {
        if (type == "simple")
            model->addDesignSimple(length,width,height,name,true);
        else if (type == "cutted_upper")
            model->addDesignCuttedUpper(length,width,height,name,true);
        else if (type == "cutted_lower")
            model->addDesignCuttedLower(length,width,height,name,true);
        else
            throw std::invalid_argument("Invalid design type!");
    }
    catch (std::exception& err)
    {
        xml.raiseError(QString("Design data is corrupted (%1)").arg(QString(err.what())));
    }
}

void ModelXMLReader::ParseColor(QXmlStreamReader &xml, QColor &color)
{
    xml.readNext();
    while (StdCheck(xml) && EndElementCheck(xml,"color"))
    {
        if (xml.name() == "red")
            color.setRed(GetIntData(xml));
        if (xml.name() == "green")
            color.setGreen(GetIntData(xml));
        if (xml.name() == "blue")
            color.setBlue(GetIntData(xml));
        xml.readNext();
    }
}

void ModelXMLReader::AddDetail(QXmlStreamReader &xml, AbstractModel *model,
                               int index, const QString &name, const QColor &color)
{
    try
    {
        model->addDetail(index,color,name,true);
    }
    catch (std::exception &err)
    {
        xml.raiseError(QString("Detail data is corrupted (%1)").arg(QString(err.what())));
    }
}

void ModelXMLReader::ParsePosition(QXmlStreamReader &xml, int &x, int &y, int &z, int &rot)
{
    while (StdCheck(xml) && EndElementCheck(xml,"position"))
    {
        xml.readNext();
        if (xml.name() == "x")
            x = GetIntData(xml);
        if (xml.name() == "y")
            y = GetIntData(xml);
        if (xml.name() == "z")
            z = GetIntData(xml);
        if (xml.name() == "rotation")
            rot = GetIntData(xml);
    }
}

void ModelXMLReader::AddPart(QXmlStreamReader &xml, AbstractModel *model,
                             int index, int &x, int &y, int &z, int &rot)
{
    try
    {
        model->addPart(index,x,y,z,rot,true);
    }
    catch (std::exception& err)
    {
        xml.raiseError(QString("Part data is corrupted (%1)").arg(QString(err.what())));
    }
}

