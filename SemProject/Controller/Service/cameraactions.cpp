#include "cameraactions.h"
#include <cmath>

#define CAM_PI 3.14159265358979f

CameraActions::CameraActions()
{

}

CameraActions::~CameraActions()
{

}

void CameraActions::zoom(AbstractView *view, QWheelEvent *event)
{
    if (view != NULL)
    {
        int dif = event->angleDelta().y() / 120;
        view->setScale(view->getScale() - dif);
        view->resetView();
    }
}

void CameraActions::move(AbstractView *view, const QPoint &pos, const QPoint &prev)
{
    if ((view != NULL) && (pos != prev))
    {
        QPoint dif = prev-pos;
        view->rotateX(-dif.x());
        view->rotateY(-dif.y());
        view->resetView();
    }
}

