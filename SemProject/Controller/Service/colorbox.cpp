#include "colorbox.h"
#include <QPainter>

ColorBox::ColorBox(QWidget *parent) : QWidget(parent)
{
    _color = COLORBOX_DEFAULT_COLOR;
}

ColorBox::~ColorBox()
{

}

void ColorBox::setColor(const QColor &color)
{
    _color = color;
    repaint();
}

const QColor &ColorBox::getColor() const
{
    return _color;
}

void ColorBox::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.fillRect(0,0,width(),height(),_color);
}

