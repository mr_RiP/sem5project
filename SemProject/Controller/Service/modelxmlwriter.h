#ifndef MODELXMLWRITER_H
#define MODELXMLWRITER_H

#include <QFile>
#include <QXmlStreamWriter>
#include "../../Model/Abstract/abstractmodel.h"

class ModelXMLWriter
{
public:
    static void write(QFile* file, const AbstractModel* model);

protected:
    ModelXMLWriter();

    static void WriteDesigns(QXmlStreamWriter& xml, const AbstractModel* model);
    static void WriteDetails(QXmlStreamWriter& xml, const AbstractModel* model);
    static void WriteParts(QXmlStreamWriter& xml, const AbstractModel* model);

    static QString GetDesignType(const AbstractDesign* design);
    static QString GetDesignIndex(const AbstractDetail* detail, const AbstractModel *model);
    static void WriteColor(QXmlStreamWriter& xml, const QColor& color);
    static QString GetDetailIndex(const AbstractPart* part, const AbstractModel *model);
    static void WritePosition(QXmlStreamWriter& xml, const AbstractPosition* pos);
};

#endif // MODELXMLWRITER_H
