#ifndef CAMERAACTIONS_H
#define CAMERAACTIONS_H

#include <QWheelEvent>
#include <QMouseEvent>
#include "../../View/Abstract/abstractview.h"

class CameraActions
{
public:
    ~CameraActions();

    static void zoom(AbstractView* view, QWheelEvent* event);
    static void move(AbstractView* view, const QPoint& pos, const QPoint& prev);

protected:
    CameraActions();
};

#endif // CAMERAACTIONS_H
