#include "modelxmlwriter.h"

void ModelXMLWriter::write(QFile *file, const AbstractModel *model)
{
    QXmlStreamWriter xml(file);
    xml.writeStartDocument();
    xml.writeStartElement("constructor_model");

    WriteDesigns(xml,model);
    WriteDetails(xml,model);
    WriteParts(xml,model);

    xml.writeEndElement(); // constructor_model
    xml.writeEndDocument();
}

ModelXMLWriter::ModelXMLWriter()
{

}

void ModelXMLWriter::WriteDesigns(QXmlStreamWriter &xml, const AbstractModel *model)
{
    xml.writeStartElement("designs");

    int n = model->getDesignsNum();
    for (int i=0; i<n; i++)
    {
        const AbstractDesign* design = model->getDesign(i);
        xml.writeStartElement("design");
        xml.writeAttribute("type",GetDesignType(design));

        xml.writeTextElement("name",design->getName());
        xml.writeTextElement("length",QString::number(design->getLength()));
        xml.writeTextElement("width",QString::number(design->getWidth()));
        xml.writeTextElement("height",QString::number(design->getHeight()));

        xml.writeEndElement(); // design
    }

    xml.writeEndElement(); // Designs
}

void ModelXMLWriter::WriteDetails(QXmlStreamWriter &xml, const AbstractModel *model)
{
    xml.writeStartElement("details");

    int n = model->getDetailsNum();
    for (int i=0; i<n; i++)
    {
        const AbstractDetail* detail = model->getDetail(i);
        xml.writeStartElement("detail");
        xml.writeTextElement("index", GetDesignIndex(detail,model));
        xml.writeTextElement("name",detail->getName());
        WriteColor(xml, detail->getColor());
        xml.writeEndElement(); // detail
    }

    xml.writeEndElement(); // Details
}

void ModelXMLWriter::WriteParts(QXmlStreamWriter &xml, const AbstractModel *model)
{
    xml.writeStartElement("parts");

    int n = model->getPartsNum();
    for (int i=0; i<n; i++)
    {
        const AbstractPart* part = model->getPart(i);
        xml.writeStartElement("part");
        xml.writeTextElement("index",GetDetailIndex(part, model));
        WritePosition(xml, part->getPosition());
        xml.writeEndElement(); // part
    }

    xml.writeEndElement(); // Parts
}

QString ModelXMLWriter::GetDesignType(const AbstractDesign *design)
{
    if (design->isSimple())
        return "simple";
    else if (design->isCuttedLower())
        return "cutted_lower";
    else
        return "cutted_upper";
}

QString ModelXMLWriter::GetDesignIndex(const AbstractDetail *detail, const AbstractModel* model)
{
    int index = model->getDesignIndex(detail->getDesign());
    return QString::number(index);
}

void ModelXMLWriter::WriteColor(QXmlStreamWriter &xml, const QColor &color)
{
    xml.writeStartElement("color");
    xml.writeTextElement("red",QString::number(color.red()));
    xml.writeTextElement("green",QString::number(color.green()));
    xml.writeTextElement("blue",QString::number(color.blue()));
    xml.writeEndElement(); // color
}

QString ModelXMLWriter::GetDetailIndex(const AbstractPart *part, const AbstractModel *model)
{
    int index = model->getDetailIndex(part->getDetail());
    return QString::number(index);
}

void ModelXMLWriter::WritePosition(QXmlStreamWriter &xml, const AbstractPosition *pos)
{
    xml.writeStartElement("position");
    xml.writeTextElement("x",QString::number(pos->getX()));
    xml.writeTextElement("y",QString::number(pos->getY()));
    xml.writeTextElement("z",QString::number(pos->getZ()));
    xml.writeTextElement("rotation",QString::number(pos->getRotation()));
    xml.writeEndElement(); // position
}

