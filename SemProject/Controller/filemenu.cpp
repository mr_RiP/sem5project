#include "filemenu.h"
#include <QFileDialog>
#include <QFile>
#include "Service/modelxmlwriter.h"
#include "Service/modelxmlreader.h"
#include <stdexcept>
#include <QMessageBox>

FileMenu::FileMenu(QWidget *parent) : QMenu(parent)
{
    setTitle("Файл");
    connect(addAction("Открыть"),&QAction::triggered,this,&FileMenu::LoadModel);
    connect(addAction("Сохранить"),&QAction::triggered,this,&FileMenu::SaveModel);
    connect(addAction("Сохранить как..."),&QAction::triggered,this,&FileMenu::SaveModelAs);

    _model = NULL;
}

FileMenu::~FileMenu()
{

}

void FileMenu::setModel(AbstractModel *model)
{
    if (_model != NULL)
        disconnect(_model,&QObject::destroyed,this,&FileMenu::ModelDestroyedEvent);

    _model = model;

    if (_model != NULL)
        connect(_model,&QObject::destroyed,this,&FileMenu::ModelDestroyedEvent);
}

bool FileMenu::isReady() const
{
    return _model != NULL;
}

void FileMenu::reset()
{
    _filename.clear();
}

void FileMenu::ModelDestroyedEvent()
{
    _model = NULL;
}

void FileMenu::SaveModel()
{
    if (_filename.isEmpty())
        SaveModelAs();
    else
        SaveModelData();
}

void FileMenu::SaveModelAs()
{
    SetFileName(false);
    SaveModelData();
}

void FileMenu::LoadModel()
{
    SetFileName(true);
    LoadModelData();
}

void FileMenu::SetFileName(bool load)
{
    QString file;
    if (load)
    {
        if (_model->getDesignsNum() != 0)
            WarningMsgModelWillBeCleared();
        file = QFileDialog::getOpenFileName(this, "Открыть файл модели", "",
                                            "Файл XML (*.xml)");
    }
    else
        file = QFileDialog::getSaveFileName(this, "Сохранить модель в файл", "",
                                            "Файл XML (*.xml)");

    if (!file.isEmpty())
        _filename = file;
}

void FileMenu::LoadModelData()
{
    if (!_filename.isEmpty())
    {
        QFile file(_filename);
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            try
            {
                ModelXMLReader::parse(&file,_model);
            }
            catch (std::exception& err)
            {
                ErrMsgBadData(QString(err.what()));
            }
            file.close();
        }
        else
            ErrMsgCantOpenFile();
    }
}

void FileMenu::ErrMsgCantOpenFile()
{
    QMessageBox::critical(this,"Ошибка",
                          "Не удалось открыть указанный файл",QMessageBox::Ok);
}

void FileMenu::ErrMsgBadData(const QString &wat)
{
    QMessageBox::critical(this,"Ошибка",
                          QString("Файл поврежден. Подробности: %1").arg(wat),
                          QMessageBox::Ok);
}

void FileMenu::WarningMsgModelWillBeCleared()
{
    QMessageBox::warning(this,"Предупреждение",
                         "При загрузке модели из файла "
                         "все несохраненные данные будут удалены",
                         QMessageBox::Ok);
}

void FileMenu::SaveModelData()
{
    if (!_filename.isEmpty())
    {
        QFile file(_filename);
        if (file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            ModelXMLWriter::write(&file,_model);
            file.close();
        }
        else
            ErrMsgCantOpenFile();
    }
}

