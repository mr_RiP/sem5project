#ifndef TOOLBOXWIDGET_H
#define TOOLBOXWIDGET_H

#include <QWidget>
#include "Abstract/abstracttool.h"
#include "../Model/Abstract/abstractmodel.h"
#include "modelscreen.h"
#include "modelpalettewidget.h"

namespace Ui {
class ToolBoxWidget;
}

class ToolBoxWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ToolBoxWidget(QWidget *parent = 0);
    ~ToolBoxWidget();

    void setScreen(ModelScreen* screen);
    void setPalette(const ModelPaletteWidget* palette);
    bool isReady() const;

private slots:
    void ScreenDestroyedEvent();

    void on_toolBox_currentChanged(int index);

protected:
    void SetTool(int index);

private:
    Ui::ToolBoxWidget *ui;
    ModelScreen* _screen;
};

#endif // TOOLBOXWIDGET_H
