#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "Model/model.h"
#include "Controller/designpalettecontroller.h"
#include "Controller/detailpalettecontroller.h"

#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    
    ConstructorSetup();
    //SetDebugModel();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete _model;
}

void MainWindow::ConstructorSetup()
{
    _model = new Model;
    ui->modelScreen->setModel(_model);
    ui->designsWidget->setController(new DesignPaletteController(_model));
    ui->detailsWidget->setController(new DetailPaletteController(_model));
    ui->settingsWidget->setModel(_model);
    ui->settingsWidget->setModelScreen(ui->modelScreen);
    ui->settingsWidget->setDesignPalette(ui->designsWidget);
    ui->settingsWidget->setDetailPalette(ui->detailsWidget);
    ui->toolboxWidget->setScreen(ui->modelScreen);
    ui->toolboxWidget->setPalette(ui->detailsWidget);
    ui->fileMenu->setModel(_model);
}

void MainWindow::SetDebugModel()
{
    _model->addDesignSimple(1,1,1,"Малый куб");
    _model->addDesignSimple(3,3,1,"Пластина");

    _model->addDetail(0,Qt::darkRed,"Ствол");
    _model->addDetail(1,Qt::darkGreen,"Земля");

    _model->addPart(1,0,0,0,0);
    _model->addPart(0,1,1,1,0);

    ui->modelScreen->reset();
}
